<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permintaan extends Model
{
    protected $table = 'requests';
    protected $fillable =['total_transaksi','status_request','status_request','tanggal_konfirmasi','admin_id'];
    public function pelanggan()
    {
        return $this->belongsTo('App\User','pelanggan_id');
    }
    public function admin()
    {
        return $this->belongsTo('App\User','admin_id');
    }
    public function jadwal()
    {
        return $this->hasOne('App\Jadwal','request_id');
    }
    public function payment()
    {
        return $this->hasOne('App\Payment');
    }
    public function products(){
		return $this->belongsToMany('App\Product','product_request','request_id','product_id')
                        ->withPivot( ['harga','qty','qty_diterima','catatan','sub_total'] )
                        ->withTimestamps();
	}
    public function loading()
    {
        return $this->hasOne('App\Loading');
    }
    
}
