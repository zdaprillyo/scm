<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Privilege;
use Validator;
use Auth;
class PrivilegeController extends Controller
{
    public function validation($request,$action){
      $validator = Validator::make($request->all(), [
        'id' => ($action==0) ? '':'required|numeric',
        'nama' => ($action==2) ? '':'required|string',
      ]);
      if($validator->fails()){
        $message_title="Data Tidak Lengkap !";
        $message_conten=$validator->errors()->all();
        $message_type="error";
        $message_succes = false;
        $result = array(
                    'success' => $message_succes,
                    'message_title' => $message_title,
                    'message_conten' => $message_conten,
                    'message_type' => $message_type,
                   );
        return $result;
      }
      return 1;
    }
    private $fitur_id=2;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=Auth::user();
        if($user->getPermission('sub',$this->fitur_id,'read')!=1){ abort(401); }
        $akses=$user->getAccesses('sub',$this->fitur_id);
        $privileges=Privilege::all();
        $feature_name="Privilege";
        return view('aplikasi.hak_akses.privilege',compact('privileges','feature_name','akses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->getPermission('sub',$this->fitur_id,'create')!=1){ abort(401); }
        $inputs = $request->all();
        $validasi=$this->validation($request,0);
        if($validasi!=1){return json_encode($validasi);}

        try {
            $data = new Privilege();
            $data->nama=strtolower($inputs['nama']);
            $proses=$data->save(); 
        } catch (\Exception $e) {
            $proses='Kode Kesalahan : '.$e->getCode();
        }
        return json_encode($this->processResponse(0,$proses));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()->getPermission('sub',$this->fitur_id,'read')!=1){ abort(401); }
        $data=Privilege::find($id);
        if($data){
            $result = array(
                      'id'=>$data->id,
                      'nama' => $data->nama, 
                      'success' => true
                    ); 
        }else{
            $result = array(
                      'pesan'=>$data,
                      'success' => false
                    );
        }
        return json_encode($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::user()->getPermission('sub',$this->fitur_id,'update')!=1){ abort(401); }
        $inputs = $request->all();
        $validasi=$this->validation($request,1);
        if($validasi!=1){return json_encode($validasi);}
        
        try {
            $update  =  Privilege::find($inputs['id'])
                        ->update([ 'nama' => strtolower($inputs['nama']) ]);
        } catch (\Exception $e) {
            $proses='Kode Kesalahan : '.$e->getCode();
        }
        return json_encode($this->processResponse(1,$update));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if(Auth::user()->getPermission('sub',$this->fitur_id,'delete')!=1){ abort(401); }
        $inputs = $request->all();
        $validasi=$this->validation($request,2);
        if($validasi!=1){return json_encode($validasi);}

        $delete = Privilege::find($inputs['id'])->delete();
        return json_encode($this->processResponse(2,$delete));
    }
}
