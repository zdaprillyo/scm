<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permintaan;
use App\User;
use App\Product;
use Validator;
use Auth;
use App\Jadwal;
use App\Worktime;
use Artisan;

class RequestController extends Controller
{
    public function validation($request,$action){
      $validator = Validator::make($request->all(), [
        'id' => ($action==0) ? '':'required|numeric',
        'pelanggan' => ($action==1) ? '':'required|numeric',
        'tanggal' => ($action==1) ? '':'required|date',
        'nama-barang' => ($action==1) ? '':'required',
        'qty' => ($action==1) ? '':'required',
        'status_permintaan' => ($action==0 || $action==2) ? '':'required|string|in:diterima,ditolak,batal,selesai',
      ]);
      if($validator->fails()){
        $message_title="Data Tidak Lengkap !";
        $message_conten=$validator->errors()->all();
        $message_type="error";
        $message_succes = false;
        $result = array(
                    'success' => $message_succes,
                    'message_title' => $message_title,
                    'message_conten' => $message_conten,
                    'message_type' => $message_type,
                   );
        return $result;
      }
      return 1;
    }
    private $fitur_id=2;
    private $fitur_kategori="menu";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($jenis)
    {
        $user=Auth::user();
        if ($jenis=='pembelian') {
          $this->fitur_id=5;
        }
        if($user->getPermission($this->fitur_kategori,$this->fitur_id,'read')!=1){ abort(401); }
        $akses=$user->getAccesses($this->fitur_kategori,$this->fitur_id);
        if($user->privilege_id==1){
          $requests=Permintaan::where('jenis_permintaan',$jenis)->get();  
        }else if($user->privilege_id==2 || $user->privilege_id==3){
          $requests=Permintaan::where('jenis_permintaan',$jenis)->where('pelanggan_id',$user->id)->get();
        }
        $feature_name=ucfirst($jenis)." Barang";
        if($jenis=='pembelian'){$feature_name="Order Barang";}
        return view('aplikasi.request.daftar_request',compact('requests','feature_name','akses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($jenis)
    {
        $privilege_id=0;
        $judul_buat_permintaan="Buat Permintaan Pengambilan Barang";
        $jenis_pelanggan="Pengepul";
        $privilege_id=2;
        if($jenis=="pembelian"){
            $judul_buat_permintaan="Buat Permintaan Order Barang";
            $jenis_pelanggan="Konsumen";
            $privilege_id=3;
            $this->fitur_id=5;
        }
        $user=Auth::user();
        if($user->getPermission($this->fitur_kategori,$this->fitur_id,'create')!=1){ abort(401); }
        if($user->privilege_id==1){
          $pelanggans=User::where('privilege_id',$privilege_id)->get();
        }
        else if($user->privilege_id==2 || $user->privilege_id==3){
         $pelanggans=User::where('id',$user->id)->get();
        }
        
        return view('aplikasi.request.buat_request',compact('pelanggans','judul_buat_permintaan','jenis_pelanggan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();
        $pelanggan=User::find($inputs['pelanggan']);
        if(empty($pelanggan)){
            $result = array(
                        'success' => false,
                        'message_title' => "Gagal !",
                        'message_conten' => "Data pelanggan tidak ditemukan!",
                        'message_type' => "error",
                       );
            return json_encode($result);
        }
        if($pelanggan->privilege_id==3){ $this->fitur_id=5; }
        if(Auth::user()->getPermission($this->fitur_kategori,$this->fitur_id,'create')!=1){ abort(401); }
        $validasi=$this->validation($request,0);
        if($validasi!=1){return json_encode($validasi);}
        $data_barang=true;
        foreach ($inputs['nama-barang'] as $barang) {
            $data=Product::where('nama',$barang)->get();
            if(empty($data[0])){
                $data_barang=false;
                $result = array(
                            'success' => false,
                            'message_title' => "Gagal !",
                            'message_conten' => "Data barang ".$barang." tidak ditemukan!",
                            'message_type' => "error",
                           );
                return json_encode($result);
            }
        }
        try {
          $req=new Permintaan();
          if(isset($inputs['id'])) {
            $req=Permintaan::find($inputs['id']);
            $req->products()->detach();
          }
          $req->tanggal_transaksi=date('Y-m-d',strtotime($inputs['tanggal']));
          if($pelanggan->privilege_id==2){ $req->jenis_permintaan="pengambilan"; }
          else if($pelanggan->privilege_id==3){ $req->jenis_permintaan="pembelian"; }
          $req->status_request='menunggu';
          if(isset($inputs['status_permintaan'])){
            $req->status_request=$inputs['status_permintaan'];
          }
          $req->pelanggan_id=$inputs['pelanggan'];
          if($inputs['status_permintaan']=="diterima"){
            $req->tanggal_konfirmasi=now();
            $req->admin_id=Auth::id();
          }
          if($proses=$req->save()){
            $this->jadwalkan($req->status_request,$req->jenis_permintaan,$req->id);
            $i=0;
            foreach ($inputs['nama-barang'] as $namaBarang) {
              $barang=Product::where('nama',$namaBarang)->get();
              if($req->products()->wherePivot('product_id',$barang[0]->id)->exists()){
                $qty_lama=$req->products()->wherePivot('product_id',$barang[0]->id)->first()->pivot->qty;
                $harga=$req->products()->wherePivot('product_id',$barang[0]->id)->first()->pivot->harga;
                $req->products()->updateExistingPivot($barang[0]->id,[
                    'qty'=>$qty_lama+$inputs['qty'][$i],
                    'sub_total'=>($qty_lama+$inputs['qty'][$i])*$harga,
                ]);
                $i++;
              }else{
                if($req->jenis_permintaan=="pengambilan"){
                  if($inputs['status_permintaan']=="diterima"){
                    $req->products()->attach($barang[0]->id,[
                      'harga'=>$barang[0]->harga_beli,
                      'qty'=>$inputs['qty'][$i],
                      'qty_diterima'=>$inputs['qty'][$i],
                      'sub_total'=>$barang[0]->harga_beli*$inputs['qty'][$i]
                    ]);
                  }else{
                    $req->products()->attach($barang[0]->id,[
                      'harga'=>$barang[0]->harga_beli,
                      'qty'=>$inputs['qty'][$i],
                      'sub_total'=>$barang[0]->harga_beli*$inputs['qty'][$i]
                    ]);
                  }
                  
                }else if($req->jenis_permintaan=="pembelian"){
                  $req->products()->attach($barang[0]->id,[
                    'harga'=>$barang[0]->harga_jual,
                    'qty'=>$inputs['qty'][$i],
                    'sub_total'=>$barang[0]->harga_jual*$inputs['qty'][$i]
                  ]);
                }
                $i++;
              }
            }
            $total_transaksi=0;
            foreach ($req->products as $detail) {
                $total_transaksi+=$detail->pivot->sub_total;
            }
            $proses=Permintaan::where('id', $req->id)->update(['total_transaksi' => $total_transaksi]);
          }
        } catch (\Exception $e) {
            $proses='Kode Kesalahan : '.$e->getMessage();
        }
        return json_encode($this->processResponse(0,$proses));
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=Permintaan::find($id);
        if($data){
          if($data->pelanggan->privilege->id==3){
            $this->fitur_id=5;
          }
          if(Auth::user()->getPermission($this->fitur_kategori,$this->fitur_id,'read')!=1){ abort(401); }
          $isi_table="";
          foreach ($data->products as $produk) {
            $isi_table=$isi_table.
                        "<tr style='text-align:right'>".
                          "<input type=\"hidden\" name=\"id_product_request[]\" id=\"id_product_request\" value=".$produk->id.">".
                          "<td style='text-align:center'>".ucfirst($produk->nama)."</td>".
                          "<td>".number_format($produk->pivot->harga,0,',','.')."</td>".
                          "<td>".$produk->pivot->qty."</td>";
                        if ($data->jenis_permintaan=='pengambilan'){
                          $tdValue="";
                          if(is_null($produk->pivot->qty_diterima)){
                            $tdValue="<div class=\"input-group\">
                                      <input type=\"number\" class=\"form-control\" style=\"height: 30px; width: 10px;\"  name=\"qty_diterima[]\" id=\"qty_diterima\" min=\"0\" value=".$produk->pivot->qty." max=".$produk->pivot->qty.">
                                    </div>";
                          }else{
                            $tdValue=number_format($produk->pivot->qty_diterima,0,',','.');
                          }
                          $isi_table=$isi_table.
                            "<td>".$tdValue."</td>";
                        }
            $isi_table=$isi_table.
                        "<td>".number_format($produk->pivot->sub_total,0,',','.')."</td>";
                        if ($data->jenis_permintaan=='pengambilan'){
                          $tdValue="";
                          if(is_null($produk->pivot->catatan) && $data->status_request=='menunggu'){
                            $tdValue="<div class=\"input-group\">
                                      <input type=\"text\" class=\"form-control\" style=\"height: 30px; width: 200px;\"  name=\"catatan[]\" id=\"catatan\">
                                    </div>";
                          }else{
                            $tdValue=$produk->pivot->catatan;
                          }
                          $isi_table=$isi_table.
                            "<td>".$tdValue."</td>";
                        }
                    "</tr>";
          }
          $admin='-';
          $tgl_konfirmasi='-';
          if(isset($data->admin->name)){
            $admin=$data->admin->name;
            $tgl_konfirmasi=date('d-m-Y H:i',strtotime($data->tanggal_konfirmasi));
          }
          $result = array(
                    'id'=>$data->id,
                    'tanggal_transaksi' => date('d-m-Y',strtotime($data->tanggal_transaksi)),
                    'total_transaksi' => number_format($data->total_transaksi,0,',','.'),
                    'status_request' => ucfirst($data->status_request),
                    'pelanggan' => $data->pelanggan->name,
                    'tanggal_konfirmasi' => $tgl_konfirmasi,
                    'admin' => $admin,
                    'isi_table'=>$isi_table,
                    'success' => true
                  ); 
        }else{
            $result = array(
                      'pesan'=>$data,
                      'success' => false
                    );
        }
        return json_encode($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user=Auth::user();
        $request=Permintaan::find($id);
        $judul_buat_permintaan='Buat Permintaan Order Barang';
        $jenis_pelanggan='Konsumen';
        $this->fitur_id=5;
        if($request->pelanggan->privilege_id==2){
          $judul_buat_permintaan="Buat Permintaan Pengambilan Barang";
          $jenis_pelanggan="Pengepul";
          $this->fitur_id=2;
        }
        if($user->getPermission($this->fitur_kategori,$this->fitur_id,'update')!=1){ abort(401); }
        return view('aplikasi.request.buat_request',compact('request','judul_buat_permintaan','jenis_pelanggan'));
    }

    public function terimaRequest(Request $request){
      if(Auth::user()->getPermission($this->fitur_kategori,$this->fitur_id,'update')!=1){ abort(401); }
      $inputs = $request->all();
      try {
        $permintaan=Permintaan::find($inputs['id-modal-permintaan']);
        if($permintaan){
          for ($i=0; $i < count($inputs['id_product_request']); $i++) { 
            $productRequest=$permintaan->products()->wherePivot('product_id',$inputs['id_product_request'][$i])->first();
            if(!$productRequest){ return json_encode($this->processResponse(1,'Data barang tidak ditemukan!')); }
          }
          for ($i=0; $i < count($inputs['id_product_request']); $i++) { 
            $productRequest=$permintaan->products()->wherePivot('product_id',$inputs['id_product_request'][$i])->first();
            if($inputs['qty_diterima'][$i]>$productRequest->pivot->qty){
              return json_encode($this->processResponse(1,'Kuantitas '.ucfirst($productRequest->nama).' yang diterima harus lebih kecil atau sama dengan kuantitas yang ditawarkan!'));
            }else if($inputs['qty_diterima'][$i]<0){
              return json_encode($this->processResponse(1,'Kuantitas '.ucfirst($productRequest->nama).' yang diterima tidak boleh kurang dari 0!'));
            }
          }
          $total_transaksi=0;
          for ($i=0; $i < count($inputs['id_product_request']); $i++) { 
            $hargaProduk=$permintaan->products()->wherePivot('product_id',$inputs['id_product_request'][$i])->first()->pivot->harga;
            $sub_total=$hargaProduk*$inputs['qty_diterima'][$i];
            $total_transaksi+=$sub_total;
            $update=$permintaan->products()->updateExistingPivot($inputs['id_product_request'][$i],['qty_diterima' => $inputs['qty_diterima'][$i],'catatan'=>$inputs['catatan'][$i],'sub_total'=>$sub_total ]);
          }
          $update  = $permintaan->update(['total_transaksi'=>$total_transaksi,'status_request' => 'diterima','tanggal_konfirmasi'=>now(),'admin_id'=>Auth::user()->id ]);
          $this->jadwalkan($permintaan->status_request,$permintaan->jenis_permintaan,$permintaan->id);
        }
      } catch (\Exception $e) {
        $proses='Kode Kesalahan : '.$e->getCode();
      }
      return json_encode($this->processResponse(1,$update));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::user()->getPermission($this->fitur_kategori,$this->fitur_id,'update')!=1){ abort(401); }
        $inputs = $request->all();
        $validasi=$this->validation($request,1);
        if($validasi!=1){return json_encode($validasi);}
        try {
            $permintaan=Permintaan::find($inputs['id']);
            if($inputs['status_permintaan']=='selesai' && $permintaan->jenis_permintaan=='pengambilan'){
              foreach ($permintaan->products as $barang_masuk) {
                $barang_stok=Product::find($barang_masuk->pivot->product_id);
                $update=$barang_stok->update(['stok'=>$barang_stok->stok+$barang_masuk->pivot->qty]);
              }
            }
            $update  = $permintaan->update([ 'status_request' => $inputs['status_permintaan'],'tanggal_konfirmasi'=>now(),'admin_id'=>Auth::user()->id ]);
            $this->jadwalkan($inputs['status_permintaan'],$permintaan->jenis_permintaan,$inputs['id']);
        } catch (\Exception $e) {
            $update=$e->getMessage();
        }
        return json_encode($this->processResponse(1,$update));
    }

    public function jadwalkan($status_permintaan,$jenis_permintaan,$request_id){
      if($status_permintaan=='diterima' && $jenis_permintaan=='pengambilan'){
        $tgl_sekarang=date('Y-m-d');
        for ($i=0; $i < date('t'); $i++) { 
          $worktime=Worktime::whereDate('waktu_mulai',$tgl_sekarang)->get();
          foreach ($worktime as $time) {
            if($time->status==1){
              $jadwal=new Jadwal();
              $jadwal->status_jadwal='terjadwal';
              $jadwal->request_id=$request_id;
              $jadwal->worktime_id=$time->id;
              $update=$jadwal->save();
              $time->status=0;
              $update=$time->save();
              break 2;
            }
          }
        $tgl_sekarang=date('Y-m-d',strtotime('+1 day',strtotime($tgl_sekarang)));
        }
      }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
