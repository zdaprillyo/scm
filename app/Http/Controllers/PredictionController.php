<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use App\Prediction;
use App\Permintaan;
use App\Fuel;
use MathPHP\Statistics\Significance;
use App\Budget;
use App\Payment;

class PredictionController extends Controller
{
    public function validation($request,$action){
      $validator = Validator::make($request->all(), [
        'id' => ($action==0) ? '':'required|numeric',
        'utk_tanggal' => ($action==2) ? '':'required|date'
      ]);
      if($validator->fails()){
        $message_title="Data Tidak Lengkap !";
        $message_conten=$validator->errors()->all();
        $message_type="error";
        $message_succes = false;
        $result = array(
                    'success' => $message_succes,
                    'message_title' => $message_title,
                    'message_conten' => $message_conten,
                    'message_type' => $message_type,
                   );
        return $result;
      }
      return 1;
    }
	  private $fitur_id=4;
    private $fitur_kategori="sub";
    public function index()
    {
      $user=Auth::user();
      if($user->getPermission($this->fitur_kategori,$this->fitur_id,'read')!=1){ abort(401); }
      $akses=$user->getAccesses($this->fitur_kategori,$this->fitur_id);
      $predictions=Prediction::all();
      $feature_name="Prediksi";
      return view('aplikasi.prediction.prediction',compact('predictions','feature_name','akses'));
    }

    public function store(Request $request)
    {
      $user=Auth::user();
      if($user->getPermission($this->fitur_kategori,$this->fitur_id,'create')!=1){ abort(401); }
      $inputs = $request->all();
      $validasi=$this->validation($request,0);
      if($validasi!=1){return json_encode($validasi);}
      try{
          $prediksi = new Prediction();
          $prediksi->utk_tanggal=date('Y-m-d',strtotime($inputs['utk_tanggal']));
          $sampaiTgl=date('Y-m-d',strtotime('-7 days',strtotime($prediksi->utk_tanggal)));
          $dariTgl=date('Y-m-d',strtotime('-30 days',strtotime($sampaiTgl)));
          $prediksi->dari_tanggal=$dariTgl;
          $prediksi->hingga_tanggal=$sampaiTgl;
          $dsAsli=$this->getDatasetAsli($prediksi->dari_tanggal,$prediksi->hingga_tanggal);
          $prediksi->prediksi_pembayaran=$this->repeatGenerateDataset($dsAsli);
          $prediksi->user_id=$user->id;
          $proses=$prediksi->save();
      } catch (\Exception $e) {
          $proses='Kode Kesalahan : '.$e->getCode();
      }
      return json_encode($this->processResponse(0,$proses));
    }

    public function getDatasetAsli($dari_tanggal,$hingga_tanggal){
      $dataset=[];
      $dariTgl=$dari_tanggal;
      $sampaiTgl=$hingga_tanggal;
      $payments=Payment::where('status_pembayaran','lunas')->whereBetween('created_at',[$dariTgl,$sampaiTgl])->get();
      foreach($payments as $pay){ $dataset[]=$pay->total_pembayaran; }
      return $dataset;
    }
    public function repeatGenerateDataset($dataset1,$n=1000){
      $dataset2=[];
      for($i=0; $i < $n; $i++){ $dataset2[]=$this->generateDataset($dataset1); }
      return round(array_sum($dataset2)/$n);
    }  
    public function generateDataset($dataset1){
      $dataset2=[];
      if($this->getStDev($dataset1)>0){
        for ($i=0; $i < count($dataset1); $i++) { 
            $dataset2[]=$this->getRandomValueNormal(min($dataset1),max($dataset1),array_sum($dataset1)/count($dataset1),$this->getStDev($dataset1));
        }
        $tTest = Significance::tTest($dataset1, $dataset2);
        if($tTest['p2']>0.05){
          return round(array_sum($dataset2)/count($dataset2));
        }else{
          $this->generateDataset($dataset1);
        }
      }else{
        return round(array_sum($dataset1)/count($dataset1));
      }
    }
    public function getRandomValueNormal($min,$max,$mean,$std_deviation,$step=1) {
      $rand1 = (float)mt_rand()/(float)mt_getrandmax();
      $rand2 = (float)mt_rand()/(float)mt_getrandmax();
      $gaussian_number = sqrt(-2*log($rand1))*cos(2*M_PI*$rand2);
      $random_number = ($gaussian_number*$std_deviation) + $mean;
      $random_number = round($random_number/$step)*$step;
      if($random_number<$min||$random_number>$max) {
        $random_number = $this->getRandomValueNormal($min,$max,$mean,$std_deviation);
      }
      return $random_number;
    }
    public function getStDev($dataset) 
    { 
      $count_dataset = count($dataset);  
      $var = 0.0; 
      $mean = array_sum($dataset)/$count_dataset; 
      foreach($dataset as $data){ $var += pow(($data - $mean), 2); } 
      return (float)sqrt($var/$count_dataset); 
    }
}
