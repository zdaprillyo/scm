<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Loading;
use App\Permintaan;
use App\Finalproduct;
use App\Detailloading;
use Auth;
use Validator;

class LoadingController extends Controller
{
    public function validation($request,$action){
      $validator = Validator::make($request->all(), [
        'id' => ($action==0) ? '':'required|numeric',
        'waktu_mulai' => ($action==2) ? '':'required|date',
        'waktu_selesai' => ($action==2) ? '':'required|date',
        'status' => ($action==2) ? '':'nullable|string|in:dimuat,dikirim,dibongkar',
        'jenis_pengangkut' => ($action==2) ? '':'required|string|in:kontainer,truk',
        'no_pengangkut' => ($action==2) ? '':'required|string',
        'merk_pengangkut' => ($action==2) ? '':'required|string',
        'no_segel' => ($action==2) ? '':'nullable|string',
        'nama_sopir' => ($action==2) ? '':'nullable|string',
        'telepon' => ($action==2) ? '':'nullable|numeric',
        'request_id' => ($action==2 || $action==1) ? '':'required|numeric',
        'fproduct-id' =>($action==2 || $action==1) ? '':'required'
      ]);
      if($validator->fails()){
        $message_title="Data Tidak Lengkap !";
        $message_conten=$validator->errors()->all();
        $message_type="error";
        $message_succes = false;
        $result = array(
                    'success' => $message_succes,
                    'message_title' => $message_title,
                    'message_conten' => $message_conten,
                    'message_type' => $message_type,
                   );
        return $result;
      }
      return 1;
    }
    private $fitur_id=11;
    private $fitur_kategori="menu";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=Auth::user();
        if($user->getPermission($this->fitur_kategori,$this->fitur_id,'read')!=1){ abort(401); }
        $akses=$user->getAccesses($this->fitur_kategori,$this->fitur_id);
        $loadings=Loading::all();
        $feature_name="Muatan";
        return view('aplikasi.loading.loading',compact('loadings','feature_name','akses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user=Auth::user();
        if($user->getPermission($this->fitur_kategori,$this->fitur_id,'create')!=1){ abort(401); }
        $requests=Permintaan::where([
          ['jenis_permintaan','=','pembelian'],
          ['status_request','=','diterima']
        ])->get();
        return view('aplikasi.loading.buat_loading',compact('requests'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->getPermission($this->fitur_kategori,$this->fitur_id,'create')!=1){ abort(401); }
        $inputs = $request->all();
        $validasi=$this->validation($request,0);
        if($validasi!=1){return json_encode($validasi);}

        try {
          $load=new Loading();
          $load->waktu_mulai=date('Y-m-d H:i',strtotime($inputs['waktu_mulai']));
          $load->waktu_selesai=date('Y-m-d H:i',strtotime($inputs['waktu_selesai']));
          $load->status='dimuat';
          $load->jenis_pengangkut=$inputs['jenis_pengangkut'];
          $load->no_pengangkut=$inputs['no_pengangkut'];
          $load->merk_pengangkut=$inputs['merk_pengangkut'];
          if($load->jenis_pengangkut=='kontainer'){
            $load->no_segel=$inputs['no_segel'];
          }else if($load->jenis_pengangkut=='truk'){
            $load->nama_sopir=$inputs['nama_sopir'];
            $load->telepon=$inputs['telepon'];
          }
          $load->request_id=$inputs['request_id'];
          if($proses=$load->save()){
            foreach ($inputs['fproduct-id'] as $fid) {
              $fproduk=Finalproduct::where('id',$fid)->where('status',1)->get();
              if(empty($fproduk[0])) { return json_encode($this->processResponse(0,'Final produk tidak ditemukan!'));}
              $detail=new Detailloading();
              $detail->finalproduct_id=$fproduk[0]->id;
              $detail->loading_id=$load->id;
              $detail->harga=$fproduk[0]->product->harga_jual;
              $detail->sub_total=$fproduk[0]->qty*$detail->harga;
              $proses=$detail->save();
              $proses=$fproduk[0]->update(['status'=>0]);
            }
          }
          $proses=Permintaan::find($load->request_id)->update(['status_request'=>'proses']);  
        } catch (\Exception $e) {
          $proses='Kode Kesalahan : '.$e->getCode();
        }
        return json_encode($this->processResponse(0,$proses));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()->getPermission($this->fitur_kategori,$this->fitur_id,'read')!=1){ abort(401); }
        $data=Loading::find($id);
        if($data){
          $isi_table="";
          foreach ($data->detailloadings as $detail) {
            $isi_table=$isi_table.
                        "<tr>".
                          "<td>".$detail->id."</td>".
                          "<td>".$detail->finalproduct->id."</td>".
                          "<td>".ucwords($detail->finalproduct->product->nama)."</td>".
                          "<td style='text-align:right'>".$detail->finalproduct->qty."</td>".
                          "<td style='text-align:right'>".number_format($detail->harga,0,',','.')."</td>".
                          "<td style='text-align:right'>".number_format($detail->sub_total,0,',','.')."</td>".
                        "</tr>";
          }
          $result = array(
                    'id'=>$data->id,
                    'waktu_mulai' => $data->waktu_mulai,
                    'waktu_selesai' => $data->waktu_selesai,
                    'status' => $data->status,
                    'jenis_pengangkut' => $data->jenis_pengangkut,
                    'no_pengangkut' => $data->no_pengangkut,
                    'merk_pengangkut' => ucfirst($data->merk_pengangkut),
                    'no_segel' => $data->no_segel,
                    'nama_sopir' => ucfirst($data->nama_sopir),
                    'telepon' => $data->telepon,
                    'request_id' => $data->request_id.'-'.$data->request->pelanggan->name.
                                    ' ('.date('d-m-Y',strtotime($data->request->tanggal_transaksi)).')',
                    'isi_table'=>$isi_table,
                    'success' => true
                  ); 
        }else{
            $result = array(
                      'pesan'=>$data,
                      'success' => false
                    );
        }
        return json_encode($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::user()->getPermission($this->fitur_kategori,$this->fitur_id,'update')!=1){ abort(401); }
        $inputs = $request->all();
        $validasi=$this->validation($request,1);
        if($validasi!=1){return json_encode($validasi);}
        try {
            $loading=Loading::find($inputs['id']);
            $update  =  $loading->update([ 
                          'waktu_mulai' => $inputs['waktu_mulai'],
                          'waktu_selesai' => $inputs['waktu_selesai'],
                          'status' => $inputs['status'],
                          'jenis_pengangkut' => strtolower($inputs['jenis_pengangkut']),
                          'no_pengangkut' => strtoupper($inputs['no_pengangkut']),
                          'merk_pengangkut' => strtolower($inputs['merk_pengangkut']),
                          'no_segel' => (strtolower($inputs['jenis_pengangkut'])=='kontainer') ? $inputs['no_segel'] : null,
                          'nama_sopir' => (strtolower($inputs['jenis_pengangkut'])=='truk') ? strtolower($inputs['nama_sopir']) : null,
                          'telepon' => (strtolower($inputs['jenis_pengangkut'])=='truk') ? $inputs['telepon'] : null
                        ]);
            if($inputs['status']=='dibongkar'){
              foreach ($loading->detailloadings as $detail) {
                $fproduk=$detail->finalproduct;
                $update=$fproduk->update(['status'=>1]);
              }
            }
        } catch (\Exception $e) {
            $proses='Kode Kesalahan : '.$e->getCode();
        }
        return json_encode($this->processResponse(1,$update));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
