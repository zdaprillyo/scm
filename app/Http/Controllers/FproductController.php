<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Finalproduct;
use Validator;
use Auth;

class FproductController extends Controller
{
    private $fitur_id=10;
    private $fitur_kategori="menu";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=Auth::user();
        if($user->getPermission($this->fitur_kategori,$this->fitur_id,'read')!=1){ abort(401); }
        $akses=$user->getAccesses($this->fitur_kategori,$this->fitur_id);
        $fproducts=Finalproduct::all();
        $feature_name="Final produk";
        return view('aplikasi.fproduct.fproduct',compact('fproducts','feature_name','akses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()->getPermission($this->fitur_kategori,$this->fitur_id,'read')!=1){ abort(401); }
        $data=Finalproduct::where('id',$id)->where('status',1)->get();
        if(!empty($data[0])){
            $result = array(
                      'id'=>$data[0]->id,
                      'tanggal_dibuat' => date('d-m-Y',strtotime($data[0]->tanggal_dibuat)),
                      'qty' => $data[0]->qty,
                      'nama' => ucwords($data[0]->product->nama),
                      'success' => true
                    ); 
        }else{
            $result = array(
                      'pesan'=>'gagal',
                      'success' => false
                    );
        }
        return json_encode($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
