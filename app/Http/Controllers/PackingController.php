<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Packing;
use App\Fuel;
use App\Product;
use App\Finalproduct;
use Validator;
use Auth;

class PackingController extends Controller
{
    public function validation($request,$action){
      $validator = Validator::make($request->all(), [
        'id' => ($action==0) ? '':'required|numeric',
        'waktu_mulai' => ($action==2) ? '':'required|string',
        'waktu_selesai' => ($action==2) ? '':'required|string',
        'nama_barang' => ($action==1 || $action==2) ? '':'required|string',
        'bbm' => ($action==2) ? '':'nullable|string',
        'tanggal-detail'=> ($action==1 || $action==2) ? '':'required',
        'qty-detail'=> ($action==1 || $action==2) ? '':'required',
      ]);
      if($validator->fails()){
        $message_title="Data Tidak Lengkap !";
        $message_conten=$validator->errors()->all();
        $message_type="error";
        $message_succes = false;
        $result = array(
                    'success' => $message_succes,
                    'message_title' => $message_title,
                    'message_conten' => $message_conten,
                    'message_type' => $message_type,
                   );
        return $result;
      }
      return 1;
    }
    private $fitur_id=9;
    private $fitur_kategori="menu";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=Auth::user();
        if($user->getPermission($this->fitur_kategori,$this->fitur_id,'read')!=1){ abort(401); }
        $akses=$user->getAccesses($this->fitur_kategori,$this->fitur_id);
        $packings=Packing::all();
        $fuels=Fuel::where('status','tersedia')->get();
        $feature_name="Packing";
        return view('aplikasi.packing.packing',compact('packings','fuels','feature_name','akses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user=Auth::user();
        if($user->getPermission($this->fitur_kategori,$this->fitur_id,'create')!=1){ abort(401); }
        $fuels=Fuel::where('status','tersedia')->get();
        return view('aplikasi.packing.buat_packing',compact('fuels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if(Auth::user()->getPermission($this->fitur_kategori,$this->fitur_id,'create')!=1){ abort(401); }
      $inputs = $request->all();
      $validasi=$this->validation($request,0);
      if($validasi!=1){return json_encode($validasi);}
      try{
        $barang = Product::where('nama',$inputs['nama_barang'])->get();
        if(empty($barang[0])){
          $result = array(
                      'success' => false,
                      'message_title' => "Gagal !",
                      'message_conten' => "Data barang tidak ditemukan!",
                      'message_type' => "error",
                    );
          return json_encode($result);
        }
        $data= new Packing();
        $data->waktu_mulai= date('Y-m-d H:i',strtotime($inputs['waktu_mulai']));
        $data->waktu_selesai=date('Y-m-d H:i',strtotime($inputs['waktu_selesai']));
        if($inputs['bbm']){ $data->fuel_id=$inputs['bbm']; }
        if($proses=$data->save()){
          $i=0;
          $total_qty=0;
          foreach ($inputs['tanggal-detail'] as $tanggal) {
            $fproduk= new Finalproduct();
            $fproduk->tanggal_dibuat=date('Y-m-d',strtotime($tanggal));
            $fproduk->qty=$inputs['qty-detail'][$i];
            $fproduk->product_id=$barang[0]->id;
            $fproduk->packing_id=$data->id;
            $proses=$fproduk->save();
            $total_qty+=$inputs['qty-detail'][$i];
            $i++;
          }
          $proses=$barang[0]->update(['stok'=>($barang[0]->stok-$total_qty)]);
        }
      }catch (\Exception $e) {
        $proses='Kode Kesalahan : '.$e->getCode();
      }
      return json_encode($this->processResponse(0,$proses)); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()->getPermission($this->fitur_kategori,$this->fitur_id,'read')!=1){ abort(401); }
        $data=Packing::find($id);
        if($data){
          $isi_table="";
          foreach ($data->finalproducts as $fproduk) {
            $isi_table=$isi_table.
                        "<tr style='text-align:center'>".
                          "<td>".$fproduk->id."</td>".
                          "<td>".ucfirst($fproduk->product->nama)."</td>".
                          "<td>".date('d-m-Y',strtotime($fproduk->tanggal_dibuat))."</td>".
                          "<td style='text-align:right'>".$fproduk->qty."</td>".
                        "</tr>";
          }
          $result = array(
                    'id'=>$data->id,
                    'waktu_mulai' => date('d-m-Y H:i:s',strtotime($data->waktu_mulai)),
                    'waktu_selesai' => date('d-m-Y H:i:s',strtotime($data->waktu_selesai)),
                    'fuel_id' => $data->fuel_id,
                    'isi_table'=>$isi_table,
                    'success' => true
                  ); 
        }else{
            $result = array(
                      'pesan'=>$data,
                      'success' => false
                    );
        }
        return json_encode($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::user()->getPermission($this->fitur_kategori,$this->fitur_id,'update')!=1){ abort(401); }
        $inputs = $request->all();
        $validasi=$this->validation($request,1);
        if($validasi!=0){return json_encode($validasi);}        
        try {
            $update  =  Packing::find($inputs['id'])->update([ 
                          'waktu_mulai' =>  date('Y-m-d H:i:s',strtotime($inputs['waktu_mulai'])),
                          'waktu_selesai' => date('Y-m-d H:i:s',strtotime($inputs['waktu_selesai'])),
                          'fuel_id' => $inputs['bbm'],
                        ]);
        } catch (\Exception $e) {
            $proses='Kode Kesalahan : '.$e->getCode();
        }
        return json_encode($this->processResponse(1,$update));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if(Auth::user()->getPermission('menu',$this->fitur_id,'delete')!=1){ abort(401); }
        $inputs = $request->all();
        $validasi=$this->validation($request,2);
        if($validasi!=0){return json_encode($validasi);}
        $data=Packing::find($inputs['id']);
        foreach ($data->finalproducts as $fproduk) {
          if(!$deletefproduk=$fproduk->delete()){
            return json_encode($this->processResponse(2,$deletefproduk));
          }
        }
        $delete = $data->delete();
        return json_encode($this->processResponse(2,$delete));
    }
}
