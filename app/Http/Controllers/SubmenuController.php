<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Submenu;
use Auth;
class SubmenuController extends Controller
{

    public function validation($request,$action){
      $validator = Validator::make($request->all(), [
        'sub_id' => ($action==0) ? '':'required|numeric',
        'sub_nama' => ($action==2) ? '':'required|string',
        'sub_no_urut' => ($action==2) ? '':'required|numeric',
        'sub_url' => ($action==2) ? '':'required|string'
      ]);
      if($validator->fails()){
        $message_title="Data Tidak Lengkap !";
        $message_conten=$validator->errors()->all();
        $message_type="error";
        $message_succes = false;
        $result = array(
                    'success' => $message_succes,
                    'message_title' => $message_title,
                    'message_conten' => $message_conten,
                    'message_type' => $message_type,
                   );
        return $result;
      }
      return 1;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    private $fitur_id=1;
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->getPermission('sub',$this->fitur_id,'create')!=1){ abort(401); }
        $inputs = $request->all();
        $validasi=$this->validation($request,0);
        if($validasi!=1){return json_encode($validasi);}
        try {
            $data = new Submenu();
            $data->nama=strtolower($inputs['sub_nama']);
            $data->no_urut = $inputs['sub_no_urut'];
            $data->url = $inputs['sub_url'];
            $data->menu_id = $inputs['menu_id'];
            $proses=$data->save();
            $data->privileges()->attach(1,['create'=>1,'read'=>1,'update'=>1,'delete'=>1]);
            $data->privileges()->attach(2,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]);
            $data->privileges()->attach(3,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]); 
        } catch (\Exception $e) {
            $proses='Kode Kesalahan : '.$e->getCode();
        }
        return json_encode($this->processResponse(0,$proses));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()->getPermission('sub',$this->fitur_id,'read')!=1){ abort(401); }
        $data=Submenu::find($id);
        if($data){
            $result = array(
                      'id'=>$data->id,
                      'nama' => $data->nama,
                      'no_urut' => $data->no_urut,  
                      'url' => $data->url,
                      'success' => true
                    ); 
        }else{
            $result = array(
                      'pesan'=>$data,
                      'success' => false
                    );
        }
        return json_encode($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::user()->getPermission('sub',$this->fitur_id,'update')!=1){ abort(401); }
        $inputs = $request->all();
        $validasi=$this->validation($request,1);
        if($validasi!=1){return json_encode($validasi);}
        
        try {
            $update  =  Submenu::find($inputs['sub_id'])
                        ->update([
                            'nama' => strtolower($inputs['sub_nama']),
                            'no_urut' => $inputs['sub_no_urut'],
                            'url' => $inputs['sub_url']
                        ]);
        } catch (\Exception $e) {
            $proses='Kode Kesalahan : '.$e->getCode();
        }
        return json_encode($this->processResponse(1,$update));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if(Auth::user()->getPermission('sub',$this->fitur_id,'delete')!=1){ abort(401); }
        $inputs = $request->all();
        $validasi=$this->validation($request,2);
        if($validasi!=1){return json_encode($validasi);}
        $sub=Submenu::find($inputs['sub_id']);
        $sub->privileges()->detach();
        $delete = $sub->delete();
        return json_encode($this->processResponse(2,$delete));
    }
}
