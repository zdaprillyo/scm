<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function processResponse($process,$processResult){
      if($processResult==1){
        $pesan="";
        if($process==0){$pesan="Simpan";}
        else if($process==1){$pesan="Ubah";}
        else if($process==2){$pesan="Hapus";}
        else if($process==3){$pesan="Baca";}
        $message_title="Berhasil !";
        $message_conten=$pesan." Data Berhasil";
        $message_type="success";
        $message_succes = true;
      }else{
        $message_title="Kesalahan Internal Server !";
        $message_conten=$processResult;
        $message_type="error";
        $message_succes = false;
      }
      return array(
                  'success' => $message_succes,
                  'message_title' => $message_title,
                  'message_conten' => $message_conten,
                  'message_type' => $message_type,
                 );
    }
}
