<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Payment;
use App\Permintaan;
use App\Detailpayment;


class PaymentController extends Controller
{
    public function validation($request,$action){
      $validator = Validator::make($request->all(), [
        'id' => ($action==0) ? '':'required|numeric',
        'termin' => ($action==2) ? '':'required|numeric|in:1,2',
        'total_pembayaran' => ($action==2) ? '':'nullable|numeric',
        'status_pembayaran' => ($action==2) ? '':'required|string|in:lunas,belum lunas',
        'request_id' => ($action==2) ? '':'required|numeric'
      ]);
      if($validator->fails()){
        $message_title="Data Tidak Lengkap !";
        $message_conten=$validator->errors()->all();
        $message_type="error";
        $message_succes = false;
        $result = array(
                    'success' => $message_succes,
                    'message_title' => $message_title,
                    'message_conten' => $message_conten,
                    'message_type' => $message_type,
                   );
        return $result;
      }
      return 1;
    }
    private $fitur_id=4;
    private $fitur_kategori="menu";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $user=Auth::user();
      if($user->getPermission($this->fitur_kategori,$this->fitur_id,'read')!=1){ abort(401); }
      $akses=$user->getAccesses($this->fitur_kategori,$this->fitur_id);
      $payments=Payment::all();
      $requestIdInPayment=[];

      foreach ($payments as $pay) {
        if($pay->status_pembayaran =='belum lunas' || $pay->status_pembayaran=='lunas'){
          $requestIdInPayment[]=$pay->request_id;
        }
        
      }
      $requests=Permintaan::where([['status_request','!=','batal'],
                                   ['status_request','!=','menunggu'],
                                   ['status_request','!=','ditolak'],
                                   ['jenis_permintaan','=','pengambilan']
                                  ])
                            ->whereNotIn('id',$requestIdInPayment)
                            ->get();
      $feature_name="Pembayaran";
      return view('aplikasi.payment.payment',compact('payments','requests','feature_name','akses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->getPermission($this->fitur_kategori,$this->fitur_id,'create')!=1){ abort(401); }
        $inputs = $request->all();
        $validasi=$this->validation($request,0);
        if($validasi!=1){return json_encode($validasi);}
        try {
            $payment=new Payment();
            $payment->termin=$inputs['termin'];
            $payment->status_pembayaran=$inputs['status_pembayaran'];
            $payment->request_id=$inputs['request_id'];
            $request=Permintaan::find($payment->request_id);
            $payment->total_pembayaran=$request->total_transaksi;
            if($proses=$payment->save()){
              for ($i=0; $i < $payment->termin; $i++) {
                $detail=new Detailpayment();
                $detail->status_pembayaran=$inputs['status_pembayaran'];
                $detail->payment_id=$payment->id;
                if($payment->termin==1){
                  $detail->nominal_cicilan=$payment->total_pembayaran;
                  $detail->jatuh_tempo=date($request->tanggal_transaksi,strtotime('+30 days'));
                }else{
                  if($i==0){
                    $detail->nominal_cicilan=$payment->total_pembayaran*0.6;
                    $detail->jatuh_tempo=date($request->tanggal_transaksi,strtotime('+30 days'));
                  }else if($i==1){
                    $detail->nominal_cicilan=$payment->total_pembayaran*0.4;
                    $detail->jatuh_tempo=date($request->tanggal_transaksi,strtotime('+37 days'));
                  }
                }
                $proses=$detail->save();
              }
            }
        } catch (\Exception $e) {
            $proses='Kode Kesalahan : '.$e->getCode();
        }
        return json_encode($this->processResponse(0,$proses));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if(Auth::user()->getPermission($this->fitur_kategori,$this->fitur_id,'update')!=1){ abort(401); }
      $inputs = $request->all();
      try {
          $tanggal_bayar=null;
          if($inputs['status_pembayaran']=='lunas'){ $tanggal_bayar=now(); }
          $detail=Detailpayment::find($inputs['iddetail']);
          $update=$detail->update(['status_pembayaran'=>$inputs['status_pembayaran'],'tanggal_bayar'=>$tanggal_bayar]);
          $status_payment='lunas';
          $payment=$detail->payment;
          foreach ($payment->detailpayments as $detail) {
            if($detail->status_pembayaran=='belum lunas'){
              $status_payment='belum lunas';
              break;
            }
          }
          foreach ($payment->detailpayments as $detail) {
            if($detail->status_pembayaran=='batal'){
              $status_payment='batal';
              break;
            }
          }
          $payment->status_pembayaran=$status_payment;
          $update=$payment->save();
      } catch (\Exception $e) {
          $proses='Kode Kesalahan : '.$e->getCode();
      }
      return json_encode($this->processResponse(1,$update));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
