<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jadwal;
use App\Worktime;
use App\Permintaan;
use App\Finalproduct;
use Validator;
use Auth;
use DB;

class ScheduleController extends Controller
{
    public function validation($request,$action){
      $validator = Validator::make($request->all(), [
        'id' => ($action==0) ? '':'required|numeric',
        'tanggal' => ($action==1) ? '':'required|date',
        'waktu' => ($action==1) ? '':'required|numeric'
      ]);
      if($validator->fails()){
        $message_title="Data Tidak Lengkap !";
        $message_conten=$validator->errors()->all();
        $message_type="error";
        $message_succes = false;
        $result = array(
                    'success' => $message_succes,
                    'message_title' => $message_title,
                    'message_conten' => $message_conten,
                    'message_type' => $message_type,
                   );
        return $result;
      }
      return 1;
    }
    private $fitur_id=3;
    private $fitur_kategori="menu";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getWorktime(Request $request){
      $inputs = $request->all();
      $worktime=Worktime::where('status',1)->whereDate('waktu_mulai',date('Y-m-d',strtotime($inputs['tanggal'])))->get();
      $option="<option value='' selected>Pilih jam pengambilan</option>";
      foreach ($worktime as $time) {
        if($time->status==1){
          $option=$option."<option value=".$time->id.">".date('H:i',strtotime($time->waktu_mulai))." - ".date('H:i',strtotime($time->waktu_selesai))."</option>";
        }
      }
      $result=array('option'=>$option);
      return json_encode($result);
    }
    public function index()
    {
      $user=Auth::user();
      if($user->getPermission($this->fitur_kategori,$this->fitur_id,'read')!=1){ abort(401); }
      $akses=$user->getAccesses($this->fitur_kategori,$this->fitur_id);
      $listNilai=[];
      if($user->privilege_id==1){
        $schedules=Jadwal::all();
        $listNilai=Jadwal::getPriorityValue($schedules);
      }
      else if($user->privilege_id==2){
        $request_ids=[];
        foreach ($user->requests->where('jenis_permintaan','pengambilan') as $request){
          $request_ids[]=$request->id;
        }
        $schedules=Jadwal::whereIn('request_id',$request_ids)->get();
      }
      $feature_name="Penjadwalan";
      return view('aplikasi.schedule.schedule',compact('schedules','feature_name','listNilai','akses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()->getPermission($this->fitur_kategori,$this->fitur_id,'read')!=1){ abort(401); }
        $data=Jadwal::find($id);
        if($data){
            $result = array(
                      'id'=>$data->id,
                      'tanggal' => date('d-m-Y',strtotime($data->worktime->waktu_mulai)),
                      'waktu' =>  $data->worktime->id,
                      'success' => true
                    ); 
        }else{
            $result = array(
                      'pesan'=>$data,
                      'success' => false
                    );
        }
        return json_encode($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::user()->getPermission($this->fitur_kategori,$this->fitur_id,'update')!=1){ abort(401); }
        $inputs = $request->all();
        try {
            $jadwal=Jadwal::find($inputs['id']);
            if(isset($inputs['atribut'])){
              if($inputs['atribut']==1){
                  $update=$jadwal->update([ 
                            'waktu_mulai_aktual' => now(),
                            'status_jadwal' => 'proses'
                          ]);
                  $update=$jadwal->request->update(['status_request'=>'proses']);
              }else if($inputs['atribut']==2){
                  $update=$jadwal->update([ 
                            'waktu_selesai_aktual' => now(),
                            'status_jadwal' => 'selesai'
                          ]);
                  $update=$jadwal->request->update(['status_request'=>'diambil']);
              }
              return json_encode($this->processResponse(1,$update));      
            }
            $validasi=$this->validation($request,0);
            if($validasi!=1){return json_encode($validasi);}
            $update=Worktime::find($jadwal->worktime_id)->update(['status'=>1]);
            $update=$jadwal->update([ 
                      'worktime_id' => $inputs['waktu'],
                      'status_jadwal' => 'terjadwal'
                    ]);
            $update=Worktime::find($inputs['waktu'])->update(['status'=>0]);
        } catch (\Exception $e) {
            $proses='Kode Kesalahan : '.$e->getCode();
        }
        return json_encode($this->processResponse(1,$update));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
