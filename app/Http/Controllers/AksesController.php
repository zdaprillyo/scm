<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use App\Submenu;
Use App\Privilege;
use Auth;
class AksesController extends Controller
{
    private $fitur_id=3;
    public function index()
    {
        if(Auth::user()->getPermission('sub',$this->fitur_id,'read')!=1){ abort(401); }
        $menus=Menu::all();
        $privileges=Privilege::all();
        $feature_name="Aksesibilitas";
        //dd($menus);
        return view('aplikasi.hak_akses.aksesibilitas',compact('menus','privileges','feature_name'));
    }
    public function update(Request $request)
    {
        if(Auth::user()->getPermission('sub',$this->fitur_id,'update')!=1){ abort(401); }
        $inputs = $request->all();
        try {
            if($inputs['menusub_id']==1 && $inputs['privilege_id']==1){
              return json_encode($this->processResponse(1,"Hak Akses Administrator tidak dapat diubah"));
            }
            if($inputs['kategori']=='menu'){
              $menu=Menu::find($inputs['menusub_id']);
              $proses=$menu->privileges()->updateExistingPivot($inputs['privilege_id'],array($inputs['aksi']=>$inputs['status']));
              if(!$menu->submenus->isEmpty() && $inputs['status']==0){
              	$submenus=$menu->submenus;
                foreach ($submenus as $sub) {
                  $proses=$sub->privileges()->updateExistingPivot($inputs['privilege_id'],array('create'=>0,'read'=>0,'update'=>0,'delete'=>0));
                }
              }
            }else if($inputs['kategori']=='sub'){
            	$proses=Submenu::find($inputs['menusub_id'])->privileges()->updateExistingPivot($inputs['privilege_id'],array($inputs['aksi']=>$inputs['status']));
            }else{
            	$proses="Kategori tidak diketahui. Kategori hanya dapat bernilai Menu dan Sub";
            }
            
        } catch (\Exception $e) {
            $proses='Kode Kesalahan : '.$e->getCode();
        }
        return json_encode($this->processResponse(1,$proses));
    }
}
