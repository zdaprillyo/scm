<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Menu;
use Validator;
use Auth;

class ProductController extends Controller
{
    public function getHarga(Request $request){
        $inputs = $request->all();
        $barang=Product::where('nama',$inputs['namaBarang'])->get();
        if($inputs['jenisPelanggan']=="Pengepul"){
            return $barang[0]->harga_beli;
        }else if($inputs['jenisPelanggan']=="Konsumen"){
            return $barang[0]->harga_jual;
        }
    }
    public function search(Request $request){
        $search = $request->get('term');
        $result = Product::where('nama','LIKE',$search. '%')->get();
        return json_encode($result);
    }
    public function validation($request,$action){
      $validator = Validator::make($request->all(), [
        'id' => ($action==0) ? '':'required|numeric',
        'nama' => ($action==2) ? '':'required|string',
        'stok' => ($action==2) ? '':'required|numeric',
        'harga_jual' => ($action==2) ? '':'required|numeric',
        'harga_beli' => ($action==2) ? '':'required|numeric',
        'satuan_unit' => ($action==2) ? '':'required|string|in:kg,biji',
        'kategori' => ($action==2) ? '':'required|string|in:kertas,plastik,logam,kaca',
      ]);
      if($validator->fails()){
        $message_title="Data Tidak Lengkap !";
        $message_conten=$validator->errors()->all();
        $message_type="error";
        $message_succes = false;
        $result = array(
                    'success' => $message_succes,
                    'message_title' => $message_title,
                    'message_conten' => $message_conten,
                    'message_type' => $message_type,
                   );
        return $result;
      }
      return 1;
    }
    private $fitur_id=7;
    private $fitur_kategori="menu";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=Auth::user();
        if($user->getPermission($this->fitur_kategori,$this->fitur_id,'read')!=1){ abort(401); }
        $akses=$user->getAccesses($this->fitur_kategori,$this->fitur_id);
        $products=Product::all();
        $feature_name="Barang";
        return view('aplikasi.product.product',compact('products','feature_name','akses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->getPermission($this->fitur_kategori,$this->fitur_id,'create')!=1){ abort(401); }
        $inputs = $request->all();
        $validasi=$this->validation($request,0);
        if($validasi!=1){return json_encode($validasi);}

        try {
            $data = new Product();
            $data->nama=strtolower($inputs['nama']);
            $data->stok=$inputs['stok'];
            $data->harga_jual=$inputs['harga_jual'];
            $data->harga_beli=$inputs['harga_beli'];
            $data->satuan_unit=$inputs['satuan_unit'];
            $data->kategori=$inputs['kategori'];
            $proses=$data->save(); 
        } catch (\Exception $e) {
            $proses='Kode Kesalahan : '.$e->getCode();
        }
        return json_encode($this->processResponse(0,$proses));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()->getPermission($this->fitur_kategori,$this->fitur_id,'read')!=1){ abort(401); }
        $data=Product::find($id);
        if($data){
            $result = array(
                      'id'=>$data->id,
                      'nama' => $data->nama,
                      'stok' => $data->stok,
                      'harga_jual' => $data->harga_jual,
                      'harga_beli' => $data->harga_beli,
                      'satuan_unit' => $data->satuan_unit,
                      'kategori' => $data->kategori,
                      'success' => true
                    ); 
        }else{
            $result = array(
                      'pesan'=>$data,
                      'success' => false
                    );
        }
        return json_encode($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::user()->getPermission($this->fitur_kategori,$this->fitur_id,'update')!=1){ abort(401); }
        $inputs = $request->all();
        $validasi=$this->validation($request,1);
        if($validasi!=1){return json_encode($validasi);}
        
        try {
            $update  =  Product::find($inputs['id'])->update([ 
                          'nama' => strtolower($inputs['nama']),
                          'stok' => $inputs['stok'],
                          'harga_jual' => $inputs['harga_jual'],
                          'harga_beli' => $inputs['harga_beli'],
                          'satuan_unit' => $inputs['satuan_unit'],
                          'kategori' => $inputs['kategori'],
                        ]);
        } catch (\Exception $e) {
            $proses='Kode Kesalahan : '.$e->getCode();
        }
        return json_encode($this->processResponse(1,$update));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if(Auth::user()->getPermission($this->fitur_kategori,$this->fitur_id,'delete')!=1){ abort(401); }
        $inputs = $request->all();
        $validasi=$this->validation($request,2);
        if($validasi!=1){return json_encode($validasi);}

        $delete = Product::find($inputs['id'])->delete();
        return json_encode($this->processResponse(2,$delete));
    }
}
