<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;
use App\User;
use App\Privilege;
use Hash;

class PenggunaController extends Controller
{
    public function validation($request,$action){
      $validator = Validator::make($request->all(), [
        'id' => ($action==0) ? '':'required|numeric',
        'nama' => ($action==2) ? '':'required|string',
        'email' => ($action==2 || $action==1) ? '':'required|email|unique:users,email',
        'email' => ($action==1) ? 'required|email':'',
        'telepon' => ($action==2) ? '':'nullable|numeric|digits_between:8,15',
        'alamat' => ($action==2) ? '':'nullable|string',
        'privilege_id' => ($action==2 || $action==1) ? '':'required|numeric'
      ]);
      if($validator->fails()){
        $message_title="Data Tidak Lengkap !";
        $message_conten=$validator->errors()->all();
        $message_type="error";
        $message_succes = false;
        $result = array(
                    'success' => $message_succes,
                    'message_title' => $message_title,
                    'message_conten' => $message_conten,
                    'message_type' => $message_type,
                   );
        return $result;
      }
      return 1;
    }
    private $fitur_id=13;
    private $fitur_kategori="menu";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=Auth::user();
        if($user->getPermission($this->fitur_kategori,$this->fitur_id,'read')!=1){ abort(401); }
        $akses=$user->getAccesses($this->fitur_kategori,$this->fitur_id);
        $users=User::whereNotIn('id',[$user->id])->get();
        $privileges=Privilege::all();
        $feature_name="Pengguna";
        return view('aplikasi.user.user',compact('users','privileges','feature_name','akses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->getPermission($this->fitur_kategori,$this->fitur_id,'create')!=1){ abort(401); }
        $inputs = $request->all();
        $validasi=$this->validation($request,0);
        if($validasi!=1){return json_encode($validasi);}
        try {
            $data = new User();
            $data->name=strtolower($inputs['nama']);
            $data->email=$inputs['email'];
            $data->telepon=$inputs['telepon'];
            $data->alamat=$inputs['alamat'];
            $data->password=Hash::make('rahasia');
            $data->privilege_id=$inputs['privilege_id'];
            $proses=$data->save(); 
        } catch (\Exception $e) {
            $proses='Kode Kesalahan : '.$e->getCode();
        }
        return json_encode($this->processResponse(0,$proses));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()->getPermission($this->fitur_kategori,$this->fitur_id,'read')!=1){ abort(401); }
        $data=User::find($id);
        if($data){
            $result = array(
                      'id'=>$data->id,
                      'nama' => $data->name,
                      'email' => $data->email,
                      'telepon' => $data->telepon,
                      'alamat' => $data->alamat,
                      'success' => true
                    ); 
        }else{
            $result = array(
                      'pesan'=>$data,
                      'success' => false
                    );
        }
        return json_encode($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::user()->getPermission($this->fitur_kategori,$this->fitur_id,'update')!=1){ abort(401); }
        $inputs = $request->all();
        $validasi=$this->validation($request,1);
        if($validasi!=1){return json_encode($validasi);}
        try {
            $update  =  User::find($inputs['id'])->update([ 
                          'name' => strtolower($inputs['nama']),
                          'email' => $inputs['email'],
                          'telepon' => $inputs['telepon'],
                          'alamat' => $inputs['alamat']
                        ]);
        } catch (\Exception $e) {
            $proses='Kode Kesalahan : '.$e->getCode();
        }
        return json_encode($this->processResponse(1,$update));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
