<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Delivery;
use App\Loading;
use App\Permintaan;
use Auth;
use Validator;

class DeliveryController extends Controller
{
    public function validation($request,$action){
      $validator = Validator::make($request->all(), [
        'id' => ($action==0) ? '':'required|numeric',
        'tanggal_berangkat' => ($action==2) ? '':'required|date',
        'estimasi_sampai' => ($action==2) ? '':'required|date',
        'tanggal_sampai' => ($action==2) ? '':'nullable|date',
        'no_tiket' => ($action==2) ? '':'nullable|string',
        'status' => ($action==2) ? '':'required|string|in:dikirim,sampai,batal',
        'loading_id' => ($action==2) ? '':'nullable|numeric'
      ]);
      if($validator->fails()){
        $message_title="Data Tidak Lengkap !";
        $message_conten=$validator->errors()->all();
        $message_type="error";
        $message_succes = false;
        $result = array(
                    'success' => $message_succes,
                    'message_title' => $message_title,
                    'message_conten' => $message_conten,
                    'message_type' => $message_type,
                   );
        return $result;
      }
      return 1;
    }
    private $fitur_id=12;
    private $fitur_kategori="menu";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $user=Auth::user();
      if($user->getPermission($this->fitur_kategori,$this->fitur_id,'read')!=1){ abort(401); }
      $akses=$user->getAccesses($this->fitur_kategori,$this->fitur_id);
      $deliveries=Delivery::all();
      $loading=Loading::where('status','dimuat')->get();
      $feature_name="Pengiriman";
      return view('aplikasi.delivery.delivery',compact('deliveries','loading','feature_name','akses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if(Auth::user()->getPermission($this->fitur_kategori,$this->fitur_id,'create')!=1){ abort(401); }
      $inputs = $request->all();
      $validasi=$this->validation($request,0);
      if($validasi!=1){return json_encode($validasi);}

      try {
          $data = new Delivery();
          $data->tanggal_berangkat=date('Y-m-d H:i',strtotime($inputs['tanggal_berangkat']));
          $data->estimasi_sampai=date('Y-m-d H:i',strtotime($inputs['estimasi_sampai']));
          $data->loading_id=$inputs['loading_id'];

          $load=Loading::find($inputs['loading_id']);
          if($load->jenis_pengangkut=='truk'){
              $data->no_tiket=$inputs['no_tiket'];    
          }
          $data->status=$inputs['status'];
          $status_request='dikirim';
          if($data->status=='sampai'){
              $data->tanggal_sampai=date('Y-m-d H:i',strtotime($inputs['tanggal_sampai']));
              $status_request='selesai';
          }
          $proses=$data->save();
          $proses=Permintaan::find($load->request_id)->update(['status_request'=>$status_request]);
          $proses=$load->update(['status'=>'dikirim']);  
      } catch (\Exception $e) {
          $proses='Kode Kesalahan : '.$e->getCode();
      }
      return json_encode($this->processResponse(0,$proses));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      if(Auth::user()->getPermission($this->fitur_kategori,$this->fitur_id,'read')!=1){ abort(401); }
      $data=Delivery::find($id);
      if($data){
          $result = array(
                    'id'=>$data->id,
                    'tanggal_berangkat' => date('d-m-Y H:s',strtotime($data->tanggal_berangkat)),
                    'estimasi_sampai' => date('d-m-Y H:s',strtotime($data->estimasi_sampai)),
                    'tanggal_sampai' => date('d-m-Y H:s',strtotime($data->tanggal_sampai)),
                    'no_tiket' => $data->no_tiket,
                    'status' => $data->status,
                    'loading_id' => $data->loading_id,
                    'success' => true
                  ); 
      }else{
          $result = array(
                    'pesan'=>$data,
                    'success' => false
                  );
      }
      return json_encode($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if(Auth::user()->getPermission($this->fitur_kategori,$this->fitur_id,'update')!=1){ abort(401); }
      $inputs = $request->all();
      if(!isset($inputs['status_delivery'])){
          $validasi=$this->validation($request,1);
          if($validasi!=1){return json_encode($validasi);}
      }
      try {
        if(isset($inputs['status_delivery'])){
          $update  =  Delivery::find($inputs['id'])->update([ 'status' => $inputs['status_delivery'],'tanggal_sampai'=>now()]);
        }else{
          $tanggal_sampai=null;
          if($inputs['status']=='sampai'){
            $tanggal_sampai=date('Y-m-d H:i',strtotime($inputs['tanggal_sampai']));
          }
          $update  =  Delivery::find($inputs['id'])->update([ 
                        'tanggal_berangkat' => date('Y-m-d H:i',strtotime($inputs['tanggal_berangkat'])),
                        'estimasi_sampai' => date('Y-m-d H:i',strtotime($inputs['estimasi_sampai'])),
                        'tanggal_sampai' => $tanggal_sampai,
                        'no_tiket' => $inputs['no_tiket'],
                        'status' => $inputs['status']
                      ]);
        }
      } catch (\Exception $e) {
          $proses='Kode Kesalahan : '.$e->getCode();
      }
      return json_encode($this->processResponse(1,$update));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
