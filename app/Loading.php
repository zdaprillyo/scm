<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loading extends Model
{
	protected $fillable =['waktu_mulai','waktu_selesai','status','jenis_pengangkut','no_pengangkut','merk_pengangkut','no_segel','nama_sopir','telepon'];
	public function request()
    {
        return $this->belongsTo('App\Permintaan');
    }
    public function detailloadings(){
		return $this->hasMany('App\Detailloading');
	}
	public function delivery(){
		return $this->hasOne('App\Delivery');
	}
}
