<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $fillable = [ 'nama', 'stok', 'harga_jual','harga_beli','satuan_unit'];
    public function requests(){
		return $this->belongsToMany('App\Permintaan','product_request','product_id','request_id')
                        ->withPivot( ['harga','qty','qty_diterima','catatan','sub_total'] )
                        ->withTimestamps();
	}
	public function finalproducts(){
		return $this->hasMany('App\Finalproduct');
	}
	
}
