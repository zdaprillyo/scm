<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Packing extends Model
{
	protected $fillable =['waktu_mulai','waktu_selesai','fuel_id'];
	public function finalproducts(){
		return $this->hasMany('App\Finalproduct');
	}
	public function fuel()
    {
        return $this->belongsTo('App\Fuel');
    }
}
