<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
	protected $fillable = [ 'tanggal_berangkat', 'estimasi_sampai', 'tanggal_sampai','no_tiket','status'];
    public function loading()
    {
        return $this->belongsTo('App\Loading');
    }
}
