<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detailpayment extends Model
{
	protected $fillable =['status_pembayaran','tanggal_bayar'];
    public function payment()
    {
        return $this->belongsTo('App\Payment');
    }
}
