<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prediction extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

	public function getPembayaranAktual(){
		$pay=Payment::where('status_pembayaran','lunas')->whereDate('created_at',$this->utk_tanggal)->get();
		if(!empty($pay[0])){
			return $pay[0]->total_pembayaran;
		}else{
			return 0;
		}
	}
	
	public function getAkurasi(){
	  $prediksi=$this->prediksi_pembayaran;
	  $aktual=$this->getPembayaranAktual();
	  $diff=abs($aktual-$prediksi)/(($prediksi+$aktual)/2)*100;
	  $akurasi=100-round($diff,2);
	  if($akurasi>0){ return $akurasi; }else{ return 0; }
	}
}
