<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fuel extends Model
{
	protected $fillable = [ 'nama', 'tanggal_beli', 'qty','harga','status'];
    public function packings(){
		return $this->hasMany('App\Packing');
	}
}
