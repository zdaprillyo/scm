<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Worktime extends Model
{
	protected $fillable =['status'];
    public function jadwal()
    {
        return $this->hasOne('App\Jadwal');
    }
}
