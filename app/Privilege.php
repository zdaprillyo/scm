<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Privilege extends Model
{
    protected $fillable = [
        'nama'
    ];
    public function menus(){
		return $this->belongsToMany('App\Menu')
                        ->withPivot( ['create','read','update','delete'] )
                        ->withTimestamps();
	}
	public function users()
    {
        return $this->hasMany('App\User');
    }
    public function filterMenus(){
		return $this->menus()->wherePivot('read', 1)->orderBy('no_urut');
	}
    public function submenus(){
        return $this->belongsToMany('App\Submenu')
                        ->withPivot( ['create','read','update','delete'] )
                        ->withTimestamps();
    }
    public function filterSubs(){
        return $this->submenus()->wherePivot('read', 1)->orderBy('no_urut');
    }
}
