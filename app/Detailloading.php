<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detailloading extends Model
{
    public function finalproduct()
    {
        return $this->belongsTo('App\Finalproduct');
    }
    public function loading()
    {
        return $this->belongsTo('App\Loading');
    }
}
