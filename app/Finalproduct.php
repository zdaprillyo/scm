<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Finalproduct extends Model
{
    protected $fillable = [ 'tanggal_dibuat', 'qty', 'status'];
    public function product()
    {
        return $this->belongsTo('App\Product');
    }
    public function packing()
    {
        return $this->belongsTo('App\Packing');
    }
    public function detailloading()
    {
        return $this->hasOne('App\Detailloading');
    }
    
}
