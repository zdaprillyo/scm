<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Menu;
use App\Submenu;
use Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name', 'email','alamat','telepon','password'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function privilege()
    {
        return $this->belongsTo('App\Privilege');
    }
    public function requests()
    {
        return $this->hasMany('App\Permintaan','pelanggan_id');
    }
    public function allowedMenus(){
        return $this->privilege->filterMenus;
    }
    public function allowedSubs(){
        return $this->privilege->filterSubs;
    }
    public function budgets()
    {
        return $this->hasMany('App\Budget');
    }
    public function predictions()
    {
        return $this->hasMany('App\Prediction');
    }
    public function getPermission($kategori,$id,$aksi){
        if($kategori=='menu'){
            return Menu::find($id)->privileges()->wherePivot('privilege_id',Auth::user()->privilege->id)->first()->pivot->$aksi;
        }else if($kategori=='sub'){
            return Submenu::find($id)->privileges()->wherePivot('privilege_id',Auth::user()->privilege->id)->first()->pivot->$aksi;
        }
    }
    public function getAccesses($kategori,$id){
        if($kategori=='menu'){
            return Menu::find($id)->privileges()->wherePivot('privilege_id',Auth::user()->privilege->id)->first();
        }else if($kategori=='sub'){
            return Submenu::find($id)->privileges()->wherePivot('privilege_id',Auth::user()->privilege->id)->first();
        }
    }

}
