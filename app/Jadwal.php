<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Permintaan;
use App\Finalproduct;

class Jadwal extends Model
{
    protected $table = 'schedules';
    protected $fillable = [ 'waktu_mulai_aktual', 'waktu_selesai_aktual', 'status_jadwal','worktime_id'];
    public function request()
    {
        return $this->belongsTo('App\Permintaan');
    }
    public function worktime()
    {
        return $this->belongsTo('App\Worktime');
    }
    public function getRank($listNilai){
      $listRank=[];
      foreach ($listNilai as $nilai) {
        $listRank[]=$nilai;
      }
      rsort($listRank);
      if(array_key_exists($this->request_id,$listNilai)){
        return array_search($listNilai[$this->request_id],$listRank)+1;
      }else{
        return 0;
      }
    }
    public static function getPriorityValue($schedules){
      //cari stok barang
      $fproduk=Finalproduct::where('status',1)->get();
      $qty=[];
      $barang=[];
      $stok=[];
      foreach ($fproduk as $produk){
        if(!in_array($produk->product->nama, $barang)){
          $barang[]=$produk->product->nama;
          $qty[]=$produk->qty;
        }else{
          $qty[array_keys($barang,$produk->product->nama)[0]]+=$produk->qty;
        }}
      $stok['barang']=$barang;
      $stok['qty']=$qty;
      //cari barang yang diminta konsumen
      $qty=[];
      $barang=[];
      $order=[];
      $idBarang=[];
      $permintaan=Permintaan::where([['jenis_permintaan','=','pembelian'],['status_request','=','diterima']])->get();
      foreach ($permintaan as $p) {
        foreach ($p->products as $produk) {
          if (!in_array($produk->nama, $barang)) {
            $barang[]=$produk->nama;
            $qty[]=$produk->pivot->qty;
            $idBarang[]=$produk->id;
          }else{
            $qty[array_keys($barang,$produk->nama)[0]]+=$produk->pivot->qty;
          }
        }}
      $order['idBarang']=$idBarang;
      $order['barang']=$barang;
      $order['qty']=$qty;
      //menghitung kriteria dan bobot barang
      $kriteria=[];
      $kriteria['idBarang']=$order['idBarang'];
      $kriteria['barang']=$order['barang'];
      $qty=[];
      foreach ($kriteria['barang'] as $barang) {
        if (in_array($barang, $stok['barang'])) {
          $qtyStok=$stok['qty'][array_search($barang,$stok['barang'])];  
        }else{
          $qtyStok=0;
        }
        $qtyOrder=$order['qty'][array_search($barang,$order['barang'])];
        $qty[]=abs($qtyStok-$qtyOrder); 
      }
      $kriteria['qty']=$qty;
      $bobot=[];
      $bobot=$qty;
      //perhitungan bobot
      while (array_sum($bobot)>1) {
        $total=array_sum($bobot);
        for ($i=0; $i < count($bobot); $i++) { 
          $bobot[$i]=$bobot[$i]/$total;
        }
      }
      $kriteria['bobot']=$bobot;

      //pencarian data barang dari jadwal
      $alternatif=[];
      $idPengambilan=[];
      foreach ($schedules as $jadwal) {
        if($jadwal->status_jadwal=='terjadwal'){
          $idPengambilan[]=$jadwal->request_id;
        }
      }
      $pengambilan=Permintaan::whereIn('id',$idPengambilan)->get();
      foreach ($pengambilan as $ambil) {
        for ($i=0; $i < count($kriteria['idBarang']); $i++) { 
          $produk=$ambil->products()->where('product_id',$kriteria['idBarang'][$i])->get();
          $alternatif[$ambil->id]['barang'][]=$kriteria['barang'][$i];
          if (!empty($produk[0])) {
            $alternatif[$ambil->id]['nilai'][]=$produk[0]->pivot->qty_diterima;
          }else{
            $alternatif[$ambil->id]['nilai'][]=0;
          }
        }
      }
      //perhitungan nilai prioritas
      $listNilai=[];
      foreach ($alternatif as $idx => $nilai) {
        $prioritas=[];
        for ($i=0; $i < count($kriteria['bobot']); $i++) {
          $prioritas[]=$nilai['nilai'][$i]*$kriteria['bobot'][$i]; 
        }
        $listNilai[$idx]=array_sum($prioritas);
      }
      return $listNilai;
    }
}
