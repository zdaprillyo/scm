<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Submenu extends Model
{
    protected $fillable = ['nama', 'no_urut','url'];
    public function menu()
    {
        return $this->belongsTo('App\Menu');
    }
    public function privileges(){
		return $this->belongsToMany('App\Privilege')
                        ->withPivot( ['create','read','update','delete'] )
                        ->withTimestamps();
	}
	
}
