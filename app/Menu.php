<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Submenu;
class Menu extends Model
{
	protected $fillable = ['nama', 'ikon', 'no_urut','url'];
	public function privileges(){
		return $this->belongsToMany('App\Privilege')
                        ->withPivot( ['create','read','update','delete'] )
                        ->withTimestamps();
	}
	public function submenus(){
		return $this->hasMany('App\Submenu');
	}
	
}
