<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Budget extends Model
{
	protected $fillable = [ 'nama', 'keterangan', 'tanggal','prioritas','nominal'];
	public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function predictions(){
		return $this->belongsToMany('App\Prediction')
                        ->withPivot( ['status','nominal_tercapai','nominal_tersisa'] )
                        ->withTimestamps();
	}
}
