<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
	protected $fillable =['status_pembayaran'];
    public function request()
    {
        return $this->belongsTo('App\Permintaan');
    }
    public function detailpayments(){
		return $this->hasMany('App\Detailpayment');
	}
}
