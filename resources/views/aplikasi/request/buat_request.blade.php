@extends('layouts.app', ['class' => 'bg-default'])

@section('content')
<div class="header bg-gradient-primary py-7 py-lg-1">
  <div class="justify-content-center m-4" style="width: 98%">
    <div class="row pb-2 mr-4">
      <div class="col">
        @if($judul_buat_permintaan=="Buat Permintaan Pengambilan Barang")
        <a href="{{ url('permintaan/pengambilan') }}" class="btn btn-danger btn-outline btn-lg">Batal</a>
        @else
          <a href="{{ url('permintaan/pembelian') }}" class="btn btn-danger btn-outline btn-lg">Batal</a>
        @endif
      </div>
      <div class="col text-right">
        <button type="button" class="btn btn-success" name="buat-nota" id="buat-nota" onclick="buatNota()">Buat Permintaan</button>
      </div>
    </div>
    <div><h3 class="text-white">
      @if($judul_buat_permintaan=="Buat Permintaan Pengambilan Barang")
        {{ __('Daftar Pengambilan Barang > ') }}
      @else
        {{ __('Daftar Order Barang > ') }}
      @endif
      {{$judul_buat_permintaan}}</h3>
    </div>
    <div id="main" class="mr-4">
      <form id="form_data" enctype="multipart/form-data" method="post" class="form-horizontal text-white" action="">
        @csrf
        <input type="hidden" name="id" id="id" value="{{ $request->id ?? '' }}">
        <div class="form-group">
          <div class="form-group row">
            <div class="col-4">
              <label for="pelanggan">{{$jenis_pelanggan}}</label>
              <select class="form-control" id="pelanggan" name="pelanggan">
                @if(isset($request))
                  <option value="{{ $request->pelanggan_id }}">{{ ucwords($request->pelanggan->name) }}</option>
                @endif
                @if(isset($pelanggans))
                  @foreach($pelanggans as $pelanggan)
                    <option value="{{$pelanggan->id}}">{{ ucwords($pelanggan->name) }}</option>
                  @endforeach
                @endif
              </select>
            </div>
            @if(Auth::user()->privilege_id==1)
              <div class="col-4">
                <label for="pembayaran">Status Permintaan</label>
                <select class="form-control" id="status_permintaan" name="status_permintaan">
                  <option value="menunggu" selected>Menunggu</option>
                  <option value="diterima">Diterima</option>
                </select>
              </div>
            @else
              <input type="hidden" name="status_permintaan" id="status_permintaan">
            @endif
            <div class="col-4">
              <label for="tanggal">Tanggal</label>
              <input class="form-control datepicker" data-date-format="dd-mm-yyyy" placeholder="Select date" type="text" name="tanggal" id="tanggal"  
              @if(isset($request))
                value="{{ date('d-m-Y', strtotime($request->tanggal_transaksi)) }}"
              @else
                value="{{ date('d-m-Y') }}"
              @endif
              >
            </div>
          </div>
        </div>
      </form>
      <div id="table-input">
        <h3 class="text-white">Form Input</h3>
        <table class="table table-striped table-bordered table-hover table-dark">
          <thead>
            <tr>
              <th scope="col">Nama Barang</th>
              <th scope="col">Qty</th>
              <th scope="col">Harga</th>
              <th scope="col">Sub Total</th>
              <th scope="col">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><input type="text" name="nama_barang" id="nama_barang" class="form-control masukan" onfocusout="getHarga()"></td>
              <td><input type="number" name="qty_barang" id="qty_barang" class="form-control masukan" min="1" onkeyup="hitungSubtotal()"></td>
              <td><input type="number" name="harga_barang" id="harga_barang" class="form-control masukan" min="1" readonly></td>
              <td><input type="number" name="sub_total_barang" id="sub_total_barang" class="form-control masukan" min="1" readonly></td>
              <td>
                <button type="button" class="btn btn-info" onclick="addBarang()">Tambah</button>
                <button type="button" class="btn btn-danger" onclick="showTableInput(false)">X</button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <button style="margin: 0px auto;" id="btn-showInput" type="button" class="btn btn-info mb-2" onclick="showTableInput(true)">Tambah Barang</button>
      <div id="table-output" class="table-responsive bg-white text-default p-3">
        <table id="myTable" class="table table-striped table-hover table-sm">
          <thead>
            <tr>
              <th>Nama Barang</th>
              <th>Qty</th>
              <th>Harga</th>
              <th>Sub Total</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody id="isi-tabel">
            @if(isset($request))
              @foreach($request->products as $produk)
                <tr>
                  <td>{{ $produk->nama }}</td>
                  <td class='text-right'>{{ $produk->pivot->qty }}</td>
                  <td class='text-right'>{{ $produk->pivot->harga }}</td>
                  <td class='text-right'>{{ $produk->pivot->sub_total }}</td>
                  <td><button type="button" class="btn btn-danger btn-sm" onclick="deleteBarang(this)">X</button></td>
                </tr>
              @endforeach
            @endif
          </tbody>
        </table>  
      </div>  
    </div>
  </div>
</div>
<div class="container bg-default mt--10 pb-5"></div>
@include('layouts.footers.auth')
<script>
  showTableInput(false);
  function showTableInput(status){
    var input = document.getElementById("table-input");
    var btn=document.getElementById("btn-showInput");
    if(status){
      input.style.display="block";
      btn.style.display="none";
    }else{
      input.style.display="none";
      btn.style.display="block";
      $(".masukan").val("");
    }
  }
  $("#nama_barang").autocomplete({
    source: function(request, response) {
      $.ajax({
        url: "{{url('search')}}",
        data: { "term" : request.term },
        dataType: "json",
        success: function(data){
          var resp = $.map(data,function(obj){ return obj.nama; }); 
          response(resp);
        }
      });
    },
    minLength: 1
  });
  document.querySelector("#qty_barang").addEventListener("keypress", function (evt) {
    if (evt.which < 48 || evt.which > 57){ evt.preventDefault(); }
  });
  function getDataFromTable(){
    //gets table
    var table = document.getElementById('myTable');
    //gets rows of table
    var rowLength = table.rows.length;
    //loops through rows    
      for (i = 0; i < rowLength; i++){
        if(i>0){
           //gets cells of current row
          var cells = table.rows.item(i).cells;
           //loops through each cell in current row
          for(var j = 0; j < cells.length; j++){
              /* get your cell info here */
            cellVal = cells.item(j).innerHTML;
            if(j==0){
              // $("#nama-barang").push(cellVal); 
              $("#form_data").append("<input type=\"hidden\" class=\"hidden-data\" name=\"nama-barang[]\" id=\"nama-barang\" value=\""+cellVal+"\">");
            }else if(j==1){
              //$("#qty").push(cellVal);
              $("#form_data").append("<input type=\"hidden\" class=\"hidden-data\"name=\"qty[]\" id=\"qty\" value=\""+cellVal+"\">");
            }
          }
        }
      }
  }
  function getHarga(){
    var barang=$('#nama_barang').val();
    var idpelanggan=$('#pelanggan').val();
    if(barang!=""){
      $.ajax({
        url: "{{ url('cariHarga') }}",
        cache: false,
        data: {"namaBarang":barang,"jenisPelanggan":"{{ $jenis_pelanggan }}"},
        type: 'GET',
        success: function(result){
          //alert(result);
          $('#harga_barang').val(result);  
        }
      });
    }
  }
  function hitungSubtotal(){
    var qty=$("#qty_barang").val();
    var harga=$("#harga_barang").val();
    $("#sub_total_barang").val(qty*harga);
  }
  function addBarang(){
    var nama=$("#nama_barang").val();
    var qty=$("#qty_barang").val();
    var harga=$("#harga_barang").val();
    if(nama!="" && qty!="" && harga!=""){
      var subtotal=harga*qty;
      $("#isi-tabel").append("<tr><td>"+nama+"</td><td class='text-right'>"+qty+"</td><td class='text-right'>"+harga+"</td><td class='text-right'>"+subtotal+"</td><td><button type=\"button\" class=\"btn btn-danger btn-sm\" onclick=\"deleteBarang(this)\">X</button></td></tr>");
    }
    showTableInput(false);
  }
  function deleteBarang(ctl) { $(ctl).parents("tr").remove(); }
  function buatNota(){
    getDataFromTable();
    $.ajax({
      url: "{{ url('request') }}",
      cache: false,
      type: "POST",
      data: $('#form_data').serialize(),
      success: function(result){
        //alert(result);
       var result = eval('('+result+')');
        if (result.success){
          swal({
            title: result.message_title,
            text: result.message_conten,
            type: result.message_type,
            closeOnConfirm: false,
            closeOnCancel: false   
          }, function(){ 
              var judul="{{ $judul_buat_permintaan }}";
              var link='';
              if(judul=='Buat Permintaan Pengambilan Barang'){
                link="{{ url('permintaan/pengambilan') }}";
              }else{
                link="{{ url('permintaan/pembelian') }}";
              }
              location.replace(link); 
            });
        }
        else
        {
          swal({
            title: result.message_title,
            text: result.message_conten,
            type: result.message_type,
            closeOnConfirm: false,
            closeOnCancel: false
          });
          $(".hidden-data").remove();
        }
      },
        error: function (request, status, error) {
          console.error(request.responseText); 
          swal({
              title: "Error!",
              text: error,
              type: status,
              closeOnConfirm: false,
              closeOnCancel: false
            });
          $(".hidden-data").remove();
         }
    });
  }
</script>
@endsection
