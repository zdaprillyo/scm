@extends('layouts.app', ['class' => 'bg-default'])

@section('content')
<div class="header bg-gradient-primary py-7 py-lg-1">
  <div class="justify-content-center m-4" style="width: 98%">
    @if($akses->pivot->create)
      <a class="btn btn-info"
        @if($feature_name=="Pengambilan Barang") 
        href="{{ url('buat/pengambilan') }}"
        @elseif($feature_name=="Order Barang")
        href="{{ url('buat/pembelian') }}"
        @endif
      >Tambah {{$feature_name}}</a>
    @endif
    <div><h3 class="text-white">Daftar {{$feature_name}}</h3></div>
    <div class="row table-responsive bg-white p-3">
      <table class="table table-striped table-hover table-sm table-bordered" id="myTable">
        <thead class="text-center">
          <th>ID</th>
          <th>Tanggal</th>
          <th>Total</th>
          <th>Status</th>
          <th>Pelanggan</th>
          <th>Konfirmasi</th>
          <th>Admin</th>
          <th>Aksi</th>
        </thead>
        <tbody>
          @foreach($requests as $data)
          <tr>
            <td>{{$data->id}}</td>
            <td>{{ date('d-m-Y',strtotime($data->tanggal_transaksi) ) }}</td>
            <td class="text-right">{{ number_format($data->total_transaksi,0,',','.') }}</td>
            <td>
              @if($data->status_request=="menunggu")
                <button type="button" class="btn btn-primary btn-sm">{{ ucfirst($data->status_request) }}</button>
              @elseif($data->status_request=="selesai")
                <button type="button" class="btn btn-success btn-sm">{{ ucfirst($data->status_request) }}</button>
              @elseif($data->status_request=="diterima" || $data->status_request=="diambil" || $data->status_request=="proses" || $data->status_request=="dikirim")
                <button type="button" class="btn btn-info btn-sm">{{ ucfirst($data->status_request) }}</button>
              @elseif($data->status_request=="ditolak")
                <button type="button" class="btn btn-danger btn-sm">{{ ucfirst($data->status_request) }}</button>
              @elseif($data->status_request=="batal")
                <button type="button" class="btn btn-warning btn-sm">{{ ucfirst($data->status_request) }}</button>
              @endif
            </td>
            <td>{{ ucwords($data->pelanggan->name)}}</td>
            <td>
              @if($data->tanggal_konfirmasi)
                {{ date('d-m-Y H:i', strtotime($data->tanggal_konfirmasi)) }}
              @else
                {{ __('-') }}
              @endif
              </td>
            <td class="text-kapital">{{ $data->admin->name ?? '-'}}</td>
            <td>
              @if($akses->pivot->update)
                @if(Auth::user()->privilege_id==1 && $data->status_request=="menunggu")
                  @if($feature_name=="Pengambilan Barang") 
                    <button type="button" id="btn-ubah" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-detail" onclick="showRequestDetail({{ $data->id }})">Terima</button>
                  @elseif($feature_name=="Order Barang")
                    <button type="button" id="btn-ubah" class="btn btn-success btn-sm" onclick=" updateStatus({{ $data->id }},'diterima')">Terima</button>  
                  @endif
                  <button type="button" id="btn-ubah" class="btn btn-danger btn-sm" onclick=" updateStatus({{ $data->id }},'ditolak')">Tolak</button>
                  <button type="button" id="btn-ubah" class="btn btn-warning btn-sm" onclick="updateStatus({{ $data->id }},'batal')">Batal</button>
                @endif
                @if($data->status_request=="menunggu")
                  <a href="{{ url('request')}}/{{ $data->id }}/edit" class="btn btn-primary btn-sm">Ubah</a>
                @endif
                @if($data->status_request=="diambil" && $feature_name=="Pengambilan Barang")
                  <button type="button" id="btn-ubah" class="btn btn-success btn-sm" onclick="updateStatus({{ $data->id }},'selesai')">Selesaikan</button>
                @endif
              @endif
              @if($data->status_request!="menunggu")
                <button type="button" id="btn-ubah" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-detail" onclick="showRequestDetail({{ $data->id }})">Detail</button>
              @endif
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

<!-- Start Modal -->
  <div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="form-modal" aria-hidden="true">
    <div class="modal-dialog modal-dialogcentered- modal-lg" role="document">
      <div class="modal-content bg-default">
        <div class="modal-header">
          <h5 class="modal-title text-white" id="modal_title">Detail Permintaan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" class="text-white">&times;</span>
          </button>
        </div>
        <div class="modal-body text-white">
          <div class="row">
            <div class="col">
              <div class="form-group row">
                <div class="input-group">
                  <div class="col-sm-5 d-flex align-items-center">ID Permintaan</div>
                  <div class="col-sm-5">: <span id="id-modal-request">1231</span></div>
                </div>
                <div class="input-group">
                  <div class="col-sm-5 d-flex align-items-center">Tanggal</div>
                  <div class="col-sm-5">: <span id="tanggal-modal-request">12-12-20</span></div>
                </div>
                <div class="input-group">
                  <div class="col-sm-5 d-flex align-items-center">Pelanggan</div>
                  <div class="col-sm-5">: <span id="pelanggan-modal-request" class="text-kapital">Ayuningtiyas </span></div>
                </div>
              </div>
            </div>
            <div class="col-6">
              <div class="form-group row">
                <div class="input-group">
                  <div class="col-sm-5 d-flex align-items-center">Status</div>
                  <div class="col-sm-5">: <span id="status-modal-request">Diterima</span></div>
                </div>
                <div class="input-group" id="dikonfirmasi_oleh">
                  <div class="col-sm-5 d-flex align-items-center">Dikonfirmasi oleh</div>
                  <div class="col-sm-5">: <span id="admin-modal-request" class="text-kapital">Aprillyo</span></div>
                </div>
                <div class="input-group" id="waktu_konfirmasi">
                  <div class="col-sm-5 d-flex align-items-center">Waktu konfirmasi</div>
                  <div class="col-sm-7">: <span id="konfirmasi-modal-request">12-12-1233 23:14 </span></div>
                </div>
              </div>
            </div>
          </div>
          <label>Detail Barang</label>
          <form id="form_data_modal" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id-modal-permintaan" id="id-modal-permintaan" value="">
          <table class="table table-striped table-bordered table-hover table-dark">
            <thead class="text-center ">
              <th>Barang</th>
              <th>Harga</th>
              @if($feature_name=="Pengambilan Barang") 
                <th>Qty Tawaran</th>
                <th>Qty Diterima</th>
              @elseif($feature_name=="Order Barang")
                <th>Qty</th>
              @endif
              <th>Sub Total</th>
              @if($feature_name=="Pengambilan Barang") 
                <th>Catatan</th>
              @endif
            </thead>
            <tbody id="isi-table-modal-detail-request">
              <tr style='text-align:right'>
                <td style="text-align:left">Kerdus</td>
                <td>Rp. {{ number_format(1000,0,',','.') }}</td>
                <td>10</td>
                @if($feature_name=="Pengambilan Barang") 
                  <td>
                    <div class="input-group">
                      <input type="number" class="form-control" style="height: 30px; width: 20px;">
                      <div class="input-group-append">
                        <input type="submit" name="btn-submit" class="btn btn-outline-success btn-sm" value="Terima">
                      </div>
                    </div>
                  </td>
                @endif
                <td>{{ number_format(10000,0,',','.') }}</td>
              </tr>
            </tbody>   
            <tfoot style="text-align:right">
              <tr>
                <td @if($feature_name=="Pengambilan Barang") colspan="4" @else colspan="3" @endif class="font-weight-bold">Total Akhir</td>
                <td class="font-weight-bold"><span id="total-akhir-modal-detail-request">{{ number_format(10000,0,',','.') }}</span></td>
              </tr>
            </tfoot>
          </table>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" id="btn-modal-terima" class="btn btn-success" onclick="actionsFunction(4,0)">Terima</button>
          <button type="button" class="btn btn-warning" data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </div>
  </div>
<!-- End Modal -->
<div class="container bg-default mt--10 pb-5"></div>
@include('layouts.footers.auth')
<script>
$(document).ready( function () {
    $('#myTable').DataTable({"aaSorting": [1]});
});
var feature_name='{{$feature_name}}';
var urlRoute="{{url('request')}}";
function updateStatus(id,aksi){
  var actionUrl=urlRoute+"/"+id;
  $.ajax({
    url: actionUrl,
    data: {"_token":"{{ csrf_token() }}","id":id,"status_permintaan":aksi},
    cache: false,
    type: "PUT",
    success: function(result){
     var result = eval('('+result+')');
      if (result.success){
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false   
        }, function(){ location.reload(); });
      }
      else{
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false
        });
      }
    },
      error: function (request, status, error) {
        console.log(request.responseText);
        swal({
            title: "Error!",
            text: error,
            type: "error",
            closeOnConfirm: false,
            closeOnCancel: false
          });
      }
  });
}

function showFormInput() {
  $(".masukkan").val("");
  $("#modal_title").html("Form Data "+feature_name+" Baru");
  $("#form_data").attr("action", "javascript:actionsFunction(0,0)");
  $("#btn_action").attr("value", "Simpan Data");
  $("#btn_action").removeClass("btn-danger").addClass("btn-success");
}
function showRequestDetail(id){
    $.ajax({
    type: "GET",
    url: urlRoute+"/"+id,
    cache: false,
    success: function(result){
      var result = eval('('+result+')');
      if(result.success){
        $("#id-modal-request").html(result.id);
        $("#id-modal-permintaan").val(result.id);
        $("#tanggal-modal-request").html(result.tanggal_transaksi);
        $("#pelanggan-modal-request").html(result.pelanggan);
        $("#status-modal-request").html(result.status_request);
        if(result.status_request=='Menunggu'){
          $("#btn-modal-terima").show();
        }else{
          $("#btn-modal-terima").hide();
        }
        if(result.admin!='-'){
          $("#dikonfirmasi_oleh").show();
          $("#waktu_konfirmasi").show();
          $("#admin-modal-request").html(result.admin);
          $("#konfirmasi-modal-request").html(result.tanggal_konfirmasi);
        }else{
          $("#dikonfirmasi_oleh").hide();
          $("#waktu_konfirmasi").hide();
        }
        
        $("#isi-table-modal-detail-request").html(result.isi_table);
        $("#total-akhir-modal-detail-request").html(result.total_transaksi);
      }else{
        swal({
          title: "Gagal Mengambil Data {{$feature_name}} !",
          text: result.pesan,
          type: "error"
        });
      }
    },
    error: function (request, status, error) {
      console.log(request.responseText);
      swal({
          title: "Error!",
          text: error,
          type: "error",
          closeOnConfirm: false,
          closeOnCancel: false
      });
    }
  });
}
function showFormUpdateOrDelete(action,id) {
  $("#icon_display").removeClass();
  if(action==1){ //ubah data
    $(".masukkan").prop('disabled', false);
    $("#btn_action").attr("value", "Ubah Data");
    $("#modal_title").html("Form Ubah Data "+feature_name);
    $("#btn_action").removeClass("btn-danger").addClass("btn-success");
  }else if(action==2){ //hapus data
    $(".masukkan").prop('disabled', true);
    $("#id").prop('disabled', false);
    $("#btn_action").attr("value", "Hapus Data");
    $("#modal_title").html("Form Hapus Data "+feature_name);
    $("#btn_action").removeClass("btn-success").addClass("btn-danger");
  }
  $.ajax({
    type: "GET",
    url: urlRoute+"/"+id,
    cache: false,
    success: function(result){
      var result = eval('('+result+')');
      if(result.success){
        $("#form_data").attr("action", "javascript:actionsFunction("+action+","+result.id+")");
        $("#id").val(result.id);
        $("#nama").val(result.nama);
        $("#stok").val(result.stok);
        $("#harga_jual").val(result.harga_jual);
        $("#harga_beli").val(result.harga_beli);
        $("#satuan_unit").val(result.satuan_unit);
      }else{
        swal({
          title: "Gagal Mengambil Data {{$feature_name}} !",
          text: result.pesan,
          type: "error"
        });
      }
    },
    error: function (request, status, error) {
      swal({
          title: "Error!",
          text: error,
          type: "error",
          closeOnConfirm: false,
          closeOnCancel: false
      });
    }
  });
}
function actionsFunction(action,id) {
  var method="";
  var actionUrl=urlRoute+"/"+id;
  if(action==0){
    method='POST';
    actionUrl=urlRoute;
  }else if(action==1){
    method='PUT';
  }else if(action==2){
    method='DELETE';
  }
  var data = $('#form_data');
  if(action==4){
    method='POST';
    actionUrl = "{{ url('terimaRequest') }}";
    data = $('#form_data_modal');
  }
  $.ajax({
    url: actionUrl,
    data: data.serialize(),
    cache: false,
    type: method,
    success: function(result){
//      console.log(result);
     var result = eval('('+result+')');
      if (result.success){
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false   
        }, function(){ location.reload(); });
      }
      else{
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false
        });
      }
    },
      error: function (request, status, error) {
        console.log(request.responseText);
        swal({
            title: "Error!",
            text: error,
            type: "error",
            closeOnConfirm: false,
            closeOnCancel: false
          });
      }
  });
}

</script>
@endsection