@extends('layouts.app', ['class' => 'bg-default'])

@section('content')
<div class="header bg-gradient-primary py-7 py-lg-1">
  <div class="justify-content-center m-4" style="width: 98%">
    <div><h3 class="text-white">Daftar {{$feature_name}}</h3></div>
    <div class="row table-responsive bg-white p-3">
    	<table class="table table-striped table-hover table-sm table-bordered" id="myTable">
    		<thead class="text-center">
    			<th>ID</th>
	    		<th>Tanggal</th>
          <th>Estimasi Mulai</th>
          <th>Estimasi Selesai</th>
          <th>Status</th>
          <th>Pelanggan</th>
          <th>Permintaan</th>
          @if(Auth::user()->privilege_id==1)
            <th>Prioritas</th>
            <th>Aksi</th>
          @endif
    		</thead>
    		<tbody>
    			@foreach($schedules as $data)
    			<tr>
    				<td>{{$data->id}}</td>
    				<td>{{ date('d-m-Y', strtotime($data->worktime->waktu_mulai)) }}</td>
            <td>{{ date('H:i', strtotime($data->worktime->waktu_mulai)) }}</td>
            <td>{{ date('H:i', strtotime($data->worktime->waktu_selesai)) }}</td>
            <td>
              @if($data->status_jadwal=='terjadwal')
                <button class="btn btn-primary btn-sm">{{ucfirst($data->status_jadwal) }}</button>
              @elseif($data->status_jadwal=='proses')
                <button class="btn btn-info btn-sm">{{ucfirst($data->status_jadwal) }}</button>
              @elseif($data->status_jadwal=='selesai')
                <button class="btn btn-success btn-sm">{{ucfirst($data->status_jadwal) }}</button>
              @elseif($data->status_jadwal=='batal')
                <button class="btn btn-danger btn-sm">{{ucfirst($data->status_jadwal) }}</button>
              @endif
            </td>
            <td>{{ ucfirst($data->request->pelanggan->name) }}</td>
            <td>{{ $data->request->id }}-({{ date('d-m-Y', strtotime($data->request->tanggal_transaksi)) }})</td>
            @if(Auth::user()->privilege_id==1)
              <td>
                @if(isset($listNilai))
                  @php
                    $prioritas=$data->getRank($listNilai)
                  @endphp
                  @if($prioritas==1)
                    <button class="btn btn-sm btn-success">{{ $data->getRank($listNilai) }}</button>
                  @elseif($prioritas==2)
                    <button class="btn btn-sm btn-primary">{{ $data->getRank($listNilai) }}</button>
                  @elseif($prioritas==3)
                    <button class="btn btn-sm btn-warning">{{ $data->getRank($listNilai) }}</button>
                  @elseif($prioritas>3)
                    <button class="btn btn-sm">{{ $data->getRank($listNilai) }}</button>
                  @endif
                @else
                  Kosong
                @endif
              </td>
              <td>
              @if($akses->pivot->update)
                @if(!$data->waktu_mulai_aktual)
                  <button type="button" class="btn btn-success btn-sm" onclick="updateStatus({{ $data->id }},1)">Mulai</button>
                  <button type="button" id="btn-ubah" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#form-modal" onclick="showFormUpdateOrDelete(1,'{{ $data->id }}')">Ubah</button>
                @endif
                @if(!$data->waktu_selesai_aktual && $data->waktu_mulai_aktual)
                  <button type="button" class="btn btn-success btn-sm" onclick="updateStatus({{ $data->id }},2)">Selesai</button>
                @endif
              @endif
              </td>
            @endif
    			</tr>
    			@endforeach
    		</tbody>
    	</table>
    </div>
  </div>
</div>

<!-- Start Modal -->
  <div class="modal fade" id="form-modal" tabindex="-1" role="dialog" aria-labelledby="form-modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_title">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form_data" enctype="multipart/form-data" method="POST" action="">
            @csrf
            <div class="form-group row">
              <input type="hidden" class="masukkan" name="id" id="id" value="">
              <div class="input-group">
                <div class="col-sm-2 d-flex align-items-center">Tanggal</div>
                <div class="col-sm-10">
                  <input class="form-control masukkan datepicker" data-date-format="dd-mm-yyyy" placeholder="Masukan tanggal beli" type="text" name="tanggal" id="tanggal" onclick="getWorktime()" value="<?php echo date('d-m-Y'); ?>"> 
                </div> 
              </div>
              <div class="input-group">
                <div class="col-sm-2 d-flex align-items-center">Waktu</div>
                <div class="col-sm-10">
                  <select class="custom-select masukkan" id="waktu" name="waktu">
                    <option value='' selected>Pilih jam pengambilan</option>
                  </select>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Batal</button>
          <input type="submit" id="btn_action" class="btn btn-success" value="Simpan">
          </form>
        </div>
      </div>
    </div>
  </div>
<!-- End Modal -->
<div class="container bg-default mt--10 pb-5"></div>
@include('layouts.footers.auth')
<script>
  getWorktime();
$(document).ready( function () {
    $('#myTable').DataTable({"aaSorting": [1]});
});
var feature_name='{{$feature_name}}';
var urlRoute="{{url('schedule')}}";

function getWorktime(){
  var tanggal=$('#tanggal').val();
  $.ajax({
    url: "{{ url('getworktime') }}",
    data: {"_token":"{{ csrf_token() }}","tanggal":tanggal},
    cache: false,
    type: "GET",
    success: function(result){
      var result = eval('('+result+')');
      $('#waktu').html(result.option);
    },
      error: function (request, status, error) {
        console.log(request.responseText);
        swal({
            title: "Error!",
            text: error,
            type: "error",
            closeOnConfirm: false,
            closeOnCancel: false
          });
      }
  });
}
function updateStatus(id,atribut){
  var actionUrl=urlRoute+"/"+id;
  $.ajax({
    url: actionUrl,
    data: {"_token":"{{ csrf_token() }}","id":id,"atribut":atribut},
    cache: false,
    type: "PUT",
    success: function(result){
     var result = eval('('+result+')');
      if (result.success){
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false   
        }, function(){ location.reload(); });
      }
      else{
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false
        });
      }
    },
      error: function (request, status, error) {
        console.log(request.responseText);
        swal({
            title: "Error!",
            text: error,
            type: "error",
            closeOnConfirm: false,
            closeOnCancel: false
          });
      }
  });
}
function showFormInput() {
  $(".masukkan").val("");
  $("#modal_title").html("Form Data "+feature_name+" Baru");
  $("#form_data").attr("action", "javascript:actionsFunction(0,0)");
  $("#btn_action").attr("value", "Simpan Data");
  $("#btn_action").removeClass("btn-danger").addClass("btn-success");

}
function showFormUpdateOrDelete(action,id) {
  $("#icon_display").removeClass();
  if(action==1){ //ubah data
    $(".masukkan").prop('disabled', false);
    $("#btn_action").attr("value", "Ubah Data");
    $("#modal_title").html("Form Ubah Data "+feature_name);
    $("#btn_action").removeClass("btn-danger").addClass("btn-success");
  }else if(action==2){ //hapus data
    $(".masukkan").prop('disabled', true);
    $("#id").prop('disabled', false);
    $("#btn_action").attr("value", "Hapus Data");
    $("#modal_title").html("Form Hapus Data "+feature_name);
    $("#btn_action").removeClass("btn-success").addClass("btn-danger");
  }
  $.ajax({
    type: "GET",
    url: urlRoute+"/"+id,
    cache: false,
    success: function(result){
      var result = eval('('+result+')');
      if(result.success){
        $("#form_data").attr("action", "javascript:actionsFunction("+action+","+result.id+")");
        $("#id").val(result.id);
        $("#tanggal").val(result.tanggal);
      }else{
        swal({
          title: "Gagal Mengambil Data {{$feature_name}} !",
          text: result.pesan,
          type: "error"
        });
      }
    },
    error: function (request, status, error) {
      console.log(request.responseText);
      swal({
          title: "Error!",
          text: error,
          type: "error",
          closeOnConfirm: false,
          closeOnCancel: false
      });
    }
  });
}
function actionsFunction(action,id) {
  var method="";
  var actionUrl=urlRoute+"/"+id;
  if(action==0){
    method='POST';
    actionUrl=urlRoute;
  }else if(action==1){
    method='PUT';
  }else if(action==2){
    method='DELETE';
  }
  var data = $('#form_data');
  $.ajax({
    url: actionUrl,
    data: data.serialize(),
    cache: false,
    type: method,
    success: function(result){
     var result = eval('('+result+')');
      if (result.success){
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false   
        }, function(){ location.reload(); });
      }
      else{
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false
        });
      }
    },
      error: function (request, status, error) {
        console.log(request.responseText);
        swal({
            title: "Error!",
            text: error,
            type: "error",
            closeOnConfirm: false,
            closeOnCancel: false
          });
      }
  });
}
</script>
@endsection