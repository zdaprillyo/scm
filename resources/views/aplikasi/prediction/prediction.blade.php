@extends('layouts.app', ['class' => 'bg-default'])

@section('content')
<div class="header bg-gradient-primary py-7 py-lg-1">
  <div class="justify-content-center m-4" style="width: 98%">
    @if($akses->pivot->create)
      <button type="button" class="btn btn-info" data-toggle="modal" data-target="#form-modal" onclick="showFormInput()">Tambah {{$feature_name}}</button>
    @endif
    <div><h3 class="text-white">Daftar {{$feature_name}}</h3></div>
    <div class="row table-responsive bg-white p-3">
    	<table class="table table-striped table-hover table-sm table-bordered" id="myTable">
        <thead class="text-center">
          <th>ID</th>
          <th>Dari Tanggal</th>
          <th>Hingga Tanggal</th>
          <th>Tanggal Prediksi</th>
          <th>Prediksi Pembayaran</th>
          <th>Pembayaran Aktual</th>
          <th>Akurasi
          <th>Admin</th>
        </thead>
        <tbody>
          @foreach($predictions as $data)
          <tr>
            <td>{{ $data->id}} </td>
            <td>{{ date('d-m-Y', strtotime($data->dari_tanggal))   }}</td>
            <td>{{ date('d-m-Y', strtotime($data->hingga_tanggal)) }}</td>
            <td>{{ date('d-m-Y', strtotime($data->utk_tanggal))    }}</td>
            <td class="text-right">{{ number_format($data->prediksi_pembayaran,0,',','.') }}</td>
            <td class="text-right">{{ number_format($data->getPembayaranAktual(),0,',','.') }}</td>
            <td>{{ $data->getAkurasi() }}%</td>
            <td>{{ ucfirst($data->user->name) }}</td>
          </tr>   
          @endforeach
        </tbody>
    	</table>
    </div>
  </div>
</div>

<!-- Start Modal -->
  <div class="modal fade" id="form-modal" tabindex="-1" role="dialog" aria-labelledby="form-modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_title">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form_data" enctype="multipart/form-data" method="POST" action="">
            @csrf
            <div class="form-group row">
              <input type="hidden" class="masukkan" name="id" id="id" value="">
              <div class="input-group">
                <div class="col-sm-4 d-flex align-items-center">Tanggal Prediksi</div>
                <div class="col-sm-8">
                  <input class="form-control masukkan datepicker" data-date-format="dd-mm-yyyy" placeholder="Masukan tanggal prediksi" type="text" name="utk_tanggal" id="utk_tanggal">  
                </div> 
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Batal</button>
          <input type="submit" id="btn_action" class="btn btn-success" value="Simpan">
          </form>
        </div>
      </div>
    </div>
  </div>
<!-- End Modal -->
<div class="container bg-default mt--10 pb-5"></div>
@include('layouts.footers.auth')
<script>
$(document).ready( function () {
    $('#myTable').DataTable({"aaSorting": [2]});
});
var feature_name='{{$feature_name}}';
var urlRoute="{{url('prediction')}}";

function showFormInput() {
  $(".masukkan").val("");
  $("#modal_title").html("Form Data "+feature_name+" Baru");
  $("#form_data").attr("action", "javascript:actionsFunction(0,0)");
  $("#btn_action").attr("value", "Simpan Data");
  $("#btn_action").removeClass("btn-danger").addClass("btn-success");
  $(".masukkan").prop('disabled', false);
}
function showFormUpdateOrDelete(action,id) {
  $("#icon_display").removeClass();
  if(action==1){ //ubah data
    $(".masukkan").prop('disabled', false);
    $("#btn_action").attr("value", "Ubah Data");
    $("#modal_title").html("Form Ubah Data "+feature_name);
    $("#btn_action").removeClass("btn-danger").addClass("btn-success");
  }else if(action==2){ //hapus data
    $(".masukkan").prop('disabled', true);
    $("#id").prop('disabled', false);
    $("#btn_action").attr("value", "Hapus Data");
    $("#modal_title").html("Form Hapus Data "+feature_name);
    $("#btn_action").removeClass("btn-success").addClass("btn-danger");
  }
  $.ajax({
    type: "GET",
    url: urlRoute+"/"+id,
    cache: false,
    success: function(result){
      var result = eval('('+result+')');
      if(result.success){
        $("#form_data").attr("action", "javascript:actionsFunction("+action+","+result.id+")");
        $("#id").val(result.id);
        $("#nama").val(result.nama);
        $("#tanggal_beli").val(result.tanggal_beli);
        $("#qty").val(result.qty);
        $("#harga").val(result.harga);
        $("#status").val(result.status);
      }else{
        swal({
          title: "Gagal Mengambil Data {{$feature_name}} !",
          text: result.pesan,
          type: "error"
        });
      }
    },
    error: function (request, status, error) {
      swal({
          title: "Error!",
          text: error,
          type: "error",
          closeOnConfirm: false,
          closeOnCancel: false
      });
    }
  });
}
function actionsFunction(action,id) {
  var method="";
  var actionUrl=urlRoute+"/"+id;
  if(action==0){
    method='POST';
    actionUrl=urlRoute;
  }else if(action==1){
    method='PUT';
  }else if(action==2){
    method='DELETE';
  }
  var data = $('#form_data');
  $.ajax({
    url: actionUrl,
    data: data.serialize(),
    cache: false,
    type: method,
    success: function(result){
//      console.log(result);
     var result = eval('('+result+')');
      if (result.success){
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false   
        }, function(){ location.reload(); });
      }
      else{
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false
        });
      }
    },
      error: function (request, status, error) {
        console.log(request.responseText);
        swal({
            title: "Error!",
            text: error,
            type: "error",
            closeOnConfirm: false,
            closeOnCancel: false
          });
      }
  });
}
function updateStatus(id,aksi){
  var actionUrl=urlRoute+"/"+id;
  $.ajax({
    url: actionUrl,
    data: {"_token":"{{ csrf_token() }}","id":id,"status_bbm":aksi},
    cache: false,
    type: "PUT",
    success: function(result){
     var result = eval('('+result+')');
      if (result.success){
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false   
        }, function(){ location.reload(); });
      }
      else{
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false
        });
      }
    },
      error: function (request, status, error) {
        console.log(request.responseText);
        swal({
            title: "Error!",
            text: error,
            type: "error",
            closeOnConfirm: false,
            closeOnCancel: false
          });
      }
  });
}
</script>
@endsection