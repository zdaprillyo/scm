@extends('layouts.app', ['class' => 'bg-default'])

@section('content')
<div class="header bg-gradient-primary py-7 py-lg-1">
  <div class="justify-content-center m-4" style="width: 98%">
    @if($akses->pivot->create)
      <a class="btn btn-info" href="{{ url('loading') }}/create">Tambah {{$feature_name}}</a>
    @endif
    <div><h3 class="text-white">Daftar {{$feature_name}}</h3></div>
    <div class="row table-responsive bg-white p-3">
    	<table class="table table-striped table-hover table-sm table-bordered" id="myTable">
    		<thead class="text-center">
    			<th>ID</th>
	    		<th>Waktu Mulai</th>
          <th>Waktu Selesai</th>
          <th>Status</th>
          <th>Pengangkut</th>
          <th>No</th>
          <th>Sopir</th>
          <th>Telepon</th>
          <th>ID Permintaan</th>
          <th>Aksi</th>
    		</thead>
    		<tbody>
    			@foreach($loadings as $data)
    			<tr>
    				<td>{{$data->id}}</td>
    				<td>{{ date('d-m-Y H:s', strtotime($data->waktu_mulai)) }}</td>
            <td>{{ date('d-m-Y H:s', strtotime($data->waktu_selesai)) }}</td>
            <td>
              @if($data->status=='dimuat')
                <button class="btn btn-info btn-sm">{{ ucfirst($data->status) }}</button>
              @elseif($data->status=='dikirim')
                <button class="btn btn-success btn-sm">{{ ucfirst($data->status) }}</button>
              @elseif($data->status=='dibongkar')
                <button class="btn btn-danger btn-sm">{{ ucfirst($data->status) }}</button>
              @endif
            </td>
            <td>{{ ucfirst($data->jenis_pengangkut) }}</td>
            <td>{{ $data->no_pengangkut }}</td>
            <td>{{ ucfirst($data->nama_sopir) }}</td>
            <td>{{ $data->telepon }}</td>
            <td>{{ $data->request_id }}-{{ ucfirst($data->request->pelanggan->name) }} ({{ date('d-m-Y', strtotime($data->request->tanggal_transaksi)) }})</td>
    				<td>
              @if($akses->pivot->read)
                <button type="button" id="btn-ubah" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-detail" onclick="showLoadingDetail('{{ $data->id }}')">Detail</button>
              @endif
              @if($data->status=='dimuat'  && $akses->pivot->update)
                <button type="button" id="btn-ubah" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#form-modal" onclick="showFormUpdateOrDelete(1,'{{ $data->id }}')">Ubah</button>
              @endif
              @if($akses->pivot->delete)
                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#form-modal" onclick="showFormUpdateOrDelete(2,'{{ $data->id }}')">Hapus</button>
              @endif
    				</td>
    			</tr>
    			@endforeach
    		</tbody>
    	</table>
    </div>
  </div>
</div>

<!-- Start Form Modal -->
  <div class="modal fade" id="form-modal" tabindex="-1" role="dialog" aria-labelledby="form-modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_title">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form_data" enctype="multipart/form-data" method="POST" action="">
            @csrf
            <div class="form-group row">
              <input type="hidden" class="masukkan" name="id" id="id" value="">
              <div class="input-group">
                <div class="col-sm-3 d-flex align-items-center">Waktu Mulai</div>
                <div class="col-sm-9">
                  <input class="form-control masukkan" style="height: 38px" placeholder="Masukan tanggal beli" type="text" name="waktu_mulai" id="waktu_mulai">  
                </div> 
              </div>
              <div class="input-group">
                <div class="col-sm-3 d-flex align-items-center">Waktu Selesai</div>
                <div class="col-sm-9">
                  <input class="form-control masukkan" style="height: 38px" placeholder="Masukan tanggal beli" type="text" name="waktu_selesai" id="waktu_selesai">  
                </div> 
              </div>
              <div class="input-group">
                <div class="col-sm-3 d-flex align-items-center">Status</div>
                <div class="col-sm-9">
                  <select class="form-control masukkan" id="status" name="status">
                    <option value="dimuat">Dimuat</option>
                    <option value="dikirim">Dikirim</option>
                    <option value="dibongkar">Dibongkar</option>
                  </select>
                </div>
              </div>
              <div class="input-group">
                <div class="col-sm-3 d-flex align-items-center">Pengangkut</div>
                <div class="col-sm-9">
                  <select class="form-control masukkan" id="jenis_pengangkut" name="jenis_pengangkut" onchange="inputFilter()">
                    <option value="truk" selected>Truk</option>
                    <option value="kontainer">Kontainer</option>
                  </select>
                </div>
              </div>
              <div class="input-group">
                <div class="col-sm-3 d-flex align-items-center">Nomor</div>
                <div class="col-sm-9">
                  <input class="form-control masukkan" placeholder="Masukan nomor pengangkut" type="text" name="no_pengangkut" id="no_pengangkut">  
                </div> 
              </div>
              <div class="input-group">
                <div class="col-sm-3 d-flex align-items-center">Merk Pengangkut</div>
                <div class="col-sm-9">
                  <input class="form-control masukkan" placeholder="Masukan merk pengangkut" type="text" name="merk_pengangkut" id="merk_pengangkut">  
                </div> 
              </div>
              <div class="input-group">
                <div class="col-sm-3 d-flex align-items-center">Segel</div>
                <div class="col-sm-9">
                  <input class="form-control masukkan kontainer-info" placeholder="Masukan nomor segel" type="text" name="no_segel" id="no_segel">  
                </div> 
              </div>
              <div class="input-group">
                <div class="col-sm-3 d-flex align-items-center">Sopir</div>
                <div class="col-sm-9">
                  <input class="form-control masukkan truk-info" placeholder="Masukan nama sopir" type="text" name="nama_sopir" id="nama_sopir">  
                </div> 
              </div>
              <div class="input-group">
                <div class="col-sm-3 d-flex align-items-center">Telpon</div>
                <div class="col-sm-9">
                  <input class="form-control masukkan truk-info" placeholder="Masukan telepon sopir" type="text" name="telepon" id="telepon">  
                </div> 
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Batal</button>
          <input type="submit" id="btn_action" class="btn btn-success" value="Simpan">
          </form>
        </div>
      </div>
    </div>
  </div>
<!-- End Form Modal -->

<!-- Start Detail Modal -->
  <div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="form-modal" aria-hidden="true">
    <div class="modal-dialog modal-dialogcentered- modal-lg" role="document">
      <div class="modal-content bg-default">
        <div class="modal-header">
          <h5 class="modal-title text-white" id="modal_title">Detail Muatan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" class="text-white">&times;</span>
          </button>
        </div>
        <div class="modal-body text-white">
          <div class="row">
            <div class="col">
              <div class="form-group row">
                <div class="input-group">
                  <div class="col-sm-5 d-flex align-items-center">ID Muat</div>
                  <div class="col-sm-5">: <span id="id-modal-loading">1231</span></div>
                </div>
                <div class="input-group">
                  <div class="col-sm-5 d-flex align-items-center">Waktu Mulai</div>
                  <div class="col-sm-7">: <span id="waktu-mulai-modal-loading">{{ date('d-m-Y H:i:s') }}</span></div>
                </div>
                <div class="input-group">
                  <div class="col-sm-5 d-flex align-items-center">Waktu Selesai</div>
                  <div class="col-sm-7">: <span id="waktu-selesai-modal-loading">{{ date('d-m-Y H:i:s') }}</span></div>
                </div>
                <div class="input-group">
                  <div class="col-sm-5 d-flex align-items-center">Pengangkut</div>
                  <div class="col-sm-7">: <span id="pengangkut-modal-loading" class="text-kapital">Truk</span></div>
                </div>
                <div class="input-group">
                  <div class="col-sm-5 d-flex align-items-center">Status</div>
                  <div class="col-sm-7">: <span id="status-modal-loading" class="text-kapital">dikirim</span></div>
                </div>
                <div class="input-group">
                  <div class="col-sm-5 d-flex align-items-center">Nomor</div>
                  <div class="col-sm-7">: <span id="no-modal-loading">KH1234XD</span></div>
                </div>
              </div>
            </div>
            <div class="col">
              <div class="form-group row">
                <div class="input-group">
                  <div class="col-sm-5 d-flex align-items-center">Merk</div>
                  <div class="col-sm-7">: <span id="merk-modal-loading">Hino</span></div>
                </div>
                <div class="input-group">
                  <div class="col-sm-5 d-flex align-items-center">Segel</div>
                  <div class="col-sm-5">: <span id="segel-modal-loading">1231</span></div>
                </div>
                <div class="input-group">
                  <div class="col-sm-5 d-flex align-items-center">Sopir</div>
                  <div class="col-sm-7">: <span id="sopir-modal-loading">Aprillyo</span></div>
                </div>
                <div class="input-group">
                  <div class="col-sm-5 d-flex align-items-center">Telepon</div>
                  <div class="col-sm-7">: <span id="telepon-modal-loading">12345678901234</span></div>
                </div>
                <div class="input-group">
                  <div class="col-sm-5 d-flex align-items-center">ID Permintaan</div>
                  <div class="col-sm-7">: <span id="permintaan-modal-packing">1</span></div>
                </div>
              </div>
            </div>
          </div>
          <label>Detail Barang</label>
          <table class="table table-striped table-bordered table-hover table-dark">
            <thead class="text-center">
              <th>ID</th>
              <th>ID FProduct</th>
              <th>Nama</th>
              <th>Qty</th>
              <th>Harga</th>
              <th>Sub Total</th>
            </thead>
            <tbody id="isi-table-modal-detail-loading">
              <tr>
                <td>12</td>
                <td>123</td>
                <td>Kardus</td>
                <td style='text-align:right'>4000</td>
                <td style='text-align:right'>10000</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </div>
  </div>
<!-- End Detail Modal -->
<div class="container bg-default mt--10 pb-5"></div>
@include('layouts.footers.auth')
<script>
  inputFilter();
$(document).ready( function () {
    $('#myTable').DataTable({"aaSorting": [1]});
});
var feature_name='{{$feature_name}}';
var urlRoute="{{url('loading')}}";

function inputFilter(jenis_angkutan){
  jenis_angkutan=$('#jenis_pengangkut').val();
  if(jenis_angkutan=='truk'){
    $(".truk-info").prop('readonly', false);
    $(".kontainer-info").prop('readonly', true);
  }else if(jenis_angkutan=='kontainer'){
    $(".truk-info").prop('readonly', true);
    $(".kontainer-info").prop('readonly', false);
  }
}

function showFormInput() {
  $(".masukkan").val("");
  $("#modal_title").html("Form Data "+feature_name+" Baru");
  $("#form_data").attr("action", "javascript:actionsFunction(0,0)");
  $("#btn_action").attr("value", "Simpan Data");
  $("#btn_action").removeClass("btn-danger").addClass("btn-success");
}
function showLoadingDetail(id){
  $.ajax({
    type: "GET",
    url: urlRoute+"/"+id,
    cache: false,
    success: function(result){
      var result = eval('('+result+')');
      if(result.success){
        $("#id-modal-loading").html(result.id);
        $("#waktu-mulai-modal-loading").html(result.waktu_mulai);
        $("#waktu-selesai-modal-loading").html(result.waktu_selesai);
        $("#status-modal-loading").html(result.status);
        $("#pengangkut-modal-loading").html(result.jenis_pengangkut);
        $("#no-modal-loading").html(result.no_pengangkut);
        $("#merk-modal-loading").html(result.merk_pengangkut);
        $("#segel-modal-loading").html(result.no_segel);
        $("#sopir-modal-loading").html(result.nama_sopir);
        $("#telepon-modal-loading").html(result.telepon);
        $("#permintaan-modal-packing").html(result.request_id);
        $("#isi-table-modal-detail-loading").html(result.isi_table);
      }else{
        swal({
          title: "Gagal Mengambil Data {{$feature_name}} !",
          text: result.pesan,
          type: "error"
        });
      }
    },
    error: function (request, status, error) {
      console.log(request.responseText);
      swal({
          title: "Error!",
          text: error,
          type: "error",
          closeOnConfirm: false,
          closeOnCancel: false
      });
    }
  });
}
function showFormUpdateOrDelete(action,id) {
  $("#icon_display").removeClass();
  if(action==1){ //ubah data
    $(".masukkan").prop('disabled', false);
    $("#btn_action").attr("value", "Ubah Data");
    $("#modal_title").html("Form Ubah Data "+feature_name);
    $("#btn_action").removeClass("btn-danger").addClass("btn-success");
  }else if(action==2){ //hapus data
    $(".masukkan").prop('disabled', true);
    $("#id").prop('disabled', false);
    $("#btn_action").attr("value", "Hapus Data");
    $("#modal_title").html("Form Hapus Data "+feature_name);
    $("#btn_action").removeClass("btn-success").addClass("btn-danger");
  }
  $.ajax({
    type: "GET",
    url: urlRoute+"/"+id,
    cache: false,
    success: function(result){
      var result = eval('('+result+')');
      if(result.success){
        $("#form_data").attr("action", "javascript:actionsFunction("+action+","+result.id+")");
        $("#id").val(result.id);
        $("#waktu_mulai").val(result.waktu_mulai);
        $("#waktu_selesai").val(result.waktu_selesai);
        $("#status").val(result.status);
        $("#jenis_pengangkut").val(result.jenis_pengangkut);
        $("#no_pengangkut").val(result.no_pengangkut);
        $("#merk_pengangkut").val(result.merk_pengangkut);
        $("#no_segel").val(result.no_segel);
        $("#nama_sopir").val(result.nama_sopir);
        $("#telepon").val(result.telepon);
        inputFilter();
      }else{
        swal({
          title: "Gagal Mengambil Data {{$feature_name}} !",
          text: result.pesan,
          type: "error"
        });
      }
    },
    error: function (request, status, error) {
      swal({
          title: "Error!",
          text: error,
          type: "error",
          closeOnConfirm: false,
          closeOnCancel: false
      });
    }
  });
}
function actionsFunction(action,id) {
  var method="";
  var actionUrl=urlRoute+"/"+id;
  if(action==0){
    method='POST';
    actionUrl=urlRoute;
  }else if(action==1){
    method='PUT';
  }else if(action==2){
    method='DELETE';
  }
  var data = $('#form_data');
  $.ajax({
    url: actionUrl,
    data: data.serialize(),
    cache: false,
    type: method,
    success: function(result){
     var result = eval('('+result+')');
      if (result.success){
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false   
        }, function(){ location.reload(); });
      }
      else{
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false
        });
      }
    },
      error: function (request, status, error) {
        console.log(request.responseText);
        swal({
            title: "Error!",
            text: error,
            type: "error",
            closeOnConfirm: false,
            closeOnCancel: false
          });
      }
  });
}

</script>
@endsection