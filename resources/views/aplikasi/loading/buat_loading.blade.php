@extends('layouts.app', ['class' => 'bg-default'])

@section('content')
<div class="header bg-gradient-primary py-7 py-lg-1">
  <div class="justify-content-center m-4" style="width: 98%">
    <div class="row pb-2 mr-4">
      <div class="col">
        <a href="{{ url('loading') }}" class="btn btn-danger btn-outline btn-lg">Batal</a>
      </div>
      <div class="col text-right">
        <button type="button" class="btn btn-success" name="buat-nota" id="buat-nota" onclick="buatPack()">Muat</button>
      </div>
    </div>
    <div><h3 class="text-white">Daftar Muatan > Buat Muatan</h3>
    </div>
    <div id="main" class="mr-4">
      <form id="form_data" enctype="multipart/form-data" method="post" class="form-horizontal text-white" action="">
        @csrf
        <div class="form-group">
          <div class="form-group row">
            <div class="col">
              <label for="waktu_mulai">Waktu Mulai</label>
              <input class="form-control tanggal" type="text" name="waktu_mulai" id="waktu_mulai" placeholder="Masukan waktu mulai">
              <label for="waktu_selesai">Waktu Selesai</label>
              <input class="form-control tanggal" type="text" name="waktu_selesai" id="waktu_selesai" placeholder="Masukan waktu selesai">
              <label for="jenis_pengangkut">Jenis Pengangkut</label>
              <select class="form-control" id="jenis_pengangkut" name="jenis_pengangkut" onchange="inputFilter()">
                <option value="truk" selected>Truk</option>
                <option value="kontainer">Kontainer</option>
              </select>
            </div>
            <div class="col">
              <label for="request_id">ID Permintaan</label>
              <select class="form-control" id="request_id" name="request_id">
                @foreach($requests as $request)
                  <option value="{{ $request->id }}">{{ $request->id }}-{{ ucfirst($request->pelanggan->name) }} ({{ date('d-m-Y',strtotime($request->tanggal_transaksi)) }})</option>
                @endforeach
              </select>
              <label for="no_pengangkut">Nomor Pengangkut</label>
              <input class="form-control" type="text" name="no_pengangkut" id="no_pengangkut" placeholder="Masukan nomor pengangkut">
              <label for="nama-barang">Merk Pengangkut</label>
              <input class="form-control" type="text" name="merk_pengangkut" id="merk_pengangkut" placeholder="Masukan mark pengangkut">
            </div>
            <div class="col">
              <label for="no_segel">No Segel</label>
              <input class="form-control kontainer-info" type="text" name="no_segel" id="no_segel" placeholder="Masukan nomor segel kontainer">
              <label for="nama_sopir">Nama Sopir</label>
              <input class="form-control truk-info" type="text" name="nama_sopir" id="nama_sopir" placeholder="Masukan nama sopir truk">
              <label for="telepon">Telepon Sopir</label>
              <input class="form-control truk-info" type="text" name="telepon" id="telepon" placeholder="Masukan telepon sopir truk">
            </div>
          </div>
        </div>
      </form>
      <div id="table-output" class="table-responsive bg-white text-default p-3">
        <table id="myTable" class="table table-striped table-bordered table-hover table-sm" style="width: 50%; margin: 0px auto;">
          <thead class="text-center">
              <th>ID FProduk</th>
              <th>Tanggal Buat</th>
              <th>Nama</th>
              <th>Qty</th>
              <th>Aksi</th>
          </thead>
          <tbody id="isi-tabel"></tbody>
        </table>  
      </div>
      <button style="margin: 10px auto;" id="btn-showInput" type="button" class="btn btn-info mb-2" data-toggle="modal" data-target="#form-modal">Tambah Barang</button>  
    </div>
  </div>
</div>
<!-- Start Modal -->
  <div class="modal fade" id="form-modal" tabindex="-1" role="dialog" aria-labelledby="form-modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_title">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="form-group row">
              <div class="input-group">
                <div class="col-sm-3 d-flex align-items-center">ID FProduk</div>
                <div class="col-sm-9"><input type="text" class="form-control masukkan" id="input-fproduk-id" name="input-fproduk-id" placeholder="Masukkan id fproduk" onkeyup="getFprodukInfo()"></div>
              </div>
              <div class="input-group">
                <div class="col-sm-3 d-flex align-items-center">Tanggal Buat</div>
                <div class="col-sm-9"><input type="text" class="form-control masukkan" id="input-fproduk-tanggal" name="input-fproduk-tanggal" readonly="true"></div>
              </div>
              <div class="input-group">
                <div class="col-sm-3 d-flex align-items-center">Nama</div>
                <div class="col-sm-9"><input type="text" class="form-control masukkan" id="input-fproduk-nama" name="input-fproduk-nama" readonly="true"></div>
              </div>
              <div class="input-group">
                <div class="col-sm-3 d-flex align-items-center">Qty</div>
                <div class="col-sm-9"><input type="text" class="form-control masukkan" id="input-fproduk-qty" name="input-fproduk-qty" readonly="true"></div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Batal</button>
          <button type="button" class="btn btn-success" data-dismiss="modal" onclick="addDetail()">Tambah</button>
        </div>
      </div>
    </div>
  </div>
<!-- End Modal -->
<div class="container bg-default mt--10 pb-5"></div>
@include('layouts.footers.auth')
<script>
  inputFilter();
  function inputFilter(jenis_angkutan){
  jenis_angkutan=$('#jenis_pengangkut').val();
    if(jenis_angkutan=='truk'){
      $(".truk-info").prop('disabled', false);
      $(".kontainer-info").prop('disabled', true);
    }else if(jenis_angkutan=='kontainer'){
      $(".truk-info").prop('disabled', true);
      $(".kontainer-info").prop('disabled', false);
    }
  }
  function addDetail(){
    var id=$("#input-fproduk-id").val();
    var tgl=$("#input-fproduk-tanggal").val();
    var nama=$("#input-fproduk-nama").val();
    var qty=$("#input-fproduk-qty").val();
    if(tgl!="" && qty!="" && nama!=""){
      $("#isi-tabel").append("<tr class='text-center'><td>"+id+"</td><td>"+tgl+"</td><td>"+nama+"</td><td class='text-right'>"+qty+"</td><td><button type=\"button\" class=\"btn btn-danger btn-sm\" onclick=\"deleteDetail(this)\">X</button></td></tr>");
    }
    $('.masukkan').val("");
  }
  function deleteDetail(ctl) { $(ctl).parents("tr").remove(); }

  document.querySelector("#input-fproduk-id").addEventListener("keypress", function (evt) {
    if (evt.which < 48 || evt.which > 57){ evt.preventDefault(); }
  });

  document.querySelector("#telepon").addEventListener("keypress", function (evt) {
    if (evt.which < 48 || evt.which > 57){ evt.preventDefault(); }
  });

  $('.tanggal').datetimepicker({
    format: 'DD-MM-YYYY HH:mm',
    icons: {
      time: 'fa fa-clock-o',
      date: 'fa fa-calendar',
      up: 'fa fa-chevron-up',
      down: 'fa fa-chevron-down',
      previous: 'fa fa-chevron-left',
      next: 'fa fa-chevron-right',
      today: 'fa fa-check',
      clear: 'fa fa-trash',
      close: 'fa fa-times'
    }
  });
  function getFprodukInfo(){
    var id=$('#input-fproduk-id').val();
    if(id){
      $.ajax({
        type: "GET",
        url: "{{ url('fproduct') }}"+"/"+id,
        cache: false,
        success: function(result){
          var result = eval('('+result+')');
          if(result.success){
            $("#input-fproduk-tanggal").val(result.tanggal_dibuat);
            $("#input-fproduk-nama").val(result.nama);
            $("#input-fproduk-qty").val(result.qty);
          }else{
            $("#input-fproduk-tanggal").val('');
            $("#input-fproduk-nama").val('');
            $("#input-fproduk-qty").val('');
          }
        },
        error: function (request, status, error) {
          console.log(request.responseText);
          swal({
              title: "Error!",
              text: error,
              type: "error",
              closeOnConfirm: false,
              closeOnCancel: false
          });
        }
      });
    }
  }
  function getDataFromTable(){
    //gets table
    var table = document.getElementById('myTable');
    //gets rows of table
    var rowLength = table.rows.length;
    //loops through rows    
    for (i = 0; i < rowLength; i++){
      if(i>0){
         //gets cells of current row
        var cells = table.rows.item(i).cells;
         //loops through each cell in current row
        for(var j = 0; j < cells.length; j++){
            /* get your cell info here */
          cellVal = cells.item(j).innerHTML;
          if(j==0){
            // $("#nama-barang").push(cellVal); 
            $("#form_data").append("<input type=\"hidden\" class=\"hidden-data\" name=\"fproduct-id[]\" id=\"fproduct-id\" value=\""+cellVal+"\">");
          }
        }
      }
    }
  }
  function buatPack(){
    getDataFromTable();
    $.ajax({
      url: "{{ url('loading') }}",
      cache: false,
      type: "POST",
      data: $('#form_data').serialize(),
      success: function(result){
       var result = eval('('+result+')');
        if (result.success){
          swal({
            title: result.message_title,
            text: result.message_conten,
            type: result.message_type,
            closeOnConfirm: false,
            closeOnCancel: false   
          }, function(){ 
              var link='{{ url('loading') }}';
              location.replace(link); 
            });
        }
        else
        {
          swal({
            title: result.message_title,
            text: result.message_conten,
            type: result.message_type,
            closeOnConfirm: false,
            closeOnCancel: false
          });
          $(".hidden-data").remove();
        }
      },
        error: function (request, status, error) {
          console.error(request.responseText); 
          swal({
              title: "Error!",
              text: error,
              type: status,
              closeOnConfirm: false,
              closeOnCancel: false
            });
          $(".hidden-data").remove();
         }
    });
  }
</script>
@endsection
