@extends('layouts.app', ['class' => 'bg-default'])

@section('content')
<div class="header bg-gradient-primary py-7 py-lg-1">
  <div class="justify-content-center m-4" style="width: 98%">
    <div class="row pb-2 mr-4">
      <div class="col">
        <a href="{{ url('packing') }}" class="btn btn-danger btn-outline btn-lg">Batal</a>
      </div>
      <div class="col text-right">
        <button type="button" class="btn btn-success" name="buat-nota" id="buat-nota" onclick="buatPack()">Packing</button>
      </div>
    </div>
    <div><h3 class="text-white">Daftar Packing > Buat Packing</h3>
    </div>
    <div id="main" class="mr-4">
      <form id="form_data" enctype="multipart/form-data" method="post" class="form-horizontal text-white" action="">
        @csrf
        <div class="form-group">
          <div class="form-group row">
            <div class="col">
              <label for="waktu_mulai">Waktu Mulai</label>
              <input class="form-control tanggal" type="text" name="waktu_mulai" id="waktu_mulai">
            </div>
            <div class="col">
              <label for="waktu_selesai">Waktu Selesai</label>
              <input class="form-control tanggal" type="text" name="waktu_selesai" id="waktu_selesai">
            </div>
            <div class="col">
              <label for="nama-barang">Nama Barang</label>
              <input class="form-control" type="text" name="nama_barang" id="nama_barang">
            </div>
            <div class="col">
              <label for="bbm">BBM</label>
              <select class="form-control" id="bbm" name="bbm">
                  <option selected value="">Tidak ada</option>
                @foreach($fuels as $fuel)
                  <option value="{{$fuel->id}}">{{ ucfirst($fuel->nama) }}-{{ $fuel->id }} ({{ date('d-m-Y',strtotime($fuel->tanggal_beli)) }})</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>
      </form>
      <div id="table-output" class="table-responsive bg-white text-default p-3">
        <table id="myTable" class="table table-striped table-bordered table-hover table-sm" style="width: 50%; margin: 0px auto;">
          <thead class="text-center">
              <th>Tanggal</th>
              <th>Qty</th>
              <th>Aksi</th>
          </thead>
          <tbody id="isi-tabel"></tbody>
        </table>  
      </div>
      <button style="margin: 10px auto;" id="btn-showInput" type="button" class="btn btn-info mb-2" data-toggle="modal" data-target="#form-modal">Tambah Barang</button>  
    </div>
  </div>
</div>
<!-- Start Modal -->
  <div class="modal fade" id="form-modal" tabindex="-1" role="dialog" aria-labelledby="form-modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_title">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="form-group row">
              <div class="input-group">
                <div class="col-sm-2 d-flex align-items-center">Tanggal</div>
                <div class="col-sm-10"><input type="text" class="form-control datepicker" id="final_tanggal" name="final_tanggal" data-date-format="dd-mm-yyyy" placeholder="Masukkan tanggal" value="{{ date('d-m-Y') }}"></div>
              </div>
              <div class="input-group">
                <div class="col-sm-2 d-flex align-items-center">Qty</div>
                <div class="col-sm-10"><input type="text" class="form-control" id="final_qty" name="final_qty" placeholder="Masukkan total qty"></div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Batal</button>
          <button type="button" class="btn btn-success" data-dismiss="modal" onclick="addDetail()">Tambah</button>
        </div>
      </div>
    </div>
  </div>
<!-- End Modal -->
<div class="container bg-default mt--10 pb-5"></div>
@include('layouts.footers.auth')
<script>
  function addDetail(){
    var tgl=$("#final_tanggal").val();
    var qty=$("#final_qty").val();
    if(tgl!="" && qty!=""){
      $("#isi-tabel").append("<tr class='text-center'><td>"+tgl+"</td><td class='text-right'>"+qty+"</td><td><button type=\"button\" class=\"btn btn-danger btn-sm\" onclick=\"deleteDetail(this)\">X</button></td></tr>");
    }
    $('#final_qty').val("");
  }
  function deleteDetail(ctl) { $(ctl).parents("tr").remove(); }
  document.querySelector("#final_qty").addEventListener("keypress", function (evt) {
    if (evt.which < 48 || evt.which > 57){ evt.preventDefault(); }
  });
  $('.tanggal').datetimepicker({
    format: 'DD-MM-YYYY HH:mm',
    icons: {
      time: 'fa fa-clock-o',
      date: 'fa fa-calendar',
      up: 'fa fa-chevron-up',
      down: 'fa fa-chevron-down',
      previous: 'fa fa-chevron-left',
      next: 'fa fa-chevron-right',
      today: 'fa fa-check',
      clear: 'fa fa-trash',
      close: 'fa fa-times'
    }
  });
  $("#nama_barang").autocomplete({
    source: function(request, response) {
      $.ajax({
        url: "{{url('search')}}",
        data: { "term" : request.term },
        dataType: "json",
        success: function(data){
          var resp = $.map(data,function(obj){ return obj.nama; });
          response(resp);
        }
      });
    },
    minLength: 1
  });
  function getDataFromTable(){
    //gets table
    var table = document.getElementById('myTable');
    //gets rows of table
    var rowLength = table.rows.length;
    //loops through rows    
    for (i = 0; i < rowLength; i++){
      if(i>0){
         //gets cells of current row
        var cells = table.rows.item(i).cells;
         //loops through each cell in current row
        for(var j = 0; j < cells.length; j++){
            /* get your cell info here */
          cellVal = cells.item(j).innerHTML;
          if(j==0){
            // $("#nama-barang").push(cellVal); 
            $("#form_data").append("<input type=\"hidden\" class=\"hidden-data\" name=\"tanggal-detail[]\" id=\"tanggal-detail\" value=\""+cellVal+"\">");
          }else if(j==1){
            //$("#qty").push(cellVal);
            $("#form_data").append("<input type=\"hidden\" class=\"hidden-data\"name=\"qty-detail[]\" id=\"qty-detail\" value=\""+cellVal+"\">");
          }
        }
      }
    }
  }
  function buatPack(){
    getDataFromTable();
    $.ajax({
      url: "{{ url('packing') }}",
      cache: false,
      type: "POST",
      data: $('#form_data').serialize(),
      success: function(result){
       var result = eval('('+result+')');
        if (result.success){
          swal({
            title: result.message_title,
            text: result.message_conten,
            type: result.message_type,
            closeOnConfirm: false,
            closeOnCancel: false   
          }, function(){ 
              var link='{{ url('packing') }}';
              location.replace(link); 
            });
        }
        else
        {
          swal({
            title: result.message_title,
            text: result.message_conten,
            type: result.message_type,
            closeOnConfirm: false,
            closeOnCancel: false
          });
          $(".hidden-data").remove();
        }
      },
        error: function (request, status, error) {
          console.error(request.responseText); 
          swal({
              title: "Error!",
              text: error,
              type: status,
              closeOnConfirm: false,
              closeOnCancel: false
            });
          $(".hidden-data").remove();
         }
    });
  }
</script>
@endsection
