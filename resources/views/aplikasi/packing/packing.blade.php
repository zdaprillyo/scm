@extends('layouts.app', ['class' => 'bg-default'])

@section('content')
<div class="header bg-gradient-primary py-7 py-lg-1">
  <div class="justify-content-center m-4" style="width: 98%">
    @if($akses->pivot->create)
      <a class="btn btn-info" href="{{ url('packing') }}/create">Tambah {{$feature_name}}</a>
    @endif
    <div><h3 class="text-white">Daftar {{$feature_name}}</h3></div>
    <div class="row table-responsive bg-white p-3">
    	<table class="table table-striped table-hover table-sm table-bordered" id="myTable">
    		<thead class="text-center">
    			<th>ID</th>
	    		<th>Tanggal</th>
          <th>Jam Mulai</th>
          <th>Jam Selesai</th>
          <th>BBM</th>
          <th>Aksi</th>
    		</thead>
    		<tbody>
    			@foreach($packings as $data)
    			<tr>
    				<td>{{$data->id}}</td>
    				<td>{{ date('d-m-Y', strtotime($data->waktu_mulai)) }}</td>
            <td>{{ date('H:s', strtotime($data->waktu_mulai)) }}</td>
            <td>{{ date('H:s', strtotime($data->waktu_selesai)) }}</td>
            <td class="text-kapital">{{ $data->fuel->nama ?? '-'}}</td>
    				<td>
              @if($akses->pivot->read)
                <button type="button" id="btn-ubah" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-detail" onclick="showPackingDetail('{{ $data->id }}')">Detail</button>
              @endif
              @if($akses->pivot->update)
                <button type="button" id="btn-ubah" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#form-modal" onclick="showFormUpdateOrDelete(1,'{{ $data->id }}')">Ubah</button>
              @endif
              @if($akses->pivot->delete)
                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#form-modal" onclick="showFormUpdateOrDelete(2,'{{ $data->id }}')">Hapus</button>
              @endif
    				</td>
    			</tr>
    			@endforeach
    		</tbody>
    	</table>
    </div>
  </div>
</div>

<!-- Start Form Modal -->
  <div class="modal fade" id="form-modal" tabindex="-1" role="dialog" aria-labelledby="form-modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_title">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form_data" enctype="multipart/form-data" method="POST" action="">
            @csrf
            <div class="form-group row">
              <input type="hidden" class="masukkan" name="id" id="id" value="">
              <div class="input-group">
                <div class="col-sm-2 d-flex align-items-center">Waktu Mulai</div>
                <div class="col-sm-10">
                  <input class="form-control masukkan" style="height: 38px" placeholder="Masukan tanggal beli" type="text" name="waktu_mulai" id="waktu_mulai">  
                </div> 
              </div>
              <div class="input-group">
                <div class="col-sm-2 d-flex align-items-center">Waktu Selesai</div>
                <div class="col-sm-10">
                  <input class="form-control masukkan" style="height: 38px" placeholder="Masukan tanggal beli" type="text" name="waktu_selesai" id="waktu_selesai">  
                </div> 
              </div>
              <div class="input-group">
                <div class="col-sm-2 d-flex align-items-center">BBM</div>
                <div class="col-sm-10">
                  <select class="form-control masukkan" id="bbm" name="bbm">
                    <option selected value="">Tidak ada</option>
                  @foreach($fuels as $fuel)
                    <option value="{{$fuel->id}}">{{ ucfirst($fuel->nama) }}-{{ $fuel->id }} ({{ date('d-m-Y',strtotime($fuel->tanggal_beli)) }})</option>
                  @endforeach
                  </select>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Batal</button>
          <input type="submit" id="btn_action" class="btn btn-success" value="Simpan">
          </form>
        </div>
      </div>
    </div>
  </div>
<!-- End Form Modal -->

<!-- Start Detail Modal -->
  <div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="form-modal" aria-hidden="true">
    <div class="modal-dialog modal-dialogcentered- modal-lg" role="document">
      <div class="modal-content bg-default">
        <div class="modal-header">
          <h5 class="modal-title text-white" id="modal_title">Detail Permintaan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" class="text-white">&times;</span>
          </button>
        </div>
        <div class="modal-body text-white">
          <div class="row">
            <div class="col">
              <div class="form-group row">
                <div class="input-group">
                  <div class="col-sm-5 d-flex align-items-center">ID Packing</div>
                  <div class="col-sm-5">: <span id="id-modal-packing">1231</span></div>
                </div>
                <div class="input-group">
                  <div class="col-sm-5 d-flex align-items-center">BBM</div>
                  <div class="col-sm-7">: <span id="bbm-modal-packing">Solar-10 (12-12-2020)</span></div>
                </div>
              </div>
            </div>
            <div class="col">
              <div class="form-group row">
                <div class="input-group">
                  <div class="col-sm-5 d-flex align-items-center">Waktu Mulai</div>
                  <div class="col-sm-7">: <span id="waktu-mulai-modal-packing">{{ date('d-m-Y H:i:s') }}</span></div>
                </div>
                <div class="input-group">
                  <div class="col-sm-5 d-flex align-items-center">Waktu Selesai</div>
                  <div class="col-sm-7">: <span id="waktu-selesai-modal-packing">{{ date('d-m-Y H:i:s') }}</span></div>
                </div>
              </div>
            </div>
          </div>
          <label>Detail Packing</label>
          <table class="table table-striped table-bordered table-hover table-dark">
            <thead class="text-center">
              <th>ID</th>
              <th>Nama Barang</th>
              <th>Tanggal Dibuat</th>
              <th>Qty</th>
            </thead>
            <tbody id="isi-table-modal-detail-packing">
              <tr>
                <td>12</td>
                <td>Kardus</td>
                <td>{{ date('d-m-Y') }}</td>
                <td style='text-align:right'>10</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </div>
  </div>
<!-- End Detail Modal -->
<div class="container bg-default mt--10 pb-5"></div>
@include('layouts.footers.auth')
<script>
$(document).ready( function () {
    $('#myTable').DataTable({"aaSorting": [1]});
} );
var feature_name='{{$feature_name}}';
var urlRoute="{{url('packing')}}";

function showFormInput() {
  $(".masukkan").val("");
  $("#modal_title").html("Form Data "+feature_name+" Baru");
  $("#form_data").attr("action", "javascript:actionsFunction(0,0)");
  $("#btn_action").attr("value", "Simpan Data");
  $("#btn_action").removeClass("btn-danger").addClass("btn-success");
}
function showPackingDetail(id){
  $.ajax({
    type: "GET",
    url: urlRoute+"/"+id,
    cache: false,
    success: function(result){
      var result = eval('('+result+')');
      if(result.success){
        $("#id-modal-packing").html(result.id);
        $("#bbm-modal-packing").html(result.fuel_id);
        $("#waktu-mulai-modal-packing").html(result.waktu_mulai);
        $("#waktu-selesai-modal-packing").html(result.waktu_selesai);
        $("#isi-table-modal-detail-packing").html(result.isi_table);
      }else{
        swal({
          title: "Gagal Mengambil Data {{$feature_name}} !",
          text: result.pesan,
          type: "error"
        });
      }
    },
    error: function (request, status, error) {
      console.log(request.responseText);
      swal({
          title: "Error!",
          text: error,
          type: "error",
          closeOnConfirm: false,
          closeOnCancel: false
      });
    }
  });
}
function showFormUpdateOrDelete(action,id) {
  $("#icon_display").removeClass();
  if(action==1){ //ubah data
    $(".masukkan").prop('disabled', false);
    $("#btn_action").attr("value", "Ubah Data");
    $("#modal_title").html("Form Ubah Data "+feature_name);
    $("#btn_action").removeClass("btn-danger").addClass("btn-success");
  }else if(action==2){ //hapus data
    $(".masukkan").prop('disabled', true);
    $("#id").prop('disabled', false);
    $("#btn_action").attr("value", "Hapus Data");
    $("#modal_title").html("Form Hapus Data "+feature_name);
    $("#btn_action").removeClass("btn-success").addClass("btn-danger");
  }
  $.ajax({
    type: "GET",
    url: urlRoute+"/"+id,
    cache: false,
    success: function(result){
      var result = eval('('+result+')');
      if(result.success){
        $("#form_data").attr("action", "javascript:actionsFunction("+action+","+result.id+")");
        $("#id").val(result.id);
        $("#waktu_mulai").val(result.waktu_mulai);
        $("#waktu_selesai").val(result.waktu_selesai);
        $("#bbm").val(result.fuel_id);
      }else{
        swal({
          title: "Gagal Mengambil Data {{$feature_name}} !",
          text: result.pesan,
          type: "error"
        });
      }
    },
    error: function (request, status, error) {
      swal({
          title: "Error!",
          text: error,
          type: "error",
          closeOnConfirm: false,
          closeOnCancel: false
      });
    }
  });
}
function actionsFunction(action,id) {
  var method="";
  var actionUrl=urlRoute+"/"+id;
  if(action==0){
    method='POST';
    actionUrl=urlRoute;
  }else if(action==1){
    method='PUT';
  }else if(action==2){
    method='DELETE';
  }
  var data = $('#form_data');
  $.ajax({
    url: actionUrl,
    data: data.serialize(),
    cache: false,
    type: method,
    success: function(result){
     var result = eval('('+result+')');
      if (result.success){
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false   
        }, function(){ location.reload(); });
      }
      else{
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false
        });
      }
    },
      error: function (request, status, error) {
        console.log(request.responseText);
        swal({
            title: "Error!",
            text: error,
            type: "error",
            closeOnConfirm: false,
            closeOnCancel: false
          });
      }
  });
}

</script>
@endsection