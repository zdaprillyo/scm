@extends('layouts.app', ['class' => 'bg-default'])

@section('content')
<div class="header bg-gradient-primary py-7 py-lg-1">
  <div class="justify-content-center m-4" style="width: 98%">
    @if($aksesMenu->pivot->create)
      <button type="button" class="btn btn-info" data-toggle="modal" data-target="#form-modal-menu" onclick="showFormInput()">Tambah {{$feature_name}}</button>
    @endif
    <div><h3 class="text-white">Daftar {{$feature_name}}</h3></div>
    <div class="row table-responsive bg-white p-3">
    	<table id="myTable" class="table table-striped table-hover table-sm table-bordered">
    		<thead class="text-center">
    			<th>No</th>
	    		<th>Nama</th>
	    		<th>Ikon</th>
	    		<th>Url</th>
	    		<th>Aksi</th>
    		</thead>
    		<tbody>
    			@foreach($menus->sortBy('no_urut') as $data)
      			<tr>
      				<td>{{$data->no_urut}}</td>
      				<td>{{ ucwords($data->nama)}}</td>
      				<td><i class="{{$data->ikon}}"></i></td>
      				<td>{{$data->url}}</td>
      				<td>
                @if($aksesSub->pivot->create)
                  <button type="button" id="btn-ubah" class="btn btn-info btn-sm" data-toggle="modal" data-target="#form-modal-sub" onclick="showFormInputSub('{{ $data->id }}')">Tambah Submenu</button>
                @endif
                @if($aksesMenu->pivot->update)
                  <button type="button" id="btn-ubah" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#form-modal-menu" onclick="showFormUpdateOrDelete(1,'{{ $data->id }}')">Ubah</button>
                @endif
                @if($aksesMenu->pivot->delete)
                  <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#form-modal-menu" onclick="showFormUpdateOrDelete(2,'{{ $data->id }}')">Hapus</button>
                @endif
      				</td>
      			</tr>
            @foreach($data->submenus->sortBy('no_urut') as $sub)
            <tr>
              <td>Sub-{{$sub->no_urut}}</td>
              <td>{{ ucwords($sub->nama)}}</td>
              <td></td>
              <td>{{$sub->url}}</td>
              <td>
                @if($aksesSub->pivot->update)
                <button type="button" id="btn-ubah" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#form-modal-sub" onclick="showFormUpdateOrDeleteSub(1,'{{ $sub->id }}')">Ubah</button>
                @endif
                @if($aksesSub->pivot->delete)
                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#form-modal-sub" onclick="showFormUpdateOrDeleteSub(2,'{{ $sub->id }}')">Hapus</button>
                @endif

              </td>
            </tr>
            @endforeach
    			@endforeach
    		</tbody>
    	</table>
    </div>
  </div>
</div>
<div class="container bg-default mt--10 pb-5"></div>
<!-- Start Modal Menu-->
  <div class="modal fade" id="form-modal-menu" tabindex="-1" role="dialog" aria-labelledby="form-modal-menu" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_title">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form_data_menu" enctype="multipart/form-data" method="POST" action="">
            @csrf
            <div class="form-group row">
              <input type="hidden" class="masukkan" name="id" id="id" value="">
              <div class="input-group">
                <div class="col-sm-2 d-flex align-items-center">Nama</div>
                <div class="col-sm-10"><input type="text" class="form-control masukkan" id="nama" name="nama" placeholder="Masukkan nama {{$feature_name}}"></div>
              </div>
              <div class="input-group" id="ikon_container">
                <div class="col-sm-2 d-flex align-items-center">Ikon</div>
                <div class="col-sm-9"><input type="text" class="form-control masukkan" id="ikon" name="ikon" placeholder="Masukkan nama ikon fa family, cth. 'fa fa-truck'" oninput="updateikonDisplay()"></div>
                <i id="ikon_display" class=""></i>
              </div>
              <div class="input-group">
                <div class="col-sm-2 d-flex align-items-center">No Urut</div>
                <div class="col-sm-10"><input type="text" class="form-control masukkan" id="no_urut" name="no_urut" placeholder="Masukkan nomor urut"></div>
              </div>
              <div class="input-group">
                <div class="col-sm-2 d-flex align-items-center">URL</div>
                <div class="col-sm-10"><input type="text" class="form-control masukkan" id="url" name="url" placeholder="Masukkan alamat url"></div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Batal</button>
          <input type="submit" id="btn_action" class="btn btn-success" value="Simpan">
          </form>
        </div>
      </div>
    </div>
  </div>
<!-- End Modal Menu-->

<!-- Start Modal Sub -->
  <div class="modal fade" id="form-modal-sub" tabindex="-1" role="dialog" aria-labelledby="form-modal-sub" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modal_title_sub">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="form_data_sub" enctype="multipart/form-data" method="POST" action="">
              @csrf
              <div class="form-group row">
                <input type="hidden" class="masukkan" name="sub_id" id="sub_id" value="">
                <input type="hidden" class="masukkan" name="menu_id" id="menu_id" value="">
                <div class="input-group">
                  <div class="col-sm-2 d-flex align-items-center">Nama</div>
                  <div class="col-sm-10"><input type="text" class="form-control masukkan" id="sub_nama" name="sub_nama" placeholder="Masukkan nama Sub {{$feature_name}}"></div>
                </div>
                <div class="input-group">
                  <div class="col-sm-2 d-flex align-items-center">No Urut</div>
                  <div class="col-sm-10"><input type="text" class="form-control masukkan" id="sub_no_urut" name="sub_no_urut" placeholder="Masukkan nomor urut"></div>
                </div>
                <div class="input-group">
                  <div class="col-sm-2 d-flex align-items-center">URL</div>
                  <div class="col-sm-10"><input type="text" class="form-control masukkan" id="sub_url" name="sub_url" placeholder="Masukkan alamat url"></div>
                </div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-warning" data-dismiss="modal">Batal</button>
            <input type="submit" id="btn_action_sub" class="btn btn-success" value="Simpan">
            </form>
          </div>
        </div>
      </div>
    </div>
<!-- End Modal Sub -->
@include('layouts.footers.auth')
<script>
$(document).ready( function () {
    $('#myTable').DataTable({ "aaSorting": [] });
} );
var feature_name='{{$feature_name}}';
var urlRoute="{{url('menu')}}";
function updateikonDisplay(){
  $("#ikon_display").removeClass();
  $("#ikon_display").addClass("fa "+$("#ikon").val()+" text-blue d-flex align-items-center");
}
function showFormInputSub(menu_id){
  $(".masukkan").prop('disabled', false);
  $(".masukkan").val("");
  $("#modal_title_sub").html("Form Data Sub "+feature_name+" Baru");
  $("#form_data_sub").attr("action", "javascript:actionsFunction(1,0,0)");
  $("#btn_action_sub").attr("value", "Simpan Data");
  $("#btn_action_sub").removeClass("btn-danger").addClass("btn-success");
  $("#menu_id").val(menu_id);
}
function showFormInput() {
  $(".masukkan").prop('disabled', false);
  $(".masukkan").val("");
  $("#modal_title").html("Form Data "+feature_name+" Baru");
  $("#form_data_menu").attr("action", "javascript:actionsFunction(0,0,0)");
  $("#btn_action").attr("value", "Simpan Data");
  $("#btn_action").removeClass("btn-danger").addClass("btn-success");
  $("#ikon_display").removeClass();
  $("#ikon_display").addClass("fa fa-truck text-blue d-flex align-items-center");
}
function showFormUpdateOrDeleteSub(action,id){
  if(action==1){ //ubah data
    $(".masukkan").prop('disabled', false);
    $("#btn_action_sub").attr("value", "Ubah Data");
    $("#modal_title_sub").html("Form Ubah Data Sub "+feature_name);
    $("#btn_action_sub").removeClass("btn-danger").addClass("btn-success");
  }else if(action==2){ //hapus data
    $(".masukkan").prop('disabled', true);
    $("#sub_id").prop('disabled', false);
    $("#btn_action_sub").attr("value", "Hapus Data");
    $("#modal_title_sub").html("Form Hapus Data Sub "+feature_name);
    $("#btn_action_sub").removeClass("btn-success").addClass("btn-danger");
  }
    $.ajax({
      type: "GET",
      url: "{{url('submenu')}}"+"/"+id,
      cache: false,
      success: function(result){
        var result = eval('('+result+')');
        if(result.success){
          $("#form_data_sub").attr("action", "javascript:actionsFunction(1,"+action+","+result.id+")");
          $("#sub_id").val(result.id);
          $("#sub_nama").val(result.nama);
          $("#sub_no_urut").val(result.no_urut);
          $("#sub_url").val(result.url);
        }else{
          swal({
            title: "Gagal Mengambil Data Sub {{$feature_name}} !",
            text: result.pesan,
            type: "error"
          });
        }
      },
      error: function (request, status, error) {
        swal({
            title: "Error!",
            text: error,
            type: "error",
            closeOnConfirm: false,
            closeOnCancel: false
        });
      }
    });
}
function showFormUpdateOrDelete(action,id) {
  $("#ikon_display").removeClass();
  if(action==1){ //ubah data
    $(".masukkan").prop('disabled', false);
    $("#btn_action").attr("value", "Ubah Data");
    $("#modal_title").html("Form Ubah Data "+feature_name);
    $("#btn_action").removeClass("btn-danger").addClass("btn-success");
  }else if(action==2){ //hapus data
    $(".masukkan").prop('disabled', true);
    $("#id").prop('disabled', false);
    $("#btn_action").attr("value", "Hapus Data");
    $("#modal_title").html("Form Hapus Data "+feature_name);
    $("#btn_action").removeClass("btn-success").addClass("btn-danger");
  }
    $.ajax({
      type: "GET",
      url: urlRoute+"/"+id,
      cache: false,
      success: function(result){
        var result = eval('('+result+')');
        if(result.success){
          $("#form_data_menu").attr("action", "javascript:actionsFunction(0,"+action+","+result.id+")");
          $("#id").val(result.id);
          $("#nama").val(result.nama);
          $("#ikon").val(result.ikon);
          $("#no_urut").val(result.no_urut);
          $("#ikon_display").addClass("fa "+result.ikon+" text-blue d-flex align-items-center");
          $("#url").val(result.url);
        }else{
          swal({
            title: "Gagal Mengambil Data {{$feature_name}} !",
            text: result.pesan,
            type: "error"
          });
        }
      },
      error: function (request, status, error) {
        swal({
            title: "Error!",
            text: error,
            type: "error",
            closeOnConfirm: false,
            closeOnCancel: false
        });
      }
    });
}
function actionsFunction(fitur,action,id) {
  var method="";
  var actionUrl=urlRoute+"/"+id;
  var data = $('#form_data_menu');
  if(fitur==1){
    actionUrl="{{ url('submenu') }}"+"/"+id;
    data = $('#form_data_sub');
  }
  if(action==0){
    method='POST';
    actionUrl=urlRoute;
    if(fitur==1){
      actionUrl="{{ url('submenu') }}";
    }
  }else if(action==1){
    method='PUT';
  }else if(action==2){
    method='DELETE';
  }
  
  $.ajax({
    url: actionUrl,
    data: data.serialize(),
    cache: false,
    type: method,
    success: function(result){
     var result = eval('('+result+')');
      if (result.success){
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false   
        }, function(){ location.reload(); });
      }
      else{
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false
        });
      }
    },
      error: function (request, status, error) {
        console.log(request.responseText);
        swal({
            title: "Error!",
            text: error,
            type: "error",
            closeOnConfirm: false,
            closeOnCancel: false
          });
      }
  });
}
</script>
@endsection