@extends('layouts.app', ['class' => 'bg-default'])

@section('content')
<div class="header bg-gradient-primary py-7 py-lg-1">
  <div class="justify-content-center m-4" style="width: 98%">
    <div><h3 class="text-white">Daftar {{$feature_name}}</h3></div>
    <div class="row table-responsive bg-white p-3">
      <table id="myTable" class="table table-striped table-hover table-sm table-bordered">
        <thead class="text-center">
            <th>Kategori</th>
            <th>Nama</th>
            @foreach($privileges as $privilege)
            <th>{{$privilege->nama}}</th>
            @endforeach
        </thead>
        <tbody>
          @foreach($menus->sortBy('no_urut') as $menu)
            <tr>
            <td>{{_('Menu')}}</td>
            <td>{{ ucwords($menu->nama)}}</td>
            @foreach($privileges as $privilege)
              @foreach($menu->privileges as $akses)
                @if($akses->pivot->privilege_id==$privilege->id)
                <td>
                  @if($menu->submenus->isEmpty())
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="menu-{{ $menu->id }}-{{ $akses->id }}-create"
                    @if($akses->pivot->create==1) checked @endif @if($menu->id==1 && $privilege->id==1) disabled="true" @endif onclick="action('menu','{{ $menu->id }}','{{ $privilege->id }}','create')">
                    <label class="custom-control-label" for="menu-{{ $menu->id }}-{{ $akses->id }}-create">Create</label>
                  </div>

                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="menu-{{ $menu->id }}-{{ $akses->id }}-read"
                    @if($akses->pivot->read==1) checked @endif @if($menu->id==1 && $privilege->id==1) disabled="true" @endif onclick="action('menu','{{ $menu->id }}','{{ $privilege->id }}','read')">
                    <label class="custom-control-label" for="menu-{{ $menu->id }}-{{ $akses->id }}-read">Read</label>
                  </div>

                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="menu-{{ $menu->id }}-{{ $akses->id }}-update"
                    @if($akses->pivot->update==1) checked @endif @if($menu->id==1 && $privilege->id==1) disabled="true" @endif onclick="action('menu','{{ $menu->id }}','{{ $privilege->id }}','update')">
                    <label class="custom-control-label" for="menu-{{ $menu->id }}-{{ $akses->id }}-update">Update</label>
                  </div>

                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="menu-{{ $menu->id }}-{{ $akses->id }}-delete"
                    @if($akses->pivot->delete==1) checked @endif @if($menu->id==1 && $privilege->id==1) disabled="true" @endif onclick="action('menu','{{ $menu->id }}','{{ $privilege->id }}','delete')">
                    <label class="custom-control-label" for="menu-{{ $menu->id }}-{{ $akses->id }}-delete">Delete</label>
                  </div>
                  @else
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" id="menu-{{ $menu->id }}-{{ $akses->id }}-read"
                      @if($akses->pivot->read==1) checked @endif @if($menu->id==1 && $privilege->id==1) disabled="true" @endif onclick="action('menu','{{ $menu->id }}','{{ $privilege->id }}','read')">
                      <label class="custom-control-label" for="menu-{{ $menu->id }}-{{ $akses->id }}-read">Read</label>
                    </div>
                  @endif
                  
                </td>
                @endif
              @endforeach
            @endforeach
          </tr>
          @if(!$menu->submenus->isEmpty())
            @foreach($menu->submenus->sortBy('no_urut') as $sub)
            <tr>
            <td>{{_('Sub')}}</td>
            <td>{{ ucwords($sub->nama)}}</td>
            @foreach($privileges as $privilege)
              @foreach($sub->privileges as $akses)
                @if($akses->pivot->privilege_id==$privilege->id)
                <td>
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="sub-{{ $sub->id }}-{{ $akses->id }}-create"
                    @if($akses->pivot->create==1) checked @endif @if($menu->id==1 && $privilege->id==1) disabled="true" @endif onclick="action('sub','{{ $sub->id }}','{{ $privilege->id }}','create')">
                    <label class="custom-control-label" for="sub-{{ $sub->id }}-{{ $akses->id }}-create">Create</label>
                  </div>

                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="sub-{{ $sub->id }}-{{ $akses->id }}-read"
                    @if($akses->pivot->read==1) checked @endif @if($menu->id==1 && $privilege->id==1) disabled="true" @endif onclick="action('sub','{{ $sub->id }}','{{ $privilege->id }}','read')">
                    <label class="custom-control-label" for="sub-{{ $sub->id }}-{{ $akses->id }}-read">Read</label>
                  </div>

                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="sub-{{ $sub->id }}-{{ $akses->id }}-update"
                    @if($akses->pivot->update==1) checked @endif @if($menu->id==1 && $privilege->id==1) disabled="true" @endif onclick="action('sub','{{ $sub->id }}','{{ $privilege->id }}','update')">
                    <label class="custom-control-label" for="sub-{{ $sub->id }}-{{ $akses->id }}-update">Update</label>
                  </div>

                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="sub-{{ $sub->id }}-{{ $akses->id }}-delete"
                    @if($akses->pivot->delete==1) checked @endif @if($menu->id==1 && $privilege->id==1) disabled="true" @endif onclick="action('sub','{{ $sub->id }}','{{ $privilege->id }}','delete')">
                    <label class="custom-control-label" for="sub-{{ $sub->id }}-{{ $akses->id }}-delete">Delete</label>
                  </div>
                </td>
                @endif
              @endforeach
            @endforeach
          </tr>
            @endforeach
          @endif
          @endforeach
        </tbody>
      </table>    
    </div>
  </div>
</div>
<div class="container bg-default mt--10 pb-5"></div>

@include('layouts.footers.auth')
<script>
$(document).ready( function () {
    $('#myTable').DataTable({ "aaSorting": [] });
} );
function action(kategori,menusub_id,privilege_id,aksi){
  var cbox= document.getElementById(kategori+"-"+menusub_id+"-"+privilege_id+"-"+aksi);
  var status="";
  if(cbox.checked){status=1;}else{status=0;}
  // alert(menu_id+privilege_id+aksi+cbox.checked);
  $.ajax({
    url: "{{ url('akses') }}",
    data: {"_token":"{{ csrf_token() }}","kategori":kategori,"menusub_id": menusub_id,"privilege_id":privilege_id,"aksi":aksi,"status":status},
    cache: false,
    type: 'PUT',
    success: function(result){
//      console.log(result);
     var result = eval('('+result+')');
      if (result.success){
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false   
        }, function(){ location.reload(); });
      }
      else{
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false
        }, function(){ location.reload(); });
      }
    },
      error: function (request, status, error) {
        console.log(request.responseText);
        swal({
            title: "Error!",
            text: error,
            type: "error",
            closeOnConfirm: false,
            closeOnCancel: false
          });
      }
  });
}
</script>
@endsection