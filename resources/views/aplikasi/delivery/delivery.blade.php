@extends('layouts.app', ['class' => 'bg-default'])

@section('content')
<div class="header bg-gradient-primary py-7 py-lg-1">
  <div class="justify-content-center m-4" style="width: 98%">
    @if($akses->pivot->create)
      <button type="button" class="btn btn-info" data-toggle="modal" data-target="#form-modal" onclick="showFormInput()">Tambah {{$feature_name}}</button>
    @endif
    <div><h3 class="text-white">Daftar {{$feature_name}}</h3></div>
    <div class="row table-responsive bg-white p-3">
    	<table class="table table-striped table-hover table-sm table-bordered" id="myTable">
    		<thead class="text-center">
    			<th>ID</th>
	    		<th>Berangkat</th>
          <th>Estimasi Sampai</th>
          <th>Sampai</th>
          <th>Tiket</th>
          <th>Status</th>
          <th>ID Muatan</th>
          <th>Aksi</th>
    		</thead>
    		<tbody>
    			@foreach($deliveries as $data)
    			<tr>
    				<td>{{$data->id}}</td>
    				<td>{{ date('d-m-Y H:i', strtotime($data->tanggal_berangkat)) }}</td>
            <td>{{ date('d-m-Y H:i', strtotime($data->estimasi_sampai)) }}</td>
            <td>
              @if($data->tanggal_sampai)
                {{ date('d-m-Y H:i', strtotime($data->tanggal_sampai)) }}
              @else
                -
              @endif
            </td>
            <td>{{ $data->no_tiket ?? '-'}} </td>
    			  <td>
              @if($data->status=='sampai')
                <button class="btn btn-success btn-sm">{{ ucfirst($data->status) }}</button>
              @elseif($data->status=='dikirim')
                <button class="btn btn-info btn-sm">{{ ucfirst($data->status) }}</button>
              @elseif($data->status=='batal')
                <button class="btn btn-danger btn-sm">{{ ucfirst($data->status) }}</button>
              @endif   
            </td>
            <td>{{ $data->loading_id }}-{{ ucfirst($data->loading->jenis_pengangkut) }} ({{ date('d-m-Y', strtotime($data->loading->waktu_mulai)) }})</td>
            <td>
              @if($akses->pivot->update)
                @if(!$data->tanggal_sampai)
                <button type="button" id="btn-ubah" class="btn btn-success btn-sm" onclick="updateStatus({{ $data->id }},'sampai')">Sampai</button>
                <button type="button" id="btn-ubah" class="btn btn-danger btn-sm" onclick="updateStatus({{ $data->id }},'batal')">Batal</button>
                @endif
                @if($data->status=='dikirim')
                  <button type="button" id="btn-ubah" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#form-modal" onclick="showFormUpdateOrDelete(1,'{{ $data->id }}')">Ubah</button>
                @endif
              @endif
    				</td>
    			</tr>
    			@endforeach
    		</tbody>
    	</table>
    </div>
  </div>
</div>

<!-- Start Modal -->
  <div class="modal fade" id="form-modal" tabindex="-1" role="dialog" aria-labelledby="form-modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_title">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form_data" enctype="multipart/form-data" method="POST" action="">
            @csrf
            <div class="form-group row">
              <input type="hidden" class="masukkan" name="id" id="id" value="">
              <div class="input-group">
                <div class="col-sm-5 d-flex align-items-center">Tanggal Berangkat</div>
                <div class="col-sm-7">
                  <input class="form-control masukkan tanggal" placeholder="Masukan tanggal beli" type="text" name="tanggal_berangkat" id="tanggal_berangkat">  
                </div> 
              </div>
              <div class="input-group">
                <div class="col-sm-5 d-flex align-items-center">Estimasi Sampai</div>
                <div class="col-sm-7">
                  <input class="form-control masukkan tanggal" placeholder="Masukan tanggal beli" type="text" name="estimasi_sampai" id="estimasi_sampai">  
                </div> 
              </div>
              <div class="input-group">
                <div class="col-sm-5 d-flex align-items-center">Status</div>
                <div class="col-sm-7">
                  <select class="custom-select masukkan" id="status" name="status" onchange="filterInput(0)">
                    <option value="">Pilih status  {{$feature_name}}</option>
                    <option value='dikirim'>Dikirim</option>
                    <option value='sampai'>Sampai</option>
                    <option value='batal'>Batal</option>
                  </select>
                </div>
              </div>
              <div class="input-group">
                <div class="col-sm-5 d-flex align-items-center">Tanggal Sampai</div>
                <div class="col-sm-7">
                  <input class="form-control masukkan tanggal" placeholder="Masukan tanggal beli" type="text" name="tanggal_sampai" id="tanggal_sampai">  
                </div> 
              </div>
              <div class="input-group" id="masukan-id-muat">
                <div class="col-sm-5 d-flex align-items-center">ID Muat</div>
                <div class="col-sm-7">
                  <select class="custom-select masukkan" id="loading_id" name="loading_id" onchange="filterInput(1)">
                    <option value="">Pilih id muat</option>
                    @foreach($loading as $load)
                      <option value="{{ $load->id }}">{{ $load->id }}-{{ $load->jenis_pengangkut }} ({{ date('d-m-Y H:i',strtotime($load->waktu_mulai)) }})</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="input-group" id="masukan-nomor-tiket">
                <div class="col-sm-5 d-flex align-items-center">Nomor Tiket</div>
                <div class="col-sm-7"><input type="text" class="form-control masukkan" id="no_tiket" name="no_tiket" placeholder="Masukkan nomor tiket  {{$feature_name}}"></div>
              </div> 
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Batal</button>
          <input type="submit" id="btn_action" class="btn btn-success" value="Simpan">
          </form>
        </div>
      </div>
    </div>
  </div>
<!-- End Modal -->
<div class="container bg-default mt--10 pb-5"></div>
@include('layouts.footers.auth')
<script>
  function filterInput(filter){
    if(filter==0){
      var stts=$('#status').val();
      if(stts!='sampai'){ $("#tanggal_sampai").prop('disabled', true); } 
      else{ $("#tanggal_sampai").prop('disabled', false); }
    }else if(filter==1){
      var id=$('#loading_id').val();
      $.ajax({
      url: "{{ url('loading') }}/"+id,
      cache: false,
      type: 'GET',
      success: function(result){
       var result = eval('('+result+')');
        if (result.success){
          if(result.jenis_pengangkut=='truk'){
            $("#no_tiket").prop('disabled', false);
          }else{
            $("#no_tiket").prop('disabled', true);
          }
        }
        else{
          swal({
            title: result.message_title,
            text: result.message_conten,
            type: result.message_type,
            closeOnConfirm: false,
            closeOnCancel: false
          });
        }
      },
        error: function (request, status, error) {
          console.log(request.responseText);
          swal({
              title: "Error!",
              text: error,
              type: "error",
              closeOnConfirm: false,
              closeOnCancel: false
            });
        }
    });
    }
  }
$('.tanggal').datetimepicker({
  format: 'DD-MM-YYYY HH:mm',
  icons: {
    time: 'fa fa-clock-o',
    date: 'fa fa-calendar',
    up: 'fa fa-chevron-up',
    down: 'fa fa-chevron-down',
    previous: 'fa fa-chevron-left',
    next: 'fa fa-chevron-right',
    today: 'fa fa-check',
    clear: 'fa fa-trash',
    close: 'fa fa-times'
  }
});
$(document).ready( function () {
    $('#myTable').DataTable({"aaSorting": [1]});
} );
var feature_name='{{$feature_name}}';
var urlRoute="{{url('delivery')}}";

function showFormInput() {
  $(".masukkan").val("");
  $("#modal_title").html("Form Data "+feature_name+" Baru");
  $("#form_data").attr("action", "javascript:actionsFunction(0,0)");
  $("#btn_action").attr("value", "Simpan Data");
  $("#btn_action").removeClass("btn-danger").addClass("btn-success");
  $("#masukan-id-muat").show();
  $("#masukan-nomor-tiket").show();
}
function showFormUpdateOrDelete(action,id) {
  $("#masukan-id-muat").hide();
  $("#icon_display").removeClass();

  if(action==1){ //ubah data
    $(".masukkan").prop('disabled', false);
    $("#btn_action").attr("value", "Ubah Data");
    $("#modal_title").html("Form Ubah Data "+feature_name);
    $("#btn_action").removeClass("btn-danger").addClass("btn-success");
  }else if(action==2){ //hapus data
    $(".masukkan").prop('disabled', true);
    $("#id").prop('disabled', false);
    $("#btn_action").attr("value", "Hapus Data");
    $("#modal_title").html("Form Hapus Data "+feature_name);
    $("#btn_action").removeClass("btn-success").addClass("btn-danger");
  }
  $.ajax({
    type: "GET",
    url: urlRoute+"/"+id,
    cache: false,
    success: function(result){
      var result = eval('('+result+')');
      if(result.success){
        $("#form_data").attr("action", "javascript:actionsFunction("+action+","+result.id+")");
        $("#id").val(result.id);
        $("#tanggal_berangkat").val(result.tanggal_berangkat);
        $("#estimasi_sampai").val(result.estimasi_sampai);
        $("#tanggal_sampai").val(result.tanggal_sampai);
        $("#no_tiket").val(result.no_tiket);
        if(!result.no_tiket){ $("#masukan-nomor-tiket").hide(); }else{$("#masukan-nomor-tiket").show(); }
        $("#status").val(result.status);
      }else{
        swal({
          title: "Gagal Mengambil Data {{$feature_name}} !",
          text: result.pesan,
          type: "error"
        });
      }
    },
    error: function (request, status, error) {
      console.log(request.responseText);
      swal({
          title: "Error!",
          text: error,
          type: "error",
          closeOnConfirm: false,
          closeOnCancel: false
      });
    }
  });
}
function updateStatus(id,aksi){
  var actionUrl=urlRoute+"/"+id;
  $.ajax({
    url: actionUrl,
    data: {"_token":"{{ csrf_token() }}","id":id,"status_delivery":aksi},
    cache: false,
    type: "PUT",
    success: function(result){
     var result = eval('('+result+')');
      if (result.success){
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false   
        }, function(){ location.reload(); });
      }
      else{
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false
        });
      }
    },
      error: function (request, status, error) {
        console.log(request.responseText);
        swal({
            title: "Error!",
            text: error,
            type: "error",
            closeOnConfirm: false,
            closeOnCancel: false
          });
      }
  });
}
function actionsFunction(action,id) {
  var method="";
  var actionUrl=urlRoute+"/"+id;
  if(action==0){
    method='POST';
    actionUrl=urlRoute;
  }else if(action==1){
    method='PUT';
  }else if(action==2){
    method='DELETE';
  }
  var data = $('#form_data');
  $.ajax({
    url: actionUrl,
    data: data.serialize(),
    cache: false,
    type: method,
    success: function(result){
     var result = eval('('+result+')');
      if (result.success){
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false   
        }, function(){ location.reload(); });
      }
      else{
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false
        });
      }
    },
      error: function (request, status, error) {
        console.log(request.responseText);
        swal({
            title: "Error!",
            text: error,
            type: "error",
            closeOnConfirm: false,
            closeOnCancel: false
          });
      }
  });
}
</script>
@endsection