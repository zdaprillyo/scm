@extends('layouts.app', ['class' => 'bg-default'])

@section('content')
<div class="header bg-gradient-primary py-7 py-lg-1">
  <div class="justify-content-center m-4" style="width: 98%">
    {{-- @if($akses->pivot->create)
      <button type="button" class="btn btn-info" data-toggle="modal" data-target="#form-modal" onclick="showFormInput()">Tambah {{$feature_name}}</button>
    @endif --}}
    <div><h3 class="text-white">Daftar {{$feature_name}}</h3></div>
    <div class="row table-responsive bg-white p-3">
    	<table class="table table-striped table-hover table-sm table-bordered" id="myTable">
    		<thead class="text-center">
    			<th>ID</th>
	    		<th>Barang</th>
          <th>Qty</th>
          <th>Satuan</th>
          <th>Status</th>
          <th>Dibuat</th>
          <th>Packing ID</th>
          {{-- <th>Aksi</th> --}}
    		</thead>
    		<tbody>
    			@foreach($fproducts as $data)
    			<tr>
    				<td>{{$data->id}}</td>
    				<td>{{ ucfirst($data->product->nama)}}</td>
            <td class="text-right">{{ $data->qty }}</td>
            <td>{{ ucfirst($data->product->satuan_unit) }}</td>
            <td>
              @if($data->status==1)
                <button class="btn btn-success btn-sm">{{ __('Ada') }}</button>
              @else
                <button class="btn btn-danger btn-sm">{{ __('Tidak ada') }}</button>
              @endif
            </td>
            <td>{{ date('d-m-Y',strtotime($data->tanggal_dibuat)) }}</td>
            <td>{{ $data->packing->id }}-({{ date('d-m-Y H:i',strtotime($data->packing->waktu_mulai)) }})</td>
    				{{-- <td>
              @if($akses->pivot->update)
              <button type="button" id="btn-ubah" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#form-modal" onclick="showFormUpdateOrDelete(1,'{{ $data->id }}')">Ubah</button>
              @endif
              @if($akses->pivot->delete)
              <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#form-modal" onclick="showFormUpdateOrDelete(2,'{{ $data->id }}')">Hapus</button>
              @endif
    				</td> --}}
    			</tr>
    			@endforeach
    		</tbody>
    	</table>
    </div>
  </div>
</div>
<div class="container bg-default mt--10 pb-5"></div>
<!-- Start Modal -->
  <div class="modal fade" id="form-modal" tabindex="-1" role="dialog" aria-labelledby="form-modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_title">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form_data" enctype="multipart/form-data" method="POST" action="">
            @csrf
            <div class="form-group row">
              <input type="hidden" class="masukkan" name="id" id="id" value="">
              <div class="input-group">
                <div class="col-sm-2 d-flex align-items-center">Nama</div>
                <div class="col-sm-10"><input type="text" class="form-control masukkan" id="nama" name="nama" placeholder="Masukkan nama {{$feature_name}}"></div>
              </div>
              <div class="input-group">
                <div class="col-sm-2 d-flex align-items-center">Harga Jual</div>
                <div class="col-sm-10"><input type="text" class="form-control masukkan" id="harga_jual" name="harga_jual" placeholder="Masukkan harga jual {{$feature_name}}"></div>
              </div>
              <div class="input-group">
                <div class="col-sm-2 d-flex align-items-center">Harga Beli</div>
                <div class="col-sm-10"><input type="text" class="form-control masukkan" id="harga_beli" name="harga_beli" placeholder="Masukkan harga beli {{$feature_name}}"></div>
              </div>
              <div class="input-group">
                <div class="col-sm-2 d-flex align-items-center">Stok</div>
                <div class="col-sm-10"><input type="text" class="form-control masukkan" id="stok" name="stok" placeholder="Masukkan stok {{$feature_name}}"></div>
              </div>
              <div class="input-group">
                <div class="col-sm-2 d-flex align-items-center">Kategori</div>
                <div class="col-sm-10">
                  <select class="custom-select masukkan" id="kategori" name="kategori">
                    <option value="kertas" selected>Kertas</option>
                    <option value="plastik">Plastik</option>
                    <option value="logam">Logam</option>
                    <option value="kaca">Kaca</option>
                  </select>
                </div>
              </div>
              <div class="input-group">
                <div class="col-sm-2 d-flex align-items-center">Satuan Unit</div>
                <div class="col-sm-10">
                  <select class="custom-select masukkan" id="satuan_unit" name="satuan_unit">
                    <option value="kg" selected>Kilogram</option>
                    <option value="biji">Bijian</option>
                  </select>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Batal</button>
          <input type="submit" id="btn_action" class="btn btn-success" value="Simpan">
          </form>
        </div>
      </div>
    </div>
  </div>
<!-- End Modal -->
@include('layouts.footers.auth')
<script>
$(document).ready( function () {
    $('#myTable').DataTable({"aaSorting": [4]});
} );
var feature_name='{{$feature_name}}';
var urlRoute="{{url('barang')}}";

function showFormInput() {
  $(".masukkan").val("");
  $("#modal_title").html("Form Data "+feature_name+" Baru");
  $("#form_data").attr("action", "javascript:actionsFunction(0,0)");
  $("#btn_action").attr("value", "Simpan Data");
  $("#btn_action").removeClass("btn-danger").addClass("btn-success");
}
function showFormUpdateOrDelete(action,id) {
  $("#icon_display").removeClass();
  if(action==1){ //ubah data
    $(".masukkan").prop('disabled', false);
    $("#btn_action").attr("value", "Ubah Data");
    $("#modal_title").html("Form Ubah Data "+feature_name);
    $("#btn_action").removeClass("btn-danger").addClass("btn-success");
  }else if(action==2){ //hapus data
    $(".masukkan").prop('disabled', true);
    $("#id").prop('disabled', false);
    $("#btn_action").attr("value", "Hapus Data");
    $("#modal_title").html("Form Hapus Data "+feature_name);
    $("#btn_action").removeClass("btn-success").addClass("btn-danger");
  }
  $.ajax({
    type: "GET",
    url: urlRoute+"/"+id,
    cache: false,
    success: function(result){
      var result = eval('('+result+')');
      if(result.success){
        $("#form_data").attr("action", "javascript:actionsFunction("+action+","+result.id+")");
        $("#id").val(result.id);
        $("#nama").val(result.nama);
        $("#stok").val(result.stok);
        $("#harga_jual").val(result.harga_jual);
        $("#harga_beli").val(result.harga_beli);
        $("#satuan_unit").val(result.satuan_unit);
        $("#kategori").val(result.kategori)
      }else{
        swal({
          title: "Gagal Mengambil Data {{$feature_name}} !",
          text: result.pesan,
          type: "error"
        });
      }
    },
    error: function (request, status, error) {
      swal({
          title: "Error!",
          text: error,
          type: "error",
          closeOnConfirm: false,
          closeOnCancel: false
      });
    }
  });
}
function actionsFunction(action,id) {
  var method="";
  var actionUrl=urlRoute+"/"+id;
  if(action==0){
    method='POST';
    actionUrl=urlRoute;
  }else if(action==1){
    method='PUT';
  }else if(action==2){
    method='DELETE';
  }
  var data = $('#form_data');
  $.ajax({
    url: actionUrl,
    data: data.serialize(),
    cache: false,
    type: method,
    success: function(result){
     var result = eval('('+result+')');
      if (result.success){
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false   
        }, function(){ location.reload(); });
      }
      else{
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false
        });
      }
    },
      error: function (request, status, error) {
        console.log(request.responseText);
        swal({
            title: "Error!",
            text: error,
            type: "error",
            closeOnConfirm: false,
            closeOnCancel: false
          });
      }
  });
}

</script>
@endsection