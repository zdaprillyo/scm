@extends('layouts.app', ['class' => 'bg-default'])

@section('content')
<div class="header bg-gradient-primary py-7 py-lg-1">
  <div class="justify-content-center m-4" style="width: 98%">
    @if($akses->pivot->create)
      <button type="button" class="btn btn-info" data-toggle="modal" data-target="#form-modal" onclick="showFormInput()">Tambah {{$feature_name}}</button>
    @endif
    <div><h3 class="text-white">Daftar {{$feature_name}}</h3></div>
    <div class="row table-responsive bg-white p-3">
    	<table class="table table-striped table-hover table-sm table-bordered" id="myTable">
    		<thead class="text-center">
    			<th>ID</th>
	    		<th>Total</th>
          <th>Termin</th>
          <th>Status</th>
          <th>Permintaan</th>
          <th>Aksi</th>
    		</thead>
    		<tbody>
    			@foreach($payments as $data)
    			<tr>
    				<td>{{$data->id}}</td>
    				<td class="text-right">{{ number_format($data->total_pembayaran,0,',','.') }}</td>
            <td class="text-right">{{ $data->termin }}</td>
            <td>
              @if($data->status_pembayaran=='lunas')
                <button class="btn btn-success btn-sm">{{ucwords($data->status_pembayaran)}}</button>
              @elseif($data->status_pembayaran=='belum lunas')
                <button class="btn btn-info btn-sm">{{ucwords($data->status_pembayaran)}}</button>
              @elseif($data->status_pembayaran=='batal')
                <button class="btn btn-danger btn-sm">{{ucwords($data->status_pembayaran)}}</button>
              @endif
            </td>
            <td>{{ $data->request_id }}-{{ ucfirst($data->request->jenis_permintaan) }}-{{ ucfirst($data->request->pelanggan->name) }} ({{ date('d-m-Y',strtotime($data->request->tanggal_transaksi)) }})</td>
            <td>
              @if($akses->pivot->update)
                @for($i=0;$i < count($data->detailpayments);$i++)
                  @if($data->detailpayments[$i]->status_pembayaran=='belum lunas')
                    <button type="button" class="btn btn-success btn-sm" onclick="updateStatus({{ $data->id }},{{ $data->detailpayments[$i]->id }},'lunas')">Lunasi Cicilan {{ $i+1 }}</button>
                    @if($i==0)
                      <button type="button" class="btn btn-danger btn-sm" onclick="updateStatus({{ $data->id }},{{ $data->detailpayments[$i]->id }},'batal')">Batal</button>
                    @endif
                    @break
                  @endif
                @endfor
              @endif
    				</td>
    			</tr>
    			@endforeach
    		</tbody>
    	</table>
    </div>
  </div>
</div>

<!-- Start Modal -->
  <div class="modal fade" id="form-modal" tabindex="-1" role="dialog" aria-labelledby="form-modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_title">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form_data" enctype="multipart/form-data" method="POST" action="">
            @csrf
            <div class="form-group row">
              <input type="hidden" class="masukkan" name="id" id="id" value="">
              <div class="input-group">
                <div class="col-sm-5 d-flex align-items-center">Termin</div>
                <div class="col-sm-7">
                  <select class="custom-select masukkan" id="termin" name="termin">
                    <option value="">Pilih termin</option>
                    <option value='1'>1x</option>
                    <option value='2'>2x</option>
                  </select>
                  {{-- <input class="form-control masukkan" placeholder="Masukan termin {{ $feature_name }}" type="text" name="termin" id="termin">   --}}
                </div> 
              </div>
              <div class="input-group">
                <div class="col-sm-5 d-flex align-items-center">Status</div>
                <div class="col-sm-7">
                  <select class="custom-select masukkan" id="status_pembayaran" name="status_pembayaran">
                    <option value="">Pilih status  {{$feature_name}}</option>
                    <option value='belum lunas'>Belum Lunas</option>
                    <option value='lunas'>Lunas</option>
                  </select>
                </div>
              </div>
              <div class="input-group">
                <div class="col-sm-5 d-flex align-items-center">ID Permintaan</div>
                <div class="col-sm-7">
                  <select class="custom-select masukkan" id="request_id" name="request_id">
                    <option value="">Pilih id permintaan</option>
                    @foreach($requests as $request)
                      <option value="{{ $request->id }}">{{ $request->id }}-{{ $request->pelanggan->name }} ({{ date('d-m-Y',strtotime($request->tanggal_transaksi)) }})</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Batal</button>
          <input type="submit" id="btn_action" class="btn btn-success" value="Simpan">
          </form>
        </div>
      </div>
    </div>
  </div>
<!-- End Modal -->
<div class="container bg-default mt--10 pb-5"></div>
@include('layouts.footers.auth')
<script>
  function filterInput(filter){
    if(filter==0){
      var stts=$('#status').val();
      if(stts!='sampai'){ $("#tanggal_sampai").prop('disabled', true); } 
      else{ $("#tanggal_sampai").prop('disabled', false); }
    }else if(filter==1){
      var id=$('#loading_id').val();
      $.ajax({
      url: "{{ url('loading') }}/"+id,
      cache: false,
      type: 'GET',
      success: function(result){
       var result = eval('('+result+')');
        if (result.success){
          if(result.jenis_pengangkut=='truk'){
            $("#no_tiket").prop('disabled', false);
          }else{
            $("#no_tiket").prop('disabled', true);
          }
        }
        else{
          swal({
            title: result.message_title,
            text: result.message_conten,
            type: result.message_type,
            closeOnConfirm: false,
            closeOnCancel: false
          });
        }
      },
        error: function (request, status, error) {
          console.log(request.responseText);
          swal({
              title: "Error!",
              text: error,
              type: "error",
              closeOnConfirm: false,
              closeOnCancel: false
            });
        }
    });
    }
  }
$(document).ready( function () {
    $('#myTable').DataTable({"aaSorting": [0]});
} );
var feature_name='{{$feature_name}}';
var urlRoute="{{url('payment')}}";

function showFormInput() {
  $(".masukkan").val("");
  $("#modal_title").html("Form Data "+feature_name+" Baru");
  $("#form_data").attr("action", "javascript:actionsFunction(0,0)");
  $("#btn_action").attr("value", "Simpan Data");
  $("#btn_action").removeClass("btn-danger").addClass("btn-success");
  $("#masukan-id-muat").show();
  $("#masukan-nomor-tiket").show();
}
function showFormUpdateOrDelete(action,id) {
  $("#masukan-id-muat").hide();
  $("#icon_display").removeClass();

  if(action==1){ //ubah data
    $(".masukkan").prop('disabled', false);
    $("#btn_action").attr("value", "Ubah Data");
    $("#modal_title").html("Form Ubah Data "+feature_name);
    $("#btn_action").removeClass("btn-danger").addClass("btn-success");
  }else if(action==2){ //hapus data
    $(".masukkan").prop('disabled', true);
    $("#id").prop('disabled', false);
    $("#btn_action").attr("value", "Hapus Data");
    $("#modal_title").html("Form Hapus Data "+feature_name);
    $("#btn_action").removeClass("btn-success").addClass("btn-danger");
  }
  $.ajax({
    type: "GET",
    url: urlRoute+"/"+id,
    cache: false,
    success: function(result){
      var result = eval('('+result+')');
      if(result.success){
        $("#form_data").attr("action", "javascript:actionsFunction("+action+","+result.id+")");
        $("#id").val(result.id);
        $("#tanggal_berangkat").val(result.tanggal_berangkat);
        $("#estimasi_sampai").val(result.estimasi_sampai);
        $("#tanggal_sampai").val(result.tanggal_sampai);
        $("#no_tiket").val(result.no_tiket);
        if(!result.no_tiket){ $("#masukan-nomor-tiket").hide(); }else{$("#masukan-nomor-tiket").show(); }
        $("#status").val(result.status);
      }else{
        swal({
          title: "Gagal Mengambil Data {{$feature_name}} !",
          text: result.pesan,
          type: "error"
        });
      }
    },
    error: function (request, status, error) {
      console.log(request.responseText);
      swal({
          title: "Error!",
          text: error,
          type: "error",
          closeOnConfirm: false,
          closeOnCancel: false
      });
    }
  });
}
function updateStatus(idpembayaran,iddetail,aksi){
  var actionUrl=urlRoute+"/"+idpembayaran;
  $.ajax({
    url: actionUrl,
    data: {"_token":"{{ csrf_token() }}","iddetail":iddetail,"status_pembayaran":aksi},
    cache: false,
    type: "PUT",
    success: function(result){
     var result = eval('('+result+')');
      if (result.success){
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false   
        }, function(){ location.reload(); });
      }
      else{
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false
        });
      }
    },
      error: function (request, status, error) {
        console.log(request.responseText);
        swal({
            title: "Error!",
            text: error,
            type: "error",
            closeOnConfirm: false,
            closeOnCancel: false
          });
      }
  });
}
function actionsFunction(action,id) {
  var method="";
  var actionUrl=urlRoute+"/"+id;
  if(action==0){
    method='POST';
    actionUrl=urlRoute;
  }else if(action==1){
    method='PUT';
  }else if(action==2){
    method='DELETE';
  }
  var data = $('#form_data');
  $.ajax({
    url: actionUrl,
    data: data.serialize(),
    cache: false,
    type: method,
    success: function(result){
     var result = eval('('+result+')');
      if (result.success){
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false   
        }, function(){ location.reload(); });
      }
      else{
        swal({
          title: result.message_title,
          text: result.message_conten,
          type: result.message_type,
          closeOnConfirm: false,
          closeOnCancel: false
        });
      }
    },
      error: function (request, status, error) {
        console.log(request.responseText);
        swal({
            title: "Error!",
            text: error,
            type: "error",
            closeOnConfirm: false,
            closeOnCancel: false
          });
      }
  });
}
</script>
@endsection