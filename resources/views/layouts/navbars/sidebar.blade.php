<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-default" id="sidenav-main">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        <a {{-- class="navbar-brand pt-0" --}} href="{{ route('home') }}">
            {{-- <img src="{{ asset('argon') }}/img/brand/blue.png" class="navbar-brand-img" alt="..."> --}}
           
            <img style="height: 100px" src="{{ asset('logo-dua-saudara-v2.png') }}">
       
              
        </a>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Collapse header -->
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="{{ route('home') }}">
                            <img src="{{ asset('argon') }}/img/brand/blue.png">
                            {{-- <img src="{{ asset('logo-dua-saudara.png') }}">   --}}
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <!-- User -->
            <ul class="navbar-nav d-none d-md-flex">
                <li class="nav-item dropdown">
                    <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div {{-- class="media align-items-center" --}}>
                            <span class="avatar avatar-md rounded-circle">
                                {{-- <img alt="Image placeholder" src="{{ asset('argon') }}/img/theme/team-4-800x800.jpg"> --}}
                                <img alt="Image placeholder" src="{{ asset('avatar') }}/user-default.jpg">
                            </span>
                            <div class="media-body d-none d-lg-block">
                                <span class="mb-0 text-sm  font-weight-bold text-white">
                                    {{ ucfirst(auth()->user()->name)  }}</span>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-left">
                        <div class=" dropdown-header noti-title">
                            <h6 class="text-overflow m-0">{{ __('Welcome!') }}</h6>
                        </div>
                        <a href="{{ route('profile.edit') }}" class="dropdown-item">
                            <i class="ni ni-single-02"></i>
                            <span>{{ __('Profilku') }}</span>
                        </a>
                        <!-- <a href="#" class="dropdown-item">
                            <i class="ni ni-settings-gear-65"></i>
                            <span>{{ __('Settings') }}</span>
                        </a>
                        <a href="#" class="dropdown-item">
                            <i class="ni ni-calendar-grid-58"></i>
                            <span>{{ __('Activity') }}</span>
                        </a>
                        <a href="#" class="dropdown-item">
                            <i class="ni ni-support-16"></i>
                            <span>{{ __('Support') }}</span>
                        </a> -->
                        <div class="dropdown-divider"></div>
                        <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                            <i class="ni ni-user-run"></i>
                            <span>{{ __('Logout') }}</span>
                        </a>
                    </div>
                </li>
            </ul>
            <!-- Navigation -->
            <ul class="navbar-nav">
               {{--  <li class="nav-item">
                    <a class="nav-link @if(route('home')==url()->current()) text-blue @else text-white @endif" href="{{ route('home') }}">
                        <i class="ni ni-tv-2"></i> {{ __('Dashboard') }}
                    </a>
                </li> --}}
                
                <?php use App\User; use App\Submenu; 
                  $menus=Auth::user()->allowedMenus(); 
                  $submenus=Auth::user()->allowedSubs();
                ?> 
                @foreach($menus as $menu)
                  @if($menu->submenus->isEmpty())
                    <li class="nav-item">
                        <a class="nav-link @if(url($menu->url)==url()->current()) text-blue @else text-white @endif" href="{{ url($menu->url) }}">
                            <i class="{{$menu->ikon}}"></i> {{ __(ucwords($menu->nama)) }}
                        </a>
                    </li>
                  @else
                    <li class="nav-item">
                        <a class="nav-link text-white" href="#navbar-{{$menu->id}}" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-{{$menu->id}}">
                            <i class="fa {{$menu->ikon}} text-white"></i>
                            <span class="nav-link-text">{{ __( ucwords($menu->nama)) }}</span>
                        </a>
                        <div class="collapse show" id="navbar-{{$menu->id}}">
                            <ul class="nav nav-sm flex-column">
                                @foreach($submenus as $sub)
                                  @if($sub->menu_id==$menu->id)
                                  <li class="nav-item">
                                      <a class="nav-link @if(url($sub->url)==url()->current()) text-blue @else text-white @endif" href="{{url($sub->url)}}">
                                          {{ __(ucwords($sub->nama)) }}
                                      </a>
                                  </li>
                                  @endif
                                @endforeach
                            </ul>
                        </div>
                      </li>
                  @endif
                @endforeach
            </ul>
            <!-- Divider -->
            <hr class="my-3">
            <!-- Heading -->
            <h6 class="navbar-heading text-muted">Documentation</h6>
            <!-- Navigation -->
            <ul class="navbar-nav mb-md-3">
                <li class="nav-item">
                    <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/getting-started/overview.html">
                        <i class="ni ni-spaceship"></i> Getting started
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/foundation/colors.html">
                        <i class="ni ni-palette"></i> Foundation
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/components/alerts.html">
                        <i class="ni ni-ui-04"></i> Components
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
