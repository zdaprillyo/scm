<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Argon Dashboard') }}</title>
        <!-- Favicon -->
        <link href="{{ asset('argon') }}/img/brand/favicon.png" rel="icon" type="image/png">
        <!-- Fonts -->
        {{-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet"> --}}
        <link rel="stylesheet" type="text/css" href="{{ asset('css') }}/googleapis.css">
        <!-- Icons -->
        <link href="{{ asset('argon') }}/vendor/nucleo/css/nucleo.css" rel="stylesheet">
        <link href="{{ asset('argon') }}/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
        <!-- FA Icon CSS -->
       {{--  <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> --}}
        <link rel="stylesheet" type="text/css" href="{{ asset('css') }}/font-awesome.min.css">
        <!-- My CSS -->
        <link rel="stylesheet" href="{{ asset('css') }}/mycss.css">
        <!-- Argon CSS -->
        <link type="text/css" href="{{ asset('argon') }}/css/argon.css?v=1.0.0" rel="stylesheet">
        <!-- Sweet Alert CSS-->
        <link rel="stylesheet" type="text/css" href="{{ asset('css') }}/sweetalert.css">
        <!-- Autocomplete CSS-->
        <link rel="stylesheet" href="{{ asset('css') }}/jquery-ui.css">
        <!-- Datatable CSS-->
        {{-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css"> --}}
        <link rel="stylesheet" type="text/css" href="{{ asset('css') }}/dataTables.bootstrap4.min.css">
        <!-- Datetime picker CSS-->
        <link rel="stylesheet" type="text/css" href="{{ asset('css') }}/bootstrap-datetimepicker.css">


        <!-- Main JS -->
        <script type="text/javascript" src="{{ asset('js') }}/jquery-3.5.1.min.js"></script>
        <!-- Sweet Alert JS  -->
        <script type="text/javascript" src="{{ asset('js') }}/sweetalert.min.js"></script>
        <!-- FA JS  -->
        <script type="text/javascript" src="{{ asset('js') }}/fa5984a05ea2.js"></script>
        <!-- Autocomplete JS -->
        <script src="{{ asset('js') }}/jquery-ui.js"></script>
        <!-- DataTable JS -->
        {{-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script> --}}
        <script type="text/javascript" src="{{ asset('js') }}/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="{{ asset('js') }}/dataTables.bootstrap4.min.js"></script>
        <!-- Datetime picker JS -->
        <script type="text/javascript" src="{{ asset('js') }}/moment.min.js"></script>
        <script type="text/javascript" src="{{ asset('js') }}/bootstrap-datetimepicker.min.js"></script>
        
        
    </head>
    <body class="{{ $class ?? '' }}">
        @auth()
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
            @include('layouts.navbars.sidebar')
        @endauth
        
        <div class="main-content">
            @include('layouts.navbars.navbar')
            @yield('content')
        </div>

        @guest()
            @include('layouts.footers.guest')
        @endguest

        
        <!-- Argon JS -->
        {{-- <script src="{{ asset('argon') }}/vendor/jquery/dist/jquery.min.js"></script> --}}
        <script src="{{ asset('argon') }}/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
        <script src="{{ asset('argon') }}/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        
        @stack('js')
        
        <!-- Argon JS -->
        <script src="{{ asset('argon') }}/js/argon.js?v=1.0.0"></script>


    </body>
</html>