<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
	//template routes
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
	//custome routes
	Route::resource('menu','MenuController');
	Route::resource('submenu','SubmenuController');
	Route::resource('privilege','PrivilegeController');
	Route::resource('barang','ProductController');
	Route::resource('fuel','FuelController');
	Route::resource('request','RequestController');
	Route::resource('packing','PackingController');
	Route::resource('fproduct','FproductController');
	Route::resource('loading','LoadingController');
	Route::resource('delivery','DeliveryController');
	Route::resource('payment','PaymentController');
	Route::resource('schedule','ScheduleController');
	Route::resource('pengguna','PenggunaController');
	//Route::resource('budget','BudgetController');
	Route::get('getworktime','ScheduleController@getWorktime');
	Route::get('permintaan/{jenis}','RequestController@index')->name('permintaan');
	Route::get('buat/{jenis}','RequestController@create')->name('buat');
	Route::get('akses','AksesController@index');
	Route::put('akses','AksesController@update');
	Route::get('search', 'ProductController@search')->name('product.search');
	Route::get('cariHarga', 'ProductController@getHarga')->name('product.getHarga');
	Route::get('prediction','PredictionController@index');
	Route::get('getPredictionDetail/{id}','PredictionController@show');
	Route::post('prediction','PredictionController@store');
	Route::post('terimaRequest','RequestController@terimaRequest');
});



// Route::resource('menu','MenuController')->middleware('auth');
// Route::resource('submenu','SubmenuController')->middleware('auth');
// Route::resource('privilege','PrivilegeController')->middleware('auth');
// Route::resource('barang','ProductController')->middleware('auth');
// Route::resource('fuel','FuelController')->middleware('auth');
// Route::resource('request','RequestController')->middleware('auth');
// Route::resource('packing','PackingController')->middleware('auth');
// Route::resource('fproduct','FproductController')->middleware('auth');
// Route::resource('loading','LoadingController')->middleware('auth');
// Route::resource('delivery','DeliveryController')->middleware('auth');
// Route::resource('payment','PaymentController')->middleware('auth');
// Route::resource('schedule','ScheduleController')->middleware('auth');
// Route::resource('pengguna','PenggunaController')->middleware('auth');
// Route::resource('budget','BudgetController')->middleware('auth');
// Route::get('getworktime','ScheduleController@getWorktime')->middleware('auth');
// Route::get('permintaan/{jenis}','RequestController@index')->name('permintaan')->middleware('auth');
// Route::get('buat/{jenis}','RequestController@create')->name('buat')->middleware('auth');
// Route::get('akses','AksesController@index')->middleware('auth');
// Route::put('akses','AksesController@update')->middleware('auth');
// Route::get('search', 'ProductController@search')->middleware('auth')->name('product.search');
// Route::get('cariHarga', 'ProductController@getHarga')->middleware('auth')->name('product.getHarga');
// Route::get('prediction','PredictionController@index')->middleware('auth');
// Route::get('getPredictionDetail/{id}','PredictionController@show')->middleware('auth');
// Route::post('prediction','PredictionController@store')->middleware('auth');

