<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Fuel;
use Faker\Generator as Faker;

$factory->define(Fuel::class, function (Faker $faker) {
    return [
        'nama'=>'solar',
        'tanggal_beli'=>now(),
        'qty'=>45,
        'harga'=>10000
    ];
});
