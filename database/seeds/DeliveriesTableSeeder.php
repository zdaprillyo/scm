<?php

use Illuminate\Database\Seeder;
use App\Delivery;
use App\Loading;
class DeliveriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for ($i=1; $i <= 10; $i++) { 
    		$delivery=new Delivery();
    		$delivery->tanggal_berangkat=now();
    		$delivery->estimasi_sampai=now();
    		$delivery->tanggal_sampai=now();
    		$loading=Loading::find($i);
    		if($loading->jenis_pengangkut=='truk'){ $delivery->no_tiket='ZXCDS'.$i; }
    		$status='dikirim';
    		if($i>5){ $status='sampai'; }
    		if($i>8){ $status='batal'; }
    		$delivery->status=$status;
    		$delivery->loading_id=$loading->id;
    		$delivery->save();
    	}
    }
}
