<?php

use Illuminate\Database\Seeder;
use App\Loading;
use App\Detailloading;
use App\Finalproduct;
class LoadingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for ($i=0; $i < 10; $i++) { 
    		$loading= new Loading();
	        $loading->waktu_mulai=now();
	        $loading->waktu_selesai=now();
	        $status='dimuat';
	        if($i>4){ $status='dikirim'; }
	        if($i>7){ $status='dibongkar'; }
	        $loading->status=$status;
	        $pengangkut='truk';
	        if($i>4){ $pengangkut='kontainer'; }
	        $loading->jenis_pengangkut=$pengangkut;
	        $loading->no_pengangkut='ZD '.$i.' A';
	        $loading->merk_pengangkut='ZDA'.$i;
	        if($pengangkut=='truk'){
	        	$loading->nama_sopir='ZDA'.$i;
	        	$loading->telepon=$i.''.$i.''.$i.''.$i.''.$i.''.$i.'';
	        }else{
	        	$loading->no_segel='ZDA'.$i;
	        }
	        $loading->request_id=(16+$i-1);
	        $loading->save();
	        if($i<9){
	        	for ($y=1; $y <= 10; $y++) { 
	        		$detail=new Detailloading();
	        		$detail->finalproduct_id=($i*10)+$y;
	        		$fproduk=Finalproduct::find($detail->finalproduct_id);
	        		$detail->loading_id=$loading->id;
	        		$detail->harga=$fproduk->product->harga_jual;
	        		$detail->sub_total=$detail->harga*$fproduk->qty;
	        		$detail->save();
	        	}
	        }
    	}
        

    }
}
