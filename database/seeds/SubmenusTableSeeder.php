<?php

use Illuminate\Database\Seeder;
use App\Submenu;
class SubmenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data=new Submenu();
        $data->nama="menu";
        $data->no_urut=1;
        $data->url="menu";
        $data->menu_id=1;
        $data->save();
        $data->privileges()->attach(1,['create'=>1,'read'=>1,'update'=>1,'delete'=>1]);
        $data->privileges()->attach(2,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]);
        $data->privileges()->attach(3,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]);

        $data=new Submenu();
        $data->nama="privilege";
        $data->no_urut=2;
        $data->url="privilege";
        $data->menu_id=1;
        $data->save();
        $data->privileges()->attach(1,['create'=>1,'read'=>1,'update'=>1,'delete'=>1]);
        $data->privileges()->attach(2,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]);
        $data->privileges()->attach(3,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]);

        $data=new Submenu();
        $data->nama="aksesibilitas";
        $data->no_urut=3;
        $data->url="akses";
        $data->menu_id=1;
        $data->save();
        $data->privileges()->attach(1,['create'=>1,'read'=>1,'update'=>1,'delete'=>1]);
        $data->privileges()->attach(2,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]);
        $data->privileges()->attach(3,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]);

        $data=new Submenu();
        $data->nama="prediksi";
        $data->no_urut=1;
        $data->url="prediction";
        $data->menu_id=6;
        $data->save();
        $data->privileges()->attach(1,['create'=>1,'read'=>1,'update'=>1,'delete'=>1]);
        $data->privileges()->attach(2,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]);
        $data->privileges()->attach(3,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]);


        
    }
}
