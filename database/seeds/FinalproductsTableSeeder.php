<?php

use Illuminate\Database\Seeder;
use App\Finalproduct;
class FinalproductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status=0;  
    	for ($i=1; $i <=50; $i++) {
    		for ($y=0; $y < 5; $y++) {
    		 	$fproduk=new Finalproduct();
		        $fproduk->tanggal_dibuat=now();
		        $fproduk->qty=mt_rand(80,150);
                $fproduk->status=$status;
		        $fproduk->product_id=mt_rand(1,16);
		        $fproduk->packing_id=$i;
		        $fproduk->save();
                if($fproduk->id>=80){
                    $status=1;
                }
    		}
    	}
    }
}
