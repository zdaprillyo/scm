<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data=new User();
        $data->name='aprillyo';
        $data->email='admin@argon.com';
        $data->email_verified_at=now();
        $data->password=Hash::make('secret');
        $data->privilege_id=1;
        $data->save();

        $pengepul=['karmin','basir','salwi','asis','tamri','imis','waryo','wahdi'];
        foreach ($pengepul as $nama) {
            $data=new User();
            $data->name=$nama;
            $data->email=$nama.'@argon.com';
            $data->email_verified_at=now();
            $data->password=Hash::make('secret');
            $data->privilege_id=2;
            $data->save();
        }
        
        $konsumen=['pabrik','cahya','ida','gudang','basuki'];
        foreach ($konsumen as $nama) {
            $data=new User();
            $data->name=$nama;
            $data->email=$nama.'@argon.com';
            $data->email_verified_at=now();
            $data->password=Hash::make('secret');
            $data->privilege_id=3;
            $data->save();
        }
    }
}
