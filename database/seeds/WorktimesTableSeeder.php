<?php

use Illuminate\Database\Seeder;
use App\Worktime;
class WorktimesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$tgl_awal=date('Y-m-01');
        for ($i=1; $i <= date('t') ; $i++) {
	        $mulai=date('Y-m-d 07:00',strtotime($tgl_awal));
	    	$selesai=date('Y-m-d H:i',strtotime('+1 hour 45 minutes',strtotime($mulai)));
        	$time=new Worktime();
	        $time->waktu_mulai=$mulai;
	        $time->waktu_selesai=$selesai;
	        $time->save();
	        for ($j=0; $j < 4; $j++) { 
	        	$mulai=$selesai;
		        $selesai=date('Y-m-d H:i',strtotime('+1 hour 45 minutes',strtotime($mulai)));
		        $time=new Worktime();
		        $time->waktu_mulai=$mulai;
		        $time->waktu_selesai=$selesai;
		        $time->save();
		        if(date('H:i',strtotime($selesai))=='12:15'){
		        	$selesai=date('Y-m-d H:i',strtotime('+45 minutes',strtotime($selesai)));
		        }
	        }
	        $tgl_awal=date('Y-m-d H:i',strtotime('+1 day',strtotime($selesai)));
        }
    }
}
