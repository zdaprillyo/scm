<?php

use Illuminate\Database\Seeder;
use App\Prediction;
class PredictionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dari_tanggal=[
            '05-10-2020',
            '06-10-2020',
            '07-10-2020',
            '08-10-2020',
            '09-10-2020',
            '10-10-2020',
            '11-10-2020',
            '12-10-2020',
            '13-10-2020',
            '14-10-2020'];
        $hingga_tanggal=[
            '04-11-2020',
            '05-11-2020',
            '06-11-2020',
            '07-11-2020',
            '08-11-2020',
            '09-11-2020',
            '10-11-2020',
            '11-11-2020',
            '12-11-2020',
            '13-11-2020',
            ];
        $utk_tanggal=[
            '11-11-2020',
            '12-11-2020',
            '13-11-2020',
            '14-11-2020',
            '15-11-2020',
            '16-11-2020',
            '17-11-2020',
            '18-11-2020',
            '19-11-2020',
            '20-11-2020',
            ];
        $prediksiPembayaran=[
            '6320697',
            '6253742',
            '6241328',
            '6218707',
            '6234790',
            '6195875',
            '6242243',
            '6188925',
            '6278035',
            '6284902',
            ];
        for ($i=0; $i < 10; $i++) { 
            $prediksi= new Prediction();
            $prediksi->dari_tanggal=date('Y-m-d',strtotime($dari_tanggal[$i]));
            $prediksi->hingga_tanggal=date('Y-m-d',strtotime($hingga_tanggal[$i]));
            $prediksi->utk_tanggal=date('Y-m-d',strtotime($utk_tanggal[$i]));
            $prediksi->prediksi_pembayaran=$prediksiPembayaran[$i];
            $prediksi->user_id=1;
            $prediksi->save();
        }
    }
}
