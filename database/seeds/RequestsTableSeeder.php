<?php

use Illuminate\Database\Seeder;
use App\Permintaan;
use App\Product;
use App\Worktime;
use App\Jadwal;
class RequestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      for ($i=0; $i < 30; $i++){
        $jenis_permintaan='pengambilan';
        $pelanggan_id=mt_rand(1,9);
        if($i>14){ 
          $jenis_permintaan='pembelian';
          $pelanggan_id=mt_rand(10,14);
        }
      	$request=new Permintaan();
      	$request->tanggal_transaksi=now();
      	$request->total_transaksi=0;
      	$request->jenis_permintaan=$jenis_permintaan;
      	$request->status_request='diterima';
      	if($i>9){ 
          $request->status_request='menunggu'; 
        }
        if($jenis_permintaan=='pembelian' && $i<25){
          $request->status_request='diterima';
        }
        if($request->status_request=='diterima'){
          $request->tanggal_konfirmasi=now();
          $request->admin_id=1;
        }
      	$request->pelanggan_id=$pelanggan_id;
      	$request->save();
      	$idBarangTerpakai=[];
      	for ($y=0; $y < 5; $y++) { 
      		$barang=Product::find(mt_rand(1,16));
      		if(!in_array($barang->id, $idBarangTerpakai)){
      			$idBarangTerpakai[]=$barang->id;
      			$qty=mt_rand(20,150);
            if($i>14){ $qty=mt_rand(150,500); }
      			if($i>9){
		      		$request->products()->attach($barang->id,['harga'=>$barang->harga_beli,'qty'=>$qty,'qty_diterima'=>null,'sub_total'=>($barang->harga_beli*$qty)]);
		      	}else{
		      		$request->products()->attach($barang->id,['harga'=>$barang->harga_beli,'qty'=>$qty,'qty_diterima'=>$qty,'sub_total'=>($barang->harga_beli*$qty)]);
		      	}
      		}
      	}
      	$total_transaksi=0;
      	foreach($request->products as $detail) { $total_transaksi+=$detail->pivot->sub_total; }
      	$request->total_transaksi=$total_transaksi;
      	$request->save();
      	if($i<10){
      		$tgl_sekarang=date('Y-m-d');
      		for ($y=0; $y < date('t'); $y++) { 
	          $worktime=Worktime::whereDate('waktu_mulai',$tgl_sekarang)->get();
	          foreach ($worktime as $time) {
	            if($time->status==1){
	              $jadwal=new Jadwal();
	              if($i<5){
	              	$jadwal->status_jadwal='selesai';
	              	$jadwal->waktu_mulai_aktual=now();
	              	$jadwal->waktu_selesai_aktual=now();
	              }else{
	              	$jadwal->status_jadwal='terjadwal';
	              }
	              $jadwal->request_id=$request->id;
	              $jadwal->worktime_id=$time->id;
	              $jadwal->save();
	              $time->status=0;
	              $time->save();
	              break 2;
	            }
	          }
	        	$tgl_sekarang=date('Y-m-d',strtotime('+1 day',strtotime($tgl_sekarang)));
        	}
      	}
      }
    }
}
