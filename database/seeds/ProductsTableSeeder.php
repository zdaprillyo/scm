<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$data=new Product();
        $data->nama='kardus';
        $data->stok=100;
        $data->harga_jual=2200;
        $data->harga_beli=1100;
        $data->satuan_unit='kg';
        $data->kategori='kertas';
        $data->save();
        $data=new Product();
        $data->nama='hvs';
        $data->stok=100;
        $data->harga_jual=2900;
        $data->harga_beli=1500;
        $data->satuan_unit='kg';
        $data->kategori='kertas';
        $data->save();
        $data=new Product();
        $data->nama='buram';
        $data->stok=100;
        $data->harga_jual=1800;
        $data->harga_beli=700;
        $data->satuan_unit='kg';
        $data->kategori='kertas';
        $data->save();
        $data=new Product();
        $data->nama='duplex';
        $data->stok=100;
        $data->harga_jual=1000;
        $data->harga_beli=400;
        $data->satuan_unit='kg';
        $data->kategori='kertas';
        $data->save();
        $data=new Product();
        $data->nama='botol oli';
        $data->stok=100;
        $data->harga_jual=5000;
        $data->harga_beli=2500;
        $data->satuan_unit='kg';
        $data->kategori='plastik';
        $data->save();
        $data=new Product();
        $data->nama='blowing';
        $data->stok=100;
        $data->harga_jual=4000;
        $data->harga_beli=2500;
        $data->satuan_unit='kg';
        $data->kategori='plastik';
        $data->save();
        $data=new Product();
        $data->nama='naso';
        $data->stok=100;
        $data->harga_jual=6400;
        $data->harga_beli=3000;
        $data->satuan_unit='kg';
        $data->kategori='plastik';
        $data->save();
        $data=new Product();
        $data->nama='plastik putih';
        $data->stok=100;
        $data->harga_jual=5000;
        $data->harga_beli=2500;
        $data->satuan_unit='kg';
        $data->kategori='plastik';
        $data->save();
        $data=new Product();
        $data->nama='ember warna';
        $data->stok=100;
        $data->harga_jual=3700;
        $data->harga_beli=1700;
        $data->satuan_unit='kg';
        $data->kategori='plastik';
        $data->save();
        $data=new Product();
        $data->nama='pet botol putih';
        $data->stok=100;
        $data->harga_jual=5000;
        $data->harga_beli=2500;
        $data->satuan_unit='kg';
        $data->kategori='plastik';
        $data->save();
        $data=new Product();
        $data->nama='pet botol semu';
        $data->stok=100;
        $data->harga_jual=4600;
        $data->harga_beli=2500;
        $data->satuan_unit='kg';
        $data->kategori='plastik';
        $data->save();
        $data=new Product();
        $data->nama='ale ale';
        $data->stok=100;
        $data->harga_jual=4000;
        $data->harga_beli=1700;
        $data->satuan_unit='kg';
        $data->kategori='plastik';
        $data->save();
        $data=new Product();
        $data->nama='aqua kotor';
        $data->stok=100;
        $data->harga_jual=4000;
        $data->harga_beli=2000;
        $data->satuan_unit='kg';
        $data->kategori='plastik';
        $data->save();
        $data=new Product();
        $data->nama='aqua bersih';
        $data->stok=100;
        $data->harga_jual=7800;
        $data->harga_beli=4300;
        $data->satuan_unit='kg';
        $data->kategori='plastik';
        $data->save();
        $data=new Product();
        $data->nama='kaleng';
        $data->stok=100;
        $data->harga_jual=2200;
        $data->harga_beli=900;
        $data->satuan_unit='kg';
        $data->kategori='logam';
        $data->save();
        $data=new Product();
        $data->nama='seng';
        $data->stok=100;
        $data->harga_jual=2200;
        $data->harga_beli=900;
        $data->satuan_unit='kg';
        $data->kategori='logam';
        $data->save();
    }
}
