<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([WorktimesTableSeeder::class]);
        $this->call([PrivilegesTableSeeder::class]);
        $this->call([UsersTableSeeder::class]);
        $this->call([MenusTableSeeder::class]);
        $this->call([SubmenusTableSeeder::class]);
        $this->call([ProductsTableSeeder::class]);
        $this->call([FuelsTableSeeder::class]);
        $this->call([RequestsTableSeeder::class]);
        $this->call([PaymentsTabelSeeder::class]);
        $this->call([PredictionsTableSeeder::class]);
        $this->call([PackingsTableSeeder::class]);
        $this->call([FinalproductsTableSeeder::class]);
        $this->call([LoadingsTableSeeder::class]);
        $this->call([DeliveriesTableSeeder::class]);
    }
}
