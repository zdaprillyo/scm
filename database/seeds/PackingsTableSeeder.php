<?php

use Illuminate\Database\Seeder;
use App\Packing;
class PackingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    for ($i=0; $i < 50; $i++) { 
	        $packing=new Packing();
	        $packing->waktu_mulai=now();
	        $packing->waktu_selesai=now();
	        $pakaiBbm=mt_rand() / mt_getrandmax();
	        if($pakaiBbm>0.5){
	            $packing->fuel_id=mt_rand(1,10);
	        }
	        $packing->save();
	    }
    }
}
