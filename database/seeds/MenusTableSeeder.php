<?php

use Illuminate\Database\Seeder;
use App\Menu;
class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new Menu();
        $data->nama="manajemen hak akses";
        $data->ikon="fas fa-user-lock";
        $data->no_urut=1;
        $data->url="#";
        $data->save();
        $data->privileges()->attach(1,['create'=>1,'read'=>1,'update'=>1,'delete'=>1]);
        $data->privileges()->attach(2,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]);
        $data->privileges()->attach(3,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]);

        $data = new Menu();
        $data->nama="manajemen pengambilan barang";
        $data->ikon="fa fa-truck";
        $data->no_urut=2;
        $data->url="permintaan/pengambilan";
        $data->save();
        $data->privileges()->attach(1,['create'=>1,'read'=>1,'update'=>1,'delete'=>1]);
        $data->privileges()->attach(2,['create'=>1,'read'=>1,'update'=>1,'delete'=>0]);
        $data->privileges()->attach(3,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]);

        $data = new Menu();
        $data->nama="manajemen penjadwalan";
        $data->ikon="fa fa-calendar";
        $data->no_urut=3;
        $data->url="schedule";
        $data->save();
        $data->privileges()->attach(1,['create'=>1,'read'=>1,'update'=>1,'delete'=>1]);
        $data->privileges()->attach(2,['create'=>1,'read'=>1,'update'=>1,'delete'=>0]);
        $data->privileges()->attach(3,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]);

        $data = new Menu();
        $data->nama="manajemen pembayaran";
        $data->ikon="fa fa-money";
        $data->no_urut=4;
        $data->url="payment";
        $data->save();
        $data->privileges()->attach(1,['create'=>1,'read'=>1,'update'=>1,'delete'=>1]);
        $data->privileges()->attach(2,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]);
        $data->privileges()->attach(3,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]);

        $data = new Menu();
        $data->nama="manajemen order barang";
        $data->ikon="fa fa-inbox";
        $data->no_urut=5;
        $data->url="permintaan/pembelian";
        $data->save();
        $data->privileges()->attach(1,['create'=>1,'read'=>1,'update'=>1,'delete'=>1]);
        $data->privileges()->attach(2,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]);
        $data->privileges()->attach(3,['create'=>1,'read'=>1,'update'=>1,'delete'=>0]);

        $data = new Menu();
        $data->nama="perencanaan keuangan";
        $data->ikon="fa fa-line-chart";
        $data->no_urut=6;
        $data->url="#";
        $data->save();
        $data->privileges()->attach(1,['create'=>1,'read'=>1,'update'=>1,'delete'=>1]);
        $data->privileges()->attach(2,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]);
        $data->privileges()->attach(3,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]); 

        $data = new Menu();
        $data->nama="manajemen barang";
        $data->ikon="fa fa-briefcase";
        $data->no_urut=7;
        $data->url="barang";
        $data->save();
        $data->privileges()->attach(1,['create'=>1,'read'=>1,'update'=>1,'delete'=>1]);
        $data->privileges()->attach(2,['create'=>0,'read'=>1,'update'=>0,'delete'=>0]);
        $data->privileges()->attach(3,['create'=>0,'read'=>1,'update'=>0,'delete'=>0]); 

        $data = new Menu();
        $data->nama="manajemen bbm";
        $data->ikon="fas fa-gas-pump";
        $data->no_urut=8;
        $data->url="fuel";
        $data->save();
        $data->privileges()->attach(1,['create'=>1,'read'=>1,'update'=>1,'delete'=>1]);
        $data->privileges()->attach(2,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]);
        $data->privileges()->attach(3,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]); 

        $data = new Menu();
        $data->nama="manajemen packing";
        $data->ikon="fas fa-box";
        $data->no_urut=9;
        $data->url="packing";
        $data->save();
        $data->privileges()->attach(1,['create'=>1,'read'=>1,'update'=>1,'delete'=>1]);
        $data->privileges()->attach(2,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]);
        $data->privileges()->attach(3,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]);

        $data = new Menu();
        $data->nama="manajemen final product";
        $data->ikon="fa fa-boxes";
        $data->no_urut=10;
        $data->url="fproduct";
        $data->save();
        $data->privileges()->attach(1,['create'=>1,'read'=>1,'update'=>1,'delete'=>1]);
        $data->privileges()->attach(2,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]);
        $data->privileges()->attach(3,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]);

        $data = new Menu();
        $data->nama="manajemen muatan";
        $data->ikon="fa fa-truck-loading";
        $data->no_urut=11;
        $data->url="loading";
        $data->save();
        $data->privileges()->attach(1,['create'=>1,'read'=>1,'update'=>1,'delete'=>1]);
        $data->privileges()->attach(2,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]);
        $data->privileges()->attach(3,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]);

        $data = new Menu();
        $data->nama="manajemen pengiriman";
        $data->ikon="fas fa-truck-moving";
        $data->no_urut=12;
        $data->url="delivery";
        $data->save();
        $data->privileges()->attach(1,['create'=>1,'read'=>1,'update'=>1,'delete'=>1]);
        $data->privileges()->attach(2,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]);
        $data->privileges()->attach(3,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]);  

        $data = new Menu();
        $data->nama="manajemen pengguna";
        $data->ikon="fa fa-user";
        $data->no_urut=13;
        $data->url="pengguna";
        $data->save();
        $data->privileges()->attach(1,['create'=>1,'read'=>1,'update'=>1,'delete'=>1]);
        $data->privileges()->attach(2,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]);
        $data->privileges()->attach(3,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]);      
    }
}
