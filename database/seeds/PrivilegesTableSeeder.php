<?php

use Illuminate\Database\Seeder;
use App\Privilege;

class PrivilegesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data=new Privilege();
        $data->nama="administrator";
        $data->save();

        $data=new Privilege();
        $data->nama="pengepul";
        $data->save();

        $data=new Privilege();
        $data->nama="konsumen";
        $data->save();
    }
}
